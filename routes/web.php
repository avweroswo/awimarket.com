<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('login/twitter', 'Auth\LoginController@redirectToProvider1');
Route::get('login/twitter/callback', 'Auth\LoginController@handleProviderCallback1');

Route::get('login/google', 'Auth\LoginController@redirectToProvider2');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback2');

Route::get('paymentSuccessful', 'PaymentController@store');

Route::get('captcha', 'HomeController2@createCaptcha');

Route::get('ApiDetails', 'ApiController@adDetailsPage');

Route::get('ApiGetFeaturedAds', 'ApiController@ApiGetFeaturedAds');

Route::get('ApiGetPopularCategory', 'ApiController@getPopularCategory');

Route::post('edit_promotedAds', 'adminController@EditPromotedAds');

Route::get('reportSeller','HomeController2@reportSeller');

Route::get('ApiGetBillboard','ApiController@getBillboard');

Route::post('admins1','adminController@authenticate');

Route::get('adminLogin','adminController@login');

Route::get('ApiAdDetailsPage','ApiController@adDetaiilsPage');

Route::get('mobileview','HomeController2@mobileview');

Route::get('delete_report','adminController@delete_report');

//Route::get('searchChart','adminController@delete_report');

Route::get('getPopularPlaces', 'ApiController@getPopularPlaces');

Route::get('ApiSearchResult', 'ApiController@SearchResult');

Route::get('GetApiHeaderAndFooter', 'ApiController@GetApiHeaderAndFooter');

Route::get('login_check', 'ApiController@login_check');

Route::get('Apilogin', 'ApiController@login');

Route::get('ApiHeader', 'ApiController@ApiHeader');

Route::get('/', 'HomeController2@index');

Route::get('/ddx','HomeController2@payment');

Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay'); 

Route::get('setCategoryerror_to_null','ApiController@setCategoryerror_to_null');

Route::get('navigation_all', 'ApiController@navigation_all');

Route::get('ads_tags', 'ApiController@ads_tags');

Route::get('getCatName', 'ApiController@getCatName');

Route::get('category_all', 'ApiController@category_all');

Route::get('mobileview','HomeController2@mobileview');

Route::get('AdminLogout','LoginController@Adminlogout');

Route::get('watermark','HomeController2@watermark_image');

Route::get('getMostPopularCategories','HomeController2@getMostPopularPlaces');

Route::get('searchresults', 'HomeController2@searchresults');

Route::get('filter_location_searchresults_seller', 'AdController@filter_location_searchresults_seller');

Route::post('create_ads','AdController@addTemPhoto');

Route::get('createAd1','AdController@createAd');

Route::post('editAd1','AdController@editAd');

Route::post('addTemPhoto','RegisterController@addTemPhoto');

Route::post('add_city', 'adminController@addCity');

Route::get('getSub/{id}', 'adminController@getSubs1_two');

Route::get('getSub2/{id}', 'adminController@getSubs2_two');

Route::get('subs', 'adminController@getSubs');

Route::get('followAd', 'AdController@followAd');

Route::get('UnfollowAd', 'AdController@UnfollowAd');

Route::get('saveAd', 'AdController@saveAd');

Route::get('UnsaveAd', 'AdController@UnsaveAd');

Route::get('add_rating', 'AdController@addRating');

Route::get('remove_rating', 'AdController@removeRating');

Route::get('admindashboard', 'adminController@admindashboard');

Route::get('delete_user', 'adminController@delete_user');

Route::get('delete_admin', 'adminController@delete_admin');

Route::get('delete_category', 'adminController@delete_category');

Route::get('delete_city', 'adminController@delete_city');

Route::get('filter_location', 'AdController@filter_location');

Route::get('filter_location_searchresults', 'AdController@filter_location_searchresults');

Route::get('filter_location_searchresults_serv', 'AdController@filter_location_searchresults_serv');

Route::get('delete_nav', 'adminController@delete_nav');

Route::get('delete_tags', 'adminController@delete_tags');

Route::post('add_admin', 'adminController@add_admin');

Route::post('add_tags', 'adminController@add_tags');

Route::post('edit_category', 'adminController@edit_category');

Route::post('edit_city', 'adminController@edit_city');

Route::post('add_category', 'adminController@add_category');

Route::get('verify_user', 'adminController@verify_user');

Route::post('edit_user', 'adminController@edit_user');

Route::post('add-logo', 'adminController@add_logo');

Route::post('add-billboard', 'adminController@add_billboard');

Route::post('add-sidebar', 'adminController@add_sidebar');

Route::post('add-nav', 'adminController@add_nav');

Route::post('edit_nav', 'adminController@edit_nav');

Route::post('edit_sidebar', 'adminController@edit_sidebar');

Route::post('edit_billboard', 'adminController@edit_billboard');

Route::get('delete_sidebar', 'adminController@delete_sidebar');

Route::get('delete_billboards', 'adminController@delete_billboards');

Route::get('delete_ads', 'AdController@DeleteAds');

Route::get('delete_followed_ads', 'AdController@DeleteFollowedAds');

Route::get('delete_saved_ads', 'AdController@DeleteSavedAds');

Route::get('forgotpass', 'forgotpassController@forgotpass');

Route::post('send_forgot_phone', 'forgotpassController@send_forgot_email');

Route::post('confirm_code', 'forgotpassController@confirm_code');

Route::post('fchangepassword', 'forgotpassController@changepassword');

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('pick', 'HomeController2@pick');

Route::get('pick2', 'HomeController2@pick2')->name('facebook.login');

Route::get('logout', 'LoginController@logout');

Route::get('dashboard', 'HomeController2@dashboard');

Route::get('sellerprofile', 'HomeController2@sellerprofile');

Route::post('changepassword1', 'RegisterController@changepassword');

Route::post('update_receive_newsletter', 'RegisterController@update_receive_newsletter');

Route::post('gen-code', 'RegisterController@genCode');

Route::post('UpdateDetails', 'RegisterController@UpdateDetails');

Route::post('authenticate0', 'LoginController@Authenticate');

Route::post('register2', 'RegisterController@register');

Route::post('addPhoto', 'RegisterController@addPhoto');

Route::post('addDetails', 'ContactController@addDetails');

Route::post('create-ad', 'AdController@createAd');

Route::get('signup', 'HomeController2@register');

Route::get('drag', 'HomeController2@drag');

Route::get('makeawish', 'HomeController2@makeawish');

Route::get('makeawish_cron', 'HomeController2@makeawish_cron');

Route::get('createAd', 'HomeController2@createAd');

Route::get('userprofile', 'HomeController2@userprofile');

Route::get('promoteAd', 'HomeController2@promoteAd');

Route::get('topAd', 'HomeController2@topAd');

Route::get('sidebarAd', 'HomeController2@sidebarAd');

Route::get('billboardAd', 'HomeController2@billboardAd');

Route::get('adDetails', 'HomeController2@adDetails');

Route::get('aboutus', 'HomeController2@aboutus');

Route::get('faq', 'HomeController2@faq');

Route::get('contactus', 'HomeController2@contactus');

Route::get('category', 'HomeController2@category');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
