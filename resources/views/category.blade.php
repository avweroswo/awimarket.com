<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>




<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<!-- <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css"> -->

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css"type="text/css"> 

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  #draggable { width: 150px; height: 150px; padding: 0.5em; }
  </style>
  
    
	<!-- End WOWSlider.com HEAD section -->
        
<script type="text/javascript" src="assets/js/jquery-min.js"></script> 
<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href="http://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="http://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  type="text/css">

<link rel="stylesheet" href="css/home.css" />

<style>
    a:hover,a:focus{
    outline: none;
    text-decoration: none;
}
.tab .nav-tabs{
    border: none;
    margin-bottom: 10px;
}
.tab .nav-tabs li a{
    padding: 10px 20px;
    margin-right: 15px;
    background: #92278f;
    font-size: 17px;
    font-weight: 600;
    color: #fff;
    text-transform: uppercase;
    border: none;
    border-top: 3px solid #92278f;
    border-bottom: 3px solid #92278f;
    border-radius: 0;
    overflow: hidden;
    position: relative;
    transition: all 0.3s ease 0s;
}
.tab .nav-tabs li.active a,
.tab .nav-tabs li a:hover{
    border: none;
    border-top: 3px solid #92278f;
    border-bottom: 3px solid #92278f;
    background: #fff;
    color: #92278f;
}
.tab .nav-tabs li a:before{
    content: "";
    border-top: 15px solid #92278f;
    border-right: 15px solid transparent;
    border-bottom: 15px solid transparent;
    position: absolute;
    top: 0;
    left: -50%;
    transition: all 0.3s ease 0s;
}
.tab .nav-tabs li a:hover:before,
.tab .nav-tabs li.active a:before{ left: 0; }
.tab .nav-tabs li a:after{
    content: "";
    border-bottom: 15px solid #92278f;
    border-left: 15px solid transparent;
    border-top: 15px solid transparent;
    position: absolute;
    bottom: 0;
    right: -50%;
    transition: all 0.3s ease 0s;
}
.tab .nav-tabs li a:hover:after,
.tab .nav-tabs li.active a:after{ right: 0; }
.tab .tab-content{
    padding: 20px 30px;
    border-top: 3px solid #92278f;
    border-bottom: 3px solid #92278f;
    font-size: 17px;
    color: #92278f;
    letter-spacing: 1px;
    line-height: 30px;
    position: relative;
}
.tab .tab-content:before{
    content: "";
    border-top: 25px solid #92278f;
    border-right: 25px solid transparent;
    border-bottom: 25px solid transparent;
    position: absolute;
    top: 0;
    left: 0;
}
.tab .tab-content:after{
    content: "";
    border-bottom: 25px solid #92278f;
    border-left: 25px solid transparent;
    border-top: 25px solid transparent;
    position: absolute;
    bottom: 0;
    right: 0;
}
.tab .tab-content h3{
    font-size: 24px;
    margin-top: 0;
}
@media only screen and (max-width: 479px){
    .tab .nav-tabs li{
        width: 100%;
        text-align: center;
        margin-bottom: 15px;
    }
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #555;
    cursor: default;
    background-color: #fff;
    border: 1px solid #ddd;
    border-bottom-color: transparent;
    position: relative;
    z-index: 1;
    margin-bottom: -1px!important;
    border-top: 1px solid;
    border: 1px solid #CCCCCC !important;
    border-top: 4px solid #92278f !important;
    border-bottom: 1px #FFF solid !important;
    background-color: #fff!important;
    margin: 0;
}
</style>
    
</head>
<body>
    
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1631938846852679',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
</script>

    
</head>
<body>
    
    <div class="home-background" style='height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')
         
    <div class="page-header" style="background: url(assets/img/banner1.jpg);margin-top:120px;">
       <div class="container">
          <div class="row">
            <div class="col-md-12">
               <div class="breadcrumb-wrapper">
                    <h2 class="page-title">Products</h2>
               </div>
            </div>
         </div>
      </div>
    </div>
         
         
     <div class="main-container">
<div class="container">
<div class="row">
    
    <div class='row' >
    
    <div class='col-sm-12 clearfix'>
        <div class="pull-left " style="width:0px;">

<form id='frm-priority' action='filter_location' class="" method="get">

<select id="priority" name="priority"  class='f-sm ' data-live-search="true">
<option value="" style="background-color:white;color:black;">Sort by</option>
<option value="PL">Price: Low to High</option>
<option value="PH">Price: High to Low</option>
<option value="PO">Popularity</option>
<option value="HR">Highly rated</option>
<option value="MR">Most Recent</option>
</select>

</form>
</div>
 
@if(Session::get('categories') != null)
        <div class="pull-left f-sm-7" style="width:0px;" >

<form id='frm-subcat' action='filter_location' class="" method="get">

<select id='subcat' name='subcat'  data-live-search="true" class='f-sm'>
    
   @foreach(Session::get('categories') as $val)
   
       <option value='{{$val->id}}' >{{$val->cat_name}}</option>
   
   @endforeach
   
</select>

</form>
</div>

@endif

<div class="f-sm-8 @if(Session::get('categories') == null) {{  'pull-right'   }} @else {{'pull-left'}} @endif" style="width:0px; @if(Session::get('categories') == null) {{  'margin-right:250px;'   }} @else = @endif" >

<form id='frm-location' action='filter_location' class="" method="get">

<select id='location' name='location' class='f-sm dropup'   data-live-search="true" >
<option value='' style="">Select</option>
<option value='All' style="">All Locations</option>
@foreach($cities_all as $val)
       <option value="{{$val->id}}">{{$val->city}}</option>
@endforeach

</select>

</form>
</div>
    </div>
        <br>
    @if(Session::get('search_result') !=null)
    
       <div style='margin-bottom:40px;margin-top:50px;margin-left:50px;'>
           <h3><b>Search Result For {{Session::get('search_result')}}</b></h3>
       </div>
    
    @endif
</div>
<div class="col-sm-12 page-content">

<div class="product-filter">
<div class="grid-list-count">
<a class="list switchToGrid" href="#"><i class="fa fa-list"></i></a>
<a class="grid switchToList" href="#"><i class="fa fa-th-large"></i></a>
</div>

</div>


<div class="adds-wrapper">
    
    @if(Session::get('rating_ids') == null)
         
           
<!--
<div class="f-sm-8 @if(Session::get('categories') == null) {{  'pull-right'   }} @else {{'pull-left'}} @endif" style="width:0px; @if(Session::get('categories') == null) {{  'margin-right:250px;'   }} @else = @endif" >



<div class="adds-wrapper">-->
    
    @foreach(Session::get('all_ads') as $val)
        
    <a href="adDetails?id={{$val->id}}">
    
           <div class="item-list">
<div class="col-sm-2 no-padding photobox">
<div class="add-image">
<img src="ad_photo/{{$obj->getPhoto($val->id)}}" width="300" height="262" alt="" class="img-box1" >
<span class="photo-count text-color-white color-purple" style='font-size:15px;padding:5px;'>&#8358;{{$val->price}}</span>
<span class="photo-count text-color-white color-purple " style='top:40px;font-size:10px;padding:5px;text-transform:uppercase;'>U Fit price am</span>
<div class='cat-text2 ' style='word-wrap:break-word;max-width:80%;'>{{$val->title}}
</div>
</div>
</div>
<div class="col-sm-12 add-desc-box" style='width:83%;'>
<div class="add-details" >
<h5 class="add-title"><a href="ads-details.html">{{$val->title}}</a></h5>
<div class='cat-text ' style='display:none;word-wrap:break-word;min-height:93px;'>{{$val->details}}
</div>

<div style='width:120%;'>
<a class="btn btn-sm click-add " style='display:inline;'><i class="fa fa-certificate"></i>
<span>{{$obj->getAdtype($val->id)}}</span></a>
<a class="btn btn-common btn-sm click-add" style='display:inline;'> <i class="fa fa-eye"></i> <span>{{$obj->getCityName($val->region)}}</span> </a>
</div>
</div>
</div>
   

</div>
        
    </a>
    
    
    @endforeach
    

    


</div>
    




                    
@else


<div>    
    @foreach(Session::get('all_ads') as $val)
        
    <a href="adDetails?id={{$val->id}}">
    
           <div class="item-list">
<div class="col-sm-2 no-padding photobox">
<div class="add-image">
<img src="ad_photo/{{$obj->getPhoto($val->id)}}" width="300" height="262" alt="" class="img-box1" >
<span class="photo-count text-color-white color-purple" style='font-size:15px;padding:5px;'>&#8358;{{$val->price}}</span>
<span class="photo-count text-color-white color-purple " style='top:40px;font-size:10px;padding:5px;text-transform:uppercase;'>U Fit price am</span>
<div class='cat-text2 ' style='word-wrap:break-word;max-width:80%;'>{{$val->title}}
</div>
</div>
</div>
<div class="col-sm-12 add-desc-box" style='width:83%;'>
<div class="add-details" >
<h5 class="add-title"><a href="ads-details.html">{{$val->title}}</a></h5>
<div class='cat-text ' style='display:none;word-wrap:break-word;min-height:93px;'>{{$val->details}}
</div>

<div style='width:120%;'>
<a class="btn btn-sm click-add " style='display:inline;'><i class="fa fa-certificate"></i>
<span>{{$obj->getAdtype($val->id)}}</span></a>
<a class="btn btn-common btn-sm click-add" style='display:inline;'> <i class="fa fa-eye"></i> <span>{{$obj->getCategoryName($val->region)}}</span> </a>
</div>
</div>
</div>
   

</div>
        
    </a>
    
    
    @endforeach
    
</div>
    
@endif
    
    <?php
    
    Session::put('all_ads',null);
    
    Session::put('rating_id',null);
    
    Session::put('search',null);
    
    Session::put('categories',null);
    
    Session::put('search_result',null);
    
    
    ?>
    
    


</div>


<div class="pagination-bar">
<ul class="pagination">
<li class="active"><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#"> ...</a></li>
<li><a class="pagination-btn" href="#">Next »</a></li>
</ul>
</div>
    
<div class="post-promo text-center" onclick='jj()'>
<h2>If you nor see wetin you like make a wish then </h2>
<button  class="btn btn-post btn-danger"  style='cursor:pointer;z-index:99999999'  data-toggle="modal" data-target="#exampleModalCenter">Make a wish </button>
</div>
</div>
</div>
</div>
</div>
    
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Make a Wish</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="makeawish" method='get'>
      <div class="modal-body">
          	<!-- Search Form -->
				
					
                                            <div class="row">
                                                <br>
                                                  @if(Session::get('category_error1') == '1')
                                                    <div style='background-color: red;color:white;margin-bottom:50px;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                                                    
                          
                                                 @endif
                                                <div class="col-sm-12" style="margin-bottom:30px;">
                                                    <div class="col-sm-6">
                                                        <select name="category" class="form-control selectpicker f-c category" data-live-search="true">
                                 <option value='select' name="category">Select</option>
                                 @foreach($category_all as $values)
                                 <option value='{{$values->id}}' >{{$values->cat_name}}</option>
                                 @endforeach
                             </select>
                             
                                                    </div>
                                                    <div class="col-sm-6">
                                                         <select id="categories" name="subcategory" data-live-search="true" class="selectpicker">
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-12" style="margin-bottom:15px;">
                                                    <div class="col-sm-6">
                                                          <select name="region" class="form-control selectpicker f-c" data-live-search="true" >
                                    <option value='' >Choose</option>
                                    @foreach($all_cities as $val)
                                       <option value='{{$val->id}}' @if(isset($ads->region)) @if($val->id == $ads->region ) Selected @endif @endif>{{$val->city}}</option>
                                    @endforeach
                                </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="searchword" placeholder="Search Word" class="form-control" />
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" name="email" placeholder="Email" />
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" name="phone" placeholder="Mobile No e.g 08161834974" />
                                                    </div>
                                                </div>
                                            </div>
					
				</div> <!-- /.rbm_search -->

      
      <div class="modal-footer">
       
        <button type="Submit" class="btn btn-primary" style='z-index:99999999'>Save</button>
        
        <button type="button" class="btn btn-primary" data-dismiss="modal" style='z-index:99999999'>Close</button>
       
      </div>
      </form> <!-- /form -->
  </div>
  </div>
</div>
     

        
  

      <script type="text/javascript" src="js/app2.js"></script>
      
        @include('footer.footer')
        

<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

    
    
    
         <script>
                            
                            $('.category').on('change', function() { 
                
                
                 $.ajax({
             method: "get",
            url: "getSub/"+$(this).val(),
            data: {   },
     
            success: function(data) {
                
                str = data.split(';;');   
                
                str_id = str[0].split(',');
                
               
                
                str_name = str[1].split(',');
           
          
                text = '';
                
                for(i=0;i<str_id.length;i++)
                {
                     
                                        
                   text += '<option value="'+str_id[i]+'">'+str_name[i]+'</option>';
                }
                
                $('#categories').html('<option value="" >Select</option>'+text);
                
                $('select').selectpicker('render');
                
                $('select').selectpicker('refresh');
                
                
           
            }
        });
  
                    
      return false;
      
     
               
            });
            
            </SCRIPT>
    
 @if(Session::get('category_error1') == 2)

 <script>
     
    // $("#exampleModalCenter").modal();   
     
     alert('Wish made Successfully');
     
 </script>
 
 @endif
 
 
 
 @if(Session::get('category_error1') == 1)
 
    <script>
        
       // alert('hi');
     
     $("#exampleModalCenter").modal();   
     
     //alert('Wish made Successfully');
     
 </script>
     

@endif    

<?php Session::put('category_error1',null); ?>


    
    
    </body>
</html>