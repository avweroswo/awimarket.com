<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">



     <link href="css/libs.min.8feb2.css" type="text/css" rel="stylesheet">
        <link href="css/portal.min.6e918.css" type="text/css" rel="stylesheet">
    
    <link href="css/profile.min.dc8c7.css" type="text/css" rel="stylesheet">
    
    <link rel="stylesheet" href="css/home.css" />

<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
            <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
	<!-- End WOWSlider.com HEAD section -->
        

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"  type="text/css">
  <link rel="stylesheet" href="css/home.css" />  
</head>
    
  


<style>
    
   

.select-d {
    
    display:none!important;
    margin-bottom:60px !important;
    
}

.display-b {
    
    display:block!important;
    margin-bottom: 60px !important;
    
}
    
    
</style>

    

    </head>
<body>
    
    <div class="home-background" style='height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')


    
        <div class="js-body-wrapper b-body-wrapper" id="js-vue-scope" data-web-id="1523267817##532cbf663e380ba3732f2f4f64317e1de5514a71">
            
    


            
                <div class="h-bg-grey h-pv-10 h-pb-0">
                    <div class="container">
                        
      
                        
    <div class="h-hflex h-flex-wrap h-mv-15" style='margin-top:100px;'>
        <div class="h-pr-15 h-pl-0 seller-info__wrapper">
            <div class="h-mm1200-flex-1">
                <div class="h-mm1200-vflex" data-bazooka="seller-info" data-owner="false" data-verified="true" data-location="Lagos State, Ikeja, No 7 idowu lane computer village" data-settings-url="/profile-settings.html" data-request-manager-call-url="null" data-seller_id="2405677" data-blocked="false" data-user-name="Amamchukwu Ventures Ltd" data-last-seen="11 hrs ago" data-phone-number="09083135476" data-facebook-url="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fjiji.ng%2Fsellerpage-2405677%3Fp%3D27%26utm_medium%3Dsite-share-button%26utm_source%3Dfacebook%26utm_campaign%3DfacebookButton" data-twitter-url="http://twitter.com/share?text=Amamchukwu Ventures Ltd&amp;url=https%3A%2F%2Fjiji.ng%2Fsellerpage-2405677%3Fp%3D27%26utm_medium%3Dsite-share-button%26utm_source%3Dtwitter%26utm_campaign%3DtwitterButton" data-img-avatar="/static/img/no-image/user/no_avatar.png" data-csrf-token="1523267817##532cbf663e380ba3732f2f4f64317e1de5514a71" data-registered="2 months" data-button-text="Start chat" data-descr="" data-bazid="1"><div class="b-seller-info box-shadow h-mb-10"><div class="b-seller-info-header__wrapper"><div class="b-seller-info-header__avatar--wrapper"><div class="b-seller-info-header"><div class="b-seller-info-header__avatar box-shadow js-avatar" style="background-image: url('userprofile_img/{{$users->pic_name}}');"></div></div>
                                <div class="b-seller-info__name b-seller-info__name--verified">
                                    <h1>{{$users->company_name}}</h1>
                                </div>
                                <div class="b-seller-info__location">
                                <i class="h-icon icon-location-lg"></i>{{$users->address}}
                               </div></div><div class="b-seller-info__descr"></div></div><div class="b-seller-info-rl"><div class="b-seller-info-rl--inner"><p class="b-seller-info-rl__date">{{$dt->diffInMonths($users->created_at)}} months</p><p>Registered</p></div><div class="b-seller-info-rl--inner"><p class="b-seller-info-rl__date">
                                  2 hours </p><p>Last seen</p></div></div><div class="h-mm1200-vflex h-flex-main-center"><div class="h-mh-15 h-mv-10" style="line-height: 10px;"><div class="general-button general-button--full general-button--border-radius  general-button--with-shadow general-button--with-icon" data-bazooka="show-seller-contact" data-category-slug="seller" data-number="09083135476" data-seller_id="2405677" data-bazid="21"><div class="general-button__icon h-centerWrapp"><div class="h-centerItem"><i class="h-icon icon-call"></i></div></div><span class="general-button__text" style="display: inline;"><span class="pointer-style hidden_nos" >08XXXXXXXXX</span><span class="hide h-nos1">{{$users->phone}}</span></span><div class="clearfix"></div></div></div><div><ul class="b-seller-info__soc h-ph-15"><li><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fjiji.ng%2Fsellerpage-2405677%3Fp%3D27%26utm_medium%3Dsite-share-button%26utm_source%3Dfacebook%26utm_campaign%3DfacebookButton" rel="nofollow" target="_blank"><i class="h-icon icon-facebook-circle"></i></a></li><li><a href="http://twitter.com/share?text=Amamchukwu%20Ventures%20Ltd&amp;url=https%3A%2F%2Fjiji.ng%2Fsellerpage-2405677%3Fp%3D27%26utm_medium%3Dsite-share-button%26utm_source%3Dtwitter%26utm_campaign%3DtwitterButton" rel="nofollow" target="_blank"><i class="h-icon icon-twitter-circle"></i></a></li></ul></div></div></div></div>

                
                    <div>
                        <div class="b-seller-schedule box-shadow h-mb-10">
                            <div class="b-seller-schedule__title">Shop open</div>
                            <ul class="b-seller-schedule__days-of-week">
                                
                                    <li class="b-seller-schedule__day">Mo</li>
                                
                                    <li class="b-seller-schedule__day">Tu</li>
                                
                                    <li class="b-seller-schedule__day">We</li>
                                
                                    <li class="b-seller-schedule__day">Th</li>
                                
                                    <li class="b-seller-schedule__day">Fr</li>
                                
                                    <li class="b-seller-schedule__day">Sa</li>
                                
                                    <li class="b-seller-schedule__day">Su</li>
                                
                            </ul>

                            
                        </div>
                    </div>
                
            </div>

           

            
                
                    
    
        <div class="qa-show-adsense sticky-wrapper h-flex h-pos-rel h-inline-block h-text-right h-width-100p h-mm992-none-force" data-bazooka="sticky-elem" data-sticky="js-sticky-elem" data-top-offset="15px" data-align="right" data-bazid="2">
            <!-- /255062232/Skyscraper_fixed_160x600 -->
            
    
        <div id="div-gpt-ad-1487669506388-0" style="width: 160px; height: 600px; padding-top: 15px;" class="h-iblock js-sticky-elem h-mh-auto qa-show-adsense h-overflow-hidden">
       
        </div>
    

        </div>
    

                
            
        </div>
        
        <div class="h-flex-2-1-400 h-ph-0 h-mt-0">
            <div class="col-sm-12 page-content">

   <div class='row' >
    
    <div class='col-sm-12 clearfix'>
        <div class="pull-left " style="width:0px;">

<form id='frm-priority1' action='filter_location_searchresults_seller' class="" method="get">

<select id="priority1" name="priority" name=selectpicker" class='f-sm ' data-live-search="true">
<option value="" style="background-color:white;color:black;">Sort by</option>
<option value="PL">Price: Low to High</option>
<option value="PH">Price: High to Low</option>
<option value="PO">Popularity</option>
<option value="HR">Highly rated</option>
<option value="MR">Most Recent</option>
</select>

</form>
</div>
        
@if(Session::get('categories') != null)
        <div class="pull-left f-sm-7" style="width:0px;" >

<form id='frm-subcat1' action='filter_location_searchresults_seller' class="" method="get">

<select id='subcat1' name='subcat'  data-live-search="true" class='f-sm'>
    
   @foreach(Session::get('categories') as $val)
   
       <option value='{{$val->id}}' >{{$val->cat_name}}</option>
   
   @endforeach
   
</select>

</form>
</div>

@endif
        
  <div class="f-sm-8 @if(Session::get('categories') == null) {{  'pull-right'   }} @else {{'pull-left'}} @endif" style="width:0px; @if(Session::get('categories') == null) {{  'margin-right:250px;'   }} @else = @endif" >

<form id='frm-location1' action='filter_location_searchresults_seller' class="" method="get">

<select id='location1' name='location' class='f-sm dropup'   data-live-search="true" >
<option value='Select' style="">Select</option>
<option value='All' style="">All Locations</option>
@foreach($cities_all as $val)
       <option value="{{$val->id}}">{{$val->city}}</option>
@endforeach

</select>

</form>
</div>
    </div>
        <br>
    @if(Session::get('search_result') !=null)
    
    <div style='margin-bottom:40px;margin-top:50px;margin-left:50px;'>
           <h3><b>Search Result For {{Session::get('search_result')}}</b></h3>
       </div>
    
    @endif

<div class="product-filter">
<div class="grid-list-count">
<a class="list switchToGrid" href="#"><i class="fa fa-list"></i></a>
<a class="grid switchToList" href="#"><i class="fa fa-th-large"></i></a>
</div>

</div>


<div class="adds-wrapper">
    
    @foreach(Session::get('all_ads_search_seller') as $val)
        
    <a href="adDetails?id={{$val->id}}">
    
           <div class="item-list">
<div class="col-sm-2 no-padding photobox">
<div class="add-image">
<img src="ad_photo/{{$obj->getPhoto($val->id)}}" width="300" height="262" alt="" class="img-box1" >
<span class="photo-count text-color-white color-purple" style='font-size:15px;padding:5px;'>&#8358;2000</span>
<span class="photo-count text-color-white color-purple " style='top:40px;font-size:10px;padding:5px;text-transform:uppercase;'>U Fit price am</span>
<div class='cat-text2 ' style='word-wrap:break-word;max-width:80%;'>{{$val->title}}
</div>
</div>
</div>
<div class="col-sm-12 add-desc-box" style='width:83%;'>
<div class="add-details" >
<h5 class="add-title"><a href="ads-details.html">{{$val->title}}</a></h5>
<div class='cat-text ' style='display:none;word-wrap:break-word;min-height:93px;'>{{$val->details}}
</div>

<div style='width:120%;'>
<a class="btn btn-sm click-add " style='display:inline;'><i class="fa fa-certificate"></i>
<span>Ad type</span></a>
<a class="btn btn-common btn-sm click-add" style='display:inline;'> <i class="fa fa-eye"></i> <span>{{$obj->getCategoryName($val->region)}}</span> </a>
</div>
</div>
</div>
   

</div>
        
    </a>
    
    
    @endforeach
    


</div>


<div class="pagination-bar">
<ul class="pagination">
<li class="active"><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#"> ...</a></li>
<li><a class="pagination-btn" href="#">Next »</a></li>
</ul>
</div>
<!--
<div class="post-promo text-center">
<h2>If you nor see wetin you like make a wish then </h2>
<a  class="btn btn-post btn-danger" data-toggle="modal" data-target="#make-a-wish-modal">Make a wish </a>
</div>
-->
</div>
        </div>


    </div>

                    </div>
                </div>
            

            

    

        </div>
    

    
    

    


     

    <div class="js-subscribe-push-notification"></div>

     
    

    
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

<script type="text/javascript" src="js/app2.js"></script>  

    
    @include('footer.footer')    
    
        



    
        <div id="adwords_remarketing_tag"></div>
    

    
<div class="h-height-0 h-overflow-hidden">
    <img height="1" width="1" alt="" src="./Amamchukwu Ventures Ltd Shop ▷ Company contacts, prices, products for sale online on Jiji.ng _ Store phone number_ 09083135476_files/30">
</div>

