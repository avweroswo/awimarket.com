<Html>
    <head>
        
        <script
                   src="https://code.jquery.com/jquery-3.3.1.min.js"
                   integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                    crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        
        <link rel="stylesheet" href="css/home.css" />
  
        <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

        <link rel="shortcut icon" href="assets/img/favicon.png">




<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">


	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
        
        <script type="text/javascript" src="js/app2.js"></script>
        

	<!-- End WOWSlider.com HEAD section -->
        



    </head>
    
    <body class="body" >
        <div class="home-background">
            
           @include('header.header')
            
            <div class="row" >
                
                <div id="wowslider-container1">
	            <div class="ws_images">
                        <ul>
		         <li><img src="data1/images/oladeexslider2.jpg" alt="Oladeex-slider2" title="Oladeex-slider2" id="wows1_0"/></li>
		         <li><img src="data1/images/oladeexslider3.jpg" alt="Oladeex-slider3" title="Oladeex-slider3" id="wows1_1"/></li>
		         <li><a href="http://wowslider.com"><img src="data1/images/oladexslider1.jpg" alt="wow slider" title="OLadex-Slider1" id="wows1_2"/></a></li>
		         <li><img src="data1/images/oladexslider2.jpg" alt="Oladex-Slider2" title="Oladex-Slider2" id="wows1_3"/></li>
	                </ul>
                    </div>
	           <div class="ws_bullets">
                       <div>
		          <a href="#" title="Oladeex-slider2"><span><img src="data1/tooltips/oladeexslider2.jpg" alt="Oladeex-slider2"/>1</span></a>
		          <a href="#" title="Oladeex-slider3"><span><img src="data1/tooltips/oladeexslider3.jpg" alt="Oladeex-slider3"/>2</span></a>
		          <a href="#" title="OLadex-Slider1"><span><img src="data1/tooltips/oladexslider1.jpg" alt="OLadex-Slider1"/>3</span></a>
		          <a href="#" title="Oladex-Slider2"><span><img src="data1/tooltips/oladexslider2.jpg" alt="Oladex-Slider2"/>4</span></a>
	               </div>
                   </div>
	          <div class="ws_shadow"></div>
	        </div>	
            
                
            </div>
            
            <div class="row padding-10 background-color-white" >
                <div class="col-sm-12" align="center">
                    <H1 class="bb-m-text-20">Wetin Dey Awi Market</H1>
                </div>
                <div class="col-sm-12" align="center">
                    Na here you go see totori things wey dey Awi market
                </div> 
            </div>
            
            <div class="row padding-50 centralize" >
                
               <div class="col-sm-12 col-xs-6 col-lg-12 col-md-12">
                   
                   <div class="row">
                       
                      <div class="col-sm-2 ">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add  text-center ">
                                <i class="fa fa-paw fa-3x margin-top-50" aria-hidden="true"></i>
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Animals and Pets
                            </div>
                              
                          </div>
                    
                      </div>
                   
                      
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add text-center ">
                                
                                <i class="fa fa-desktop fa-3x margin-top-50"></i>
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Electronics
                            </div>
                              
                          </div>
                    
                      </div>
                       
                       <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add ">
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Fashion & Beauty
                            </div>
                              
                          </div>
                    
                      </div>
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add  ">
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Home,Furniture & Garden
                            </div>
                              
                          </div>
                    
                      </div>
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add text-center ">
                                 <i class="fa fa-mobile fa-3x margin-top-50 " aria-hidden="true"></i>
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Mobile Phones & Tablets
                            </div>
                              
                          </div>
                    
                      </div>
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add text-center">
                                <i class="fa fa-home fa-3x margin-top-50 " aria-hidden="true"></i>
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Real Estate
                            </div>
                              
                          </div>
                    
                      </div>
                 
             
                    </div>
                   
               </div>
                
               <div class="col-sm-12 col-xs-6 col-lg-12 col-md-12 padding-top-10 bb-m-padding" >
                   
                   <div class="row">
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add ">
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Hobbies & Arts
                            </div>
                              
                          </div>
                    
                      </div>
                   
                       
                 
                
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add text-center ">
                                 <i class="fas fa-bicycle fa-3x margin-top-50 "></i>
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Sport
                            </div>
                              
                          </div>
                    
                      </div>
                       
                       <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add text-center ">
                                
                                <i class="fas fa-briefcase fa-3x margin-top-50"></i>
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                              Jobs & Services
                            </div>
                              
                          </div>
                    
                      </div>
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add text-center ">
                                
                                <i class="fa fa-car fa-3x margin-top-50" aria-hidden="true"></i>
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                               Vehicles
                            </div>
                              
                          </div>
                    
                      </div>
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add ">
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 text-center font-size-10 text-left">
                               Pets
                            </div>
                              
                          </div>
                    
                      </div>
                       
                      <div class="col-sm-2">
                          
                          <div class="row">
                       
                            <div class="col-sm-12 round-purple color-purple click-add ">
                    
                            </div>
                       
                            <div class="col-sm-12 width-100 font-size-10 text-center font-size-10 text-left">
                               Pets
                            </div>
                              
                          </div>
                    
                      </div>
                 
             
               </div>
                   
               </div>
                
              <div class="col-sm-12 padding-top-20" align="center" >
                    <button class="btn border-radius-10 btn-1 bb-mobile click-add" >you like see more?</button>
              </div>
             
            </div>
            
            <div class="row padding-top-20">
               <div class="col-sm-12 "  >
                   <img src="images/awoof2.jpg" class="img-responsive" style="width:100%;height:300px;" />
                   <button class="purple-button-2 color-purple border-radius-10 click-add" >Awoof!</button>
                   <button class="purple-button  color-purple border-radius-10 click-add" >Everi! Everi!</button>
               </div>
            </div> 
            
            <div class="row">
                <div class="col-sm-12 " align="center"  >
                    <b>Carousel</b>
                </div>
            </div>
           
           @include('footer.footer')
            
            
          
        </div>
        
            
        
        <script type="text/javascript" src="engine1/wowslider.js"></script>
        <script type="text/javascript" src="engine1/script.js"></script>



        
    </body>
    
</Html>