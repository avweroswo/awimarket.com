<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">



<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<!-- <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css"> -->

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css"type="text/css"> 

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
    
	<!-- End WOWSlider.com HEAD section -->
        
<script type="text/javascript" src="assets/js/jquery-min.js"></script> 
<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">



<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  type="text/css">

<link rel="stylesheet" href="css/home.css" />
    
</head>
<body>
    
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1631938846852679',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

    
    <div class="home-background" style='min-height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')


  

	
     

        
        <div class='row' style="overflow:auto;margin-top:180px;margin-bottom:180px;">
                
                  <div class='clol-sm-12  back-color-white width-50-e centralize float-none height-50  clearfix ' style='padding:50px;' >
                      <H3>Forgot Password</H3>
                    @if(Session::get('suc1') == null)
                      <form action='send_forgot_phone' class='' method='post' >
                          @csrf
                          
                            <br>
                            
                              <div style='color:red;'>
                                   
                                   @if(isset($errors)) 
                                    @foreach($errors->all() as $error)
                                    {{$error}}<BR>
                                    @endforeach
                                   @endif
                                   
                              </div>
                                   
                           
                             
                      
                       <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="glyphicon glyphicon-phone" aria-hidden="true"></i></span>
                                     <input id='phone'  type="text" class="form-control" name='phone' placeholder='Please enter Phone' value='' />
                       </div>
                            <br>
                      <button  class='forgot_email_btn btn click-add' name='forgot_email_btn'><i class="fa fa-spinner fa-spin forgot-spin hide" aria-hidden="true"></i> &nbsp;Next</button>
                      </form>
                    
                    @elseif(Session::get('suc1') == 1)
                    
                      <form action='confirm_code' method='post' >
                        @csrf
                        
                             <br>
                            
                              <div style='color:red;'>
                                   
                                   @if(isset($errors)) 
                                    @foreach($errors->all() as $error)
                                    {{$error}}<BR>
                                    @endforeach
                                   @endif
                                   
                              </div>
                       <div class="input-group">
                                     
                                     <input id='code'  type="text" class="form-control" name='code' placeholder='Please enter Code' value='' />
                       </div>
                             <br>
                      <button id='confirm_code_btn1' class=' btn click-add ' name='confirm_code_btn'><i id ='confirm-spin' class="fa fa-spinner fa-spin confirm-spin hide" aria-hidden="true"></i> &nbsp;Next</button>
                      </form>
                    
                    @elseif(Session::get('suc1') == 2)
                    
                     <form action='fchangepassword' method='post' >
                         
                         <br>
                            
                              <div style='color:red;'>
                                   
                                   @if(isset($errors)) 
                                    @foreach($errors->all() as $error)
                                    {{$error}}<BR>
                                    @endforeach
                                   @endif
                                   
                              </div>
                         
                         @CSRF
                    
                    <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="
glyphicon glyphicon-lock" aria-hidden="true"></i></span>
                                     <input id="password1" type="password" class="form-control" name="password1" placeholder='Password' value='{{Session::get('password') == null ? '' : Session::get('password')}}' style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                    <br>
                    
                    <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="
glyphicon glyphicon-lock" aria-hidden="true"></i></span>
                                     <input id="password2" type="password" class="form-control" name="password2" placeholder='Confirm Password' value='{{Session::get('password') == null ? '' : Session::get('password')}}' style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                    
                    <br>
                    
                    <button  class='pass_btn btn click-add' name='pass_btn'><i class="fa fa-spinner fa-spin pass-spin hide" aria-hidden="true"></i> &nbsp;Next</button>
                      </form>
                    @elseif(Session::get('suc1') == 3)
                        Password Change Successful
                        <?php
                        
                        Session::put('suc1',null);
                        
                        ?>
                        
                    @endif
                       
                      
                          
                 
                          
                 </div>
                    
          
               
              </div>

   





<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>


<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>

        <script type="text/javascript" src="js/app2.js"></script>
  
    
    
      

        
        @include('footer.footer')
        
        
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>