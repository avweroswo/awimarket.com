<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">

<link rel="stylesheet" href="css/home.css" />

<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"  type="text/css">

<link rel="stylesheet" href="css/home.css" />
</head>
<body class="btn_collapzion" >
    


    
    <div class="home-background" style='height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')



  

 <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container1" class='m-top-70' onclick='myFunction3()' >
	<div class="ws_images"><ul>
                <?php $a=0; ?>
                @foreach($billboard as $val)
                
                
                
                <a href="{{$val->link}}" >
                      <li><img src="billboard/{{$val->picture}}" alt="" title="" id="wows1_{{$a}}"/></li>               
                </a>
		

                <?php $a++; ?>
                
                @endforeach
                
             
             @if($obj->checkAd($val->id) == 1)   
                @foreach($billboard_ads as $val)
                
                        <a href="adDetails?id={{$val->id}}" >
                      <li><img src="ad_photo/{{$obj1->getPhoto($val->id)}}" alt="" title="" id="wows1_{{$a}}"/></li>               
                </a>
		

                <?php $a++; ?>
                
                  
                @endforeach
            @endif
		
	</ul></div>
	<div class="ws_bullets"><div>
                 <?php $a=0; ?>
            
                @foreach($billboard as $val)
                
                 <?php $a++; ?>
        
                 <a href="{{$val->link}}" title="">
                     <span>
                        <img src="billboard/{{$val->picture}}" alt=""/>
                        {{$a}}
                     </span>
                 </a>
                
                
                @endforeach
                
                @if($obj->checkAd($val->id) == 1)   
                @foreach($billboard_ads as $val)
                
                <?php $a++; ?>
                
                <a href="adDetails?id={{$val->id}}" >
                      <span>
                          <img src="ad_photo/{{$obj1->getPhoto($val->id)}}" alt="" />
                          {{$a}}
                      </span>               
                </a>
		

                
                  
                @endforeach
            @endif
		
	</div></div>
	<div class="ws_shadow"></div>
	</div>	
	<script type="text/javascript" src="engine1/wowslider.js"></script>
	<script type="text/javascript" src="engine1/script.js"></script>
        
        
        <div class="wrapper" >

<section id="categories-homepage">
<div class="container">
<div class="row">
<div class="col-md-12">

<section class="featured-lis">
<div class="container">
<div class="row">
<div class="col-md-12 wow fadeIn" data-wow-delay="0.5s">
<h3 class="section-title">See Awi Promoted Ads</h3>

<div id="new-products" class="owl-carousel">
@foreach($featured_ads as $val)
@if($obj1->checkAd($val->id) == 1)

<div class="item">
<div class="product-item">
<div class="carousel-thumb">
<a href="adDetails?id={{$val->id}}" >
  
<img src="ad_photo/{{$obj1->getPhoto($val->id)}}" height="150" width="200" alt=""></a>

<div class="overlay">
<a href="adDetails?id={{$val->id}}"><i class="fa fa-link"></i></a>
</div>
</div>
<a href="adDetails?id={{$val->id}}" class="item-name">{{substr($val->title,0,20)}}</a>
<span class="price">&#8358;{{number_format($val->price)}}</span>
</div>
</div>

@endif

@endforeach

</div>
</div>
</div>
</div>
</section>

<h3 class="section-title"  >Wetin U Dey Find?</h3>
</div>
    
<div class="categories">
				<div class="container">
                                    <?php $a=0; ?><?php $z=0; ?>
                                    @foreach($popular_categories[0] as $values)
                                        <?php $a++; ?>
					<div class="col-md-2 focus-grid">
						<a href="filter_location?index=category&cat_id={{$values->category_id}}">
							<div class="focus-border">
								<div class="focus-layout">
									<div class="focus-image"><i class="{{$obj->get_icon_data($values->category_id)}}"></i></div>
									<h4 class="clrchg">{{$obj->getCategoryName($values->category_id)}}</h4>
								</div>
							</div>
						</a>
					</div>
                                    
                                    @endforeach
                                    
                                     @if($a < 6)
                                    
                                       <?php for($i=0;$i<sizeof($popular_categories[1]);$i++){ ?>
                                     
                                            <?php $a++; ?>
                                     
                                            <?php $z++; ?>
                                       
                                    
                                        <div class="col-md-2 focus-grid">
						<a href="filter_location?index=category&cat_id={{$popular_categories[1][$i]->id}}">
							<div class="focus-border">
								<div class="focus-layout">
									<div class="focus-image"><i class="{{$obj->get_icon_data($popular_categories[1][$i]->id)}}"></i></div>
									<h4 class="clrchg">{{$obj->getCategoryName($popular_categories[1][$i]->id)}}</h4>
								</div>
							</div>
						</a>
					</div>
                                       
                                      
                                       
                                        @if( $a >= 6  )
                                       
                                       @break
                                       
                                       @endif
                                       
                                       
                                       
                                       <?php } ?>
                                       
                                    @endif
                                    
                                    <div class="col-md-4 focus-grid text-center rem-1  rem-btn">
                                        <button class='btn click-add l-more'>Load more</button>
				    </div>	
                                    
                              <div class='rm1 remaining'>
                                        <?php ?>
                                      
					 @foreach($popular_categories[0] as $values)
                                       
                                         
                                         @if($a > 6)
                                         
					<div class="col-md-2 focus-grid">
						<a href="filter_location?index=category&cat_id={{$values->category_id}}">
							<div class="focus-border">
								<div class="focus-layout">
									<div class="focus-image"><i class="{{$obj->get_icon_data($values->category_id)}}"></i></div>
									<h4 class="clrchg">{{$obj->getCategoryName($values->category_id)}}</h4>
								</div>
							</div>
						</a>
					</div>
                                         
                                         @endif
                                         
                                          <?php $a++; ?>
                                         
                                         
                                        @endforeach
                                        
                                        <?php $v=0; ?>
                                        
                                        @if($z < sizeof($popular_categories[1]) )
                                       
                                          <?php for($i=0;$i<sizeof($popular_categories[1]);$i++){ ?>
                                        
                                             <?php $v++; ?>
                                        
                                        
                                        @if($i >= $z && ($v + $a) <= 6   )
                                       
                                       
                                    
                                        <div class="col-md-2 focus-grid">
						<a href="filter_location?index=category&cat_id={{$popular_categories[1][$i]->id}}">
							<div class="focus-border">
								<div class="focus-layout">
									<div class="focus-image"><i class="{{$obj->get_icon_data($popular_categories[1][$i]->id)}}"></i></div>
									<h4 class="clrchg">{{$obj->getCategoryName($popular_categories[1][$i]->id)}}</h4>
								</div>
							</div>
						</a>
					</div>
                                        
                                        
                                        @endif
                                       
                                       
                                        
                                         <?php $a++; ?>
                                       
                                      
                                       
                                            <?php } ?>
                                        
                                        
                                        @endif
                                        
                                            
                                   
					
					<div class="clearfix"></div>
                                    </div> 
				</div>
			</div>
    
    <section class="app" style="background:url('images/awoof2.jpg') !important;">
<div class="container">
<div class="row">
<div class="col-md-7 col-sm-12 col-xs-12">
<div class="app-inner wow fadeInLeft" data-wow-delay="0.5s">
<h2>Get Free Mobile App from AppStore and GooglePlay</h2>
<div class="description">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta vel, commodi laudantium officiis ipsum est aliquid dolore omnis, facilis molestiae dicta ipsam aperiam corporis minima mollitia iusto voluptatibus asperiores nemo.</p>
</div>
<a href="filter_location?index=awoof">
    <button class="btn btn-common btn-lg color-purple click-add">Awoof!</button>
</a>
<a href="filter_location?index=everieveri">
     <button class="btn btn-danger btn-lg color-purple click-add">Everi! Everi!</button>
</a>
</div>
</div>
<div class="col-md-6">
</div>
</div>
</div>
</section>

    <div style="padding:120px 0;">
        <h3 class="section-title">Places wey dey sell pass</h3>
        <div id="thumbnail-slider">
            <div class="inner">
                <ul>
                    <?php $a=0; ?>
                    
                    @foreach($popular_places[0] as $val)
                    
                        <?php $a++; ?>
                    
                        @if($a <= 10)
                        
                            <li>
                                <a class="thumb" href="city_pic/{{$obj->getPlacePic($val->region_id)}}"></a>
                            </li>
                        
                        @endif
                    
                        
                    
                    @endforeach
                    
                    @if($a <= 10)
                    
                      <?php for($i=0;$i<sizeof($popular_categories[1]);$i++){ ?>
                    
                        @if($a <= 10)
                        
                            <li>
                                <a class="thumb" href="city_pic/{{$obj->getPlacePic($popular_categories[1][$i]->id)}}"></a>
                            </li>
                        
                        @endif
                        
                        <?php $a++; ?>
                    
                      <?php } ?>
                    
                @endif
                    
                    
                   
                </ul>
            </div>
        </div>
    </div>





</div>

<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>



    
    
      
  
    </div>
    
        </div>
        
        
        
        @include('footer.footer')
        
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
           <script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>

        <script type="text/javascript" src="js/app2.js"></script>
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>