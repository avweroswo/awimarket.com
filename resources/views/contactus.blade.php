<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">

<link rel="stylesheet" href="css/home.css" />

<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
    
	<!-- End WOWSlider.com HEAD section -->
        
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"  type="text/css">

  <link rel="stylesheet" href="css/home.css" />  
</head>
<body>
    
    <div class="home-background" style='height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')
         
    <div class="page-header" style="background: url(assets/img/banner1.jpg);margin-top:120px;">
       <div class="container">
          <div class="row">
            <div class="col-md-12">
               <div class="breadcrumb-wrapper">
                    <h2 class="page-title">Contact Us</h2>
               </div>
            </div>
         </div>
      </div>
    </div>
         
         
    <section id="content" style="margin-bottom:50px;">
<div class="container">
<div class="row">
<div class="col-md-8">
<h2 class="title-2">
Love to hear from you
</h2>

<form  action='addDetails' method='post' class="" >
    
 @if(Session::get('success') == 1)
    
    <div style='color:blue;'>
        Edit Successful
    </div>
    
    <?php Session::put('success',null); ?>
    
    
    
    @endif
    
  
 
    <div style='color:red;'>
      @if(isset($errors)) 
         @foreach($errors->all() as $error)
            {{$error}}<BR>
         @endforeach
      @endif
      
      
       </div>
      
  
   
    
           
                                   
   
    <br>
    
    @csrf
<div class="row">
<div class="col-md-12">
<div class="row">
<div class="col-md-12">
<div class="form-group">
<input type="text" class="form-control" id="name" name="fullname" placeholder="Full Name"  data-error="Please enter your name">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<input type="text" class="form-control" name='email' id="email" placeholder="you@yoursite.com"  data-error="Please enter your email">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" data-error="Please enter your subject">
<div class="help-block with-errors"></div>
</div>
</div>
</div>
</div>
<div class="col-md-12">
<div class="row">
<div class="col-md-12">
<div class="form-group">
<textarea class="form-control" name='message' placeholder="Message" rows="10" data-error="Write your message" ></textarea>
 <div class="help-block with-errors"></div>
</div>
</div>
</div>
</div>
<div class="col-md-12">
<button type="submit"  class="btn click-add" style='z-index:7000'>Send Your Message</button>
<div id="msgSubmit" class="h3 text-center hidden"></div>
<div class="clearfix"></div>
</div>
</div>
</form>
</div>
<div class="col-md-4">
<h2 class="title-2">
Contact Information
</h2>
<div class="information">
<div class="contact-datails">
<div class="icon">
<i class="fa fa-map-marker icon-radius"></i>
</div>
<div class="info">
<h3>Address</h3>
<span class="detail">Main Office: NO.22-23 Street Name- City,Country</span>
<span class="datail">Customer Center: NO.130-45 Streen Name- City, Country</span>
</div>
</div>
<div class="contact-datails">
<div class="icon">
<i class="fa fa-phone icon-radius"></i>
</div>
<div class="info">
<h3>Phone Numbers</h3>
<span class="detail">Main Office: +880 123 456 789</span>
<span class="datail">Customer Supprort: +880 123 456 789 </span>
</div>
</div>
<div class="contact-datails">
<div class="icon">
<i class="fa fa-location-arrow icon-radius"></i>
</div>
<div class="info">
<h3>Email Address</h3>
<span class="detail">Customer
Support: info&copy;mail.com</span>
<span class="detail">Technical Support: support&copy;mail.com</span>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
       <script type="text/javascript" src="js/app2.js"></script>  
        @include('footer.footer')
        
        
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>