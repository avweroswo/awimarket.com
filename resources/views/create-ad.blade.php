<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">



<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<!-- <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css"> -->

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css"type="text/css"> 

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  #draggable { width: 150px; height: 150px; padding: 0.5em; }
  </style>
  
    
	<!-- End WOWSlider.com HEAD section -->
        
<script type="text/javascript" src="assets/js/jquery-min.js"></script> 
<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href="http://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="http://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  type="text/css">

<link rel="stylesheet" href="css/home.css" />
    
</head>
<body>
    
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1631938846852679',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

    
    <div class="home-background" style='min-height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')
         
         @include('user-header.user-header')


  


          
        <div class="row" style="background-color:white;overflow:auto;padding-bottom:100px;">  
            <div class="col-sm-12 " style="">
                <div class="margin-top-20 border-dashboard clearfix dd-m-width-90" style="width:50%;margin:0 auto;overflow:auto;padding:20px;">   
                    <div style="margin-bottom:40px;border-bottom:1px solid #D3D3D3;min-height:40px; ">  
                         <div class="text-color-purple pull-right"> 
                             <button class="click-properties border-radius-10 click-properties">HOW TO SELL</button>
                        </div>
                    </div>
                    <div class="row">
                                  @if(Session::get('er1') == 1)        
                            <div style="background-color:red;color:white;" >
                                
                                  @if(isset($errors)) 
         @foreach($errors->all() as $error)
            {{$error}}<BR>
         @endforeach
      @endif
      
                                
                            </div>
                            @elseif(Session::get('er1') == 2)
                            
                            <div style="background-color:blue;color:white;" >
                                Successful
                            </div>
                            
                            
                            
                            
                    @endif
                        <form id="Ad-form" action='createAd1' method='get'>

                                  @csrf
                        <div id="ad-error" style="color:red;">
                            
                        </div>
                                  <br>
                                                          
                        <div class="col-sm-12">
                            <span><strong>Photos</strong></span> <span class="text-red">*</span>
                        </div>
                        <div class="col-sm-12">
                            <span><strong>Ads with photo get 5x more clients.</strong></span> <span>Accepted formats are .jpg, .gif and .png. Max allowed size for uploaded files is 5MB. Add at least one photo if you post car or phone. </span>
                        </div>
                        
                        <div class="col-sm-12 margin-top-10 " style="overflow:auto;">
                            <div class='sortable' style="width:300%;height:100px;">
                              <div class="create-ad-image-box border-radius-10 text-center purple-border ak " data-value='' style="   background-size: 100% 150%; @if(isset($ads->id))@if(isset($obj->getAllPhoto($ads->id)[0]->pic_name)) background-image:url('ad_photo/{{$obj->getAllPhoto($ads->id)[0]->pic_name}}') @endif @endif"   >
                                 <i class="fa fa-camera" aria-hidden="true" style="font-size:30px;"></i> 
                              </div>
                              <div class="create-ad-image-box border-radius-10 text-center " data-value='' style="   background-size: 100% 150%; @if(isset($ads->id))@if(isset($obj->getAllPhoto($ads->id)[1]->pic_name)) background-image:url('ad_photo/{{$obj->getAllPhoto($ads->id)[1]->pic_name}}') @endif @endif" >
                                 <i class="fa fa-camera" aria-hidden="true" style="font-size:30px;"></i> 
                              </div>
                              <div class="create-ad-image-box border-radius-10 text-center" data-value='' style="   background-size: 100% 150%; @if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[2]->pic_name)) background-image:url('ad_photo/{{$obj->getAllPhoto($ads->id)[2]->pic_name}}') @endif @endif" >
                                  <i class="fa fa-camera" aria-hidden="true" style="font-size:30px;"></i>                                
                              </div>
                              <div class=" create-ad-image-box border-radius-10 text-center" data-value='' style="   background-size: 100% 150%;@if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[3]->pic_name)) background-image:url('ad_photo/{{$obj->getAllPhoto($ads->id)[3]->pic_name}}') @endif @endif" >
                                  <i class="fa fa-camera" aria-hidden="true" style="font-size:30px;"></i>                                
                              </div>
                              <div class=" create-ad-image-box border-radius-10 text-center" data-value='' style="   background-size: 100% 150%;@if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[4]->pic_name)) background-image:url('ad_photo/{{$obj->getAllPhoto($ads->id)[4]->pic_name}}') @endif @endif" >
                                  <i class="fa fa-camera" aria-hidden="true" style="font-size:30px;"></i>                                
                              </div>
                              
                             
                              </div>

                                 
                  
                              
                        
                        <div class="col-sm-12" >
                            <strong>
                                First picture - is the title picture.
                            </strong>
                            You can change the order of photos; just grab your photos and drag
                        </div>
                        
                        <div class="col-sm-12" style="margin-bottom:10px;" >
                             <span><strong>Title</strong></span> <span class="text-red">*</span>
                             <input type="text" name="title" value="{{isset($ads->title) ? $ads->title:''}}" class="form-control grey-border title-text f-c" placeholder="Please write a clear title for your item" />
                             <div class="label-char" ></div>
                        </div>
                        
                        <div class="col-sm-12" >
                            <div class="col-sm-6">
                             <span><strong>Category</strong></span> <span class="text-red">*</span>
                             <select name="category" class="form-control selectpicker f-c category" data-live-search="true">
                                 <option value='' name="category">Select</option>
                                 @foreach($categories as $values)
                                 <option value='{{$values->id}}' >{{$values->cat_name}}</option>
                                 @endforeach
                             </select>
                             
                              
                              
                              
                             
                            </div>
                            <div class="col-sm-6">
                             <span><strong>Subcategory</strong></span> <span class="text-red">*</span>
                             <select id="categories" name="subcategory" class="form-control selectpicker f-c categories" data-live-search="true">
                                
                                 
                             </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-12" id="sub-categories" >
                            
                          
                        </div>
                                             
                                             
                        @if(isset($ads->category))
                              
                                  <script>
                                      
                                    //  alert({{$ads->negotiable}});
                                      
                                        $(window).on('load', function() {

                                         //$('.category').change();
                                         
                                          $.ajax({
             method: "get",
            url: "getSub/"+{{$ads->category}},
            data: {   },
     
            success: function(data) {
                
                str = data.split(';;');   
                
                str_id = str[0].split(',');
                
               
                
                str_name = str[1].split(',');
           
          
                text = '';
                
                for(i=0;i<str_id.length;i++)
                {
                     
                                        
                   text += '<option value="'+str_id[i]+'">'+str_name[i]+'</option>';
                }
                
                $('#categories').html('<option value="" >Select</option>'+text);
                
                $('select').selectpicker('render');
                
                $('select').selectpicker('refresh');
                
                
           
            }
        });
  
                                         
                                          $('.category').val({{$ads->category}}).change();
                                          
                                       //   $('.categories').val({{$ads->subcategory}}).change();
                                          
                                          $.ajax({
             method: "get",
            url: "getSub2/"+{{$ads->subcategory}},
            data: {   },
     
            success: function(data) {
                
                str = data.split('))');   
                
                str_name = str[0].split(',');
                
               
                
                str_id = str[1].split(',');
           
          
                text = '';
                
                j = str_id.length-1;
                
                //x =0;
                
                for(i=0;i<str_id.length;i++)
                {
                    
                    
                    
                    if(str_id.length%2 !== 0 && i == j   )
                    {
                        text += '<div class="col-sm-12" ><input id="sub_'+str_id[i]+'"  class="form-control" name="sub_'+str_id[i]+'" value="" placeholder="'+str_name[i]+'" /></div>';
                        break;
                    }
                     
                        text += '<div class="col-sm-6" ><input name="sub_'+str_id[i]+'" id="sub_'+str_id[i]+'"  class="form-control" value="" placeholder="'+str_name[i]+'" /></div>';  
                        
                        
                   
                }
                
                
                
                $('#sub-categories').html(text+'<input type="text" id="sub_max" name="sub_max" value="'+Math.max(...str_id)+'" style="visibility:hidden" />');
                
               // $('select').selectpicker('render');
                
               // $('select').selectpicker('refresh');
                
                $('.categories').val({{$ads->subcategory}}).change();
                
                
                
                       <?php $a=0; ?>
                                @if(isset($ads->category))
                                @foreach($obj->getSubsAll($ads->category) as $value)
                            
                                    $('#sub_{{$value->id}}').val('{{$obj->getSubValue($value->id)}}');
                                    
                                  
                                    
                                   
                                   
                                @endforeach
                                @endif
                                
                                $('#sub_max').val({{$obj->maxSubID($ads->category)}});
                
                
           
            }
        });
                                          
                                          
                                         
                                          
                                      
                                
                                    
                                        });
                                         
                                        // $('.categories').change();


                                  </script>
                              
                        @else
                        
                        <script>
                            
                            $('.category').on('change', function() { 
                
                
                 $.ajax({
             method: "get",
            url: "getSub/"+$(this).val(),
            data: {   },
     
            success: function(data) {
                
                str = data.split(';;');   
                
                str_id = str[0].split(',');
                
               
                
                str_name = str[1].split(',');
           
          
                text = '';
                
                for(i=0;i<str_id.length;i++)
                {
                     
                                        
                   text += '<option value="'+str_id[i]+'">'+str_name[i]+'</option>';
                }
                
                $('#categories').html('<option value="" >Select</option>'+text);
                
                $('select').selectpicker('render');
                
                $('select').selectpicker('refresh');
                
                
           
            }
        });
  
                    
      return false;
      
     
               
            });
            
            
                        
                               $('#categories').on('change', function() { 
                
               // alert('HI');
                
                 $.ajax({
             method: "get",
            url: "getSub2/"+$(this).val(),
            data: {   },
     
            success: function(data) {
                
                str = data.split('))');   
                
                str_name = str[0].split(',');
                
               
                
                str_id = str[1].split(',');
           
          
                text = '';
                
                j = str_id.length-1;
                
                //x =0;
                
                for(i=0;i<str_id.length;i++)
                {
                    
                    
                    
                    if(str_id.length%2 !== 0 && i == j   )
                    {
                        text += '<div class="col-sm-12" ><input id="sub_'+str_id[i]+'"  class="form-control" name="sub_'+str_id[i]+'" value="" placeholder="'+str_name[i]+'" /></div>';
                        break;
                    }
                     
                        text += '<div class="col-sm-6" ><input name="sub_'+str_id[i]+'" id="sub_'+str_id[i]+'"  class="form-control" value="" placeholder="'+str_name[i]+'" /></div>';  
                        
                        
                   
                }
                
                 $('#sub-categories').html(text+'<input type="text" id="sub_max" name="sub_max" value="'+Math.max(...str_id)+'" style="visibility:hidden" />');
                
                 $('select').selectpicker('render');
                
                 $('select').selectpicker('refresh');
                
                 $('.categories').val($(this).val()).change();
                
                
           
            }
        });
  
                    
      return false;
      
     
               
            });
            
   </script>                     
                            
                              
                        @endif
                       
                        <div class="col-sm-12" >
                            <div class="col-sm-6">
                                <span><strong>Price</strong></span> <span class="text-red">*</span><span style="font-size:11px;color:#D3D3D3;">&nbsp; Numbers only. No need for Comma</span>
                                <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'>&#8358;</span>
                                     <input id="phone" type="text" class="form-control f-c" name="price" value="@if(isset($ads->price)){{$ads->price}} @endif" placeholder="Enter price"  />
                                </div>
                            </div>
                            <div class="col-sm-6" style="padding-top:33px;">
                                <input type="checkbox" name='negotiable' value='1' @if(isset($ads->negotiable)) @if($ads->negotiable == 1) checked @endif @endif/>&nbsp;&nbsp;Negotiable
                            </div>
                        </div>
                        
                        <div class="col-sm-12 margin-top-10" >                           
                            <span><strong>Description</strong></span> <span class="text-red">*</span><br>
                            <textarea class="form-control f-c" name="details" col="20" row="500" placeholder="Please provide a detailed description.You can mention as many details as possible.It will make your ad more attractive for buyers">@if(isset($ads->details)){{$ads->details}}@endif</textarea>
                        </div>
                        
                        <div class="col-sm-12 margin-top-10" >                           
                            <strong>Contact Information</strong>
                        </div>
                        
                        <div class="col-sm-12 margin-top-10" >                           
                            <div class="col-sm-6">
                                <span><strong>Region</strong></span> <span class="text-red">*</span>
                                <select name="region" class="form-control selectpicker f-c" data-live-search="true" >
                                    <option value='' >Choose</option>
                                    @foreach($all_cities as $val)
                                       <option value='{{$val->id}}' @if(isset($ads->region)) @if($val->id == $ads->region ) Selected @endif @endif>{{$val->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-sm-6">
                                <span><strong>Your phone</strong></span> <span class="text-red">*</span>
                                <input type="text" name="phone" value="@if(isset($ads->phone)) {{$ads->phone}} @endif" class="form-control f-c" placeholder="Please enter your phone" />
                            </div>
                        </div>
                
                @if(Session::get('edit_a') == null) 
                    @if(!isset($ads->category))
                        
                        <div class="col-sm-12">
                           <div class='col-sm-4'>
                            <input type="radio" name="share" value="fb" />&nbsp;&nbsp;Share on Facebook 
                           </div>
                           <div class='col-sm-4'>
                            <input type="radio" name="share" value="tw" />&nbsp;&nbsp;Share on Twitter 
                           </div>
                           <div class='col-sm-4'>
                            <input type="radio" name="share" value="go" />&nbsp;&nbsp;Share on Google
                           </div>
                            <br>
                            Your ad will be shared after moderation
                        </div>
                    
                    @endif
                 @endif
                    
                    <input type='text' name='ad_id' value='@if(isset($ads->id)){{$ads->id}}@endif' style='visibility:hidden;' />
                        
                        <div class="col-sm-12" style="margin-top:20px;">
                            <div class="color-grey" style="margin:0 auto;width:50%;padding:10px;">
                                <div class="text-center" style="font-size:10px;">
                                    By publishing an ad you agree and accept <span class="text-color-purple">the Rules of</span> 
                                    
                                </div>
                                <div class="text-center" >
                                    <br/>
                                    @if(!isset($ads->category))
                                    <button type='button' class="text-color-white btn btn-warning color-purple click-add create-ad-btn" style="font-size:10px;"><i class="fa fa-spinner fa-spin create-ad-spin hide" aria-hidden="true"></i>&nbsp;Post FREE Ad</button>
                                    @else
                                    
                                    <button type='button' class="text-color-white btn btn-warning color-purple click-add edit-ad-btn" style="font-size:10px;"><i class="fa fa-spinner fa-spin create-ad-spin hide" aria-hidden="true"></i>&nbsp;Post FREE Ad</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                                             
                        <div style="display:none;">
                            
                            <input type='text' id='pic1' name='pic1' value='@if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[0]->pic_name)) {{$obj->getAllPhoto($ads->id)[0]->pic_name}} @endif @endif'  />
                        
                            <input type='text' id='pic2' name='pic2' value='@if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[1]->pic_name)) {{$obj->getAllPhoto($ads->id)[1]->pic_name}} @endif @endif'  />
                        
                            <input type='text' id='pic3' name='pic3' value='@if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[2]->pic_name)) {{$obj->getAllPhoto($ads->id)[2]->pic_name}} @endif @endif'  />
                        
                            <input type='text' id='pic4' name='pic4' value='@if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[3]->pic_name)){{$obj->getAllPhoto($ads->id)[3]->pic_name}} @endif @endif'   />
                        
                            <input type='text' id='pic5' name='pic5' value='@if(isset($ads->id)) @if(isset($obj->getAllPhoto($ads->id)[4]->pic_name)){{$obj->getAllPhoto($ads->id)[4]->pic_name}} @endif @endif' />
                                             
                        
                        
                        </div>
                       
                       
                        
                        
                        </form>   
                         
                       
                    </div>
                </div> 
            </div>
        </div>

    <form id='form22' enctype='multipart/form-data' style="display:none;">  
                                  <meta name="csrf-token" content="{{ csrf_token() }}">
                                  
                                      <input type="file" name="photo" class="img-upload1" accept=".png,.jpg,.gif"/>
                               </form>
                             


                    <!--fin buttons -->



<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>


<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>

     
        @include('footer.footer')
        
        <?php Session::put('er1',null); ?>
        
        
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>