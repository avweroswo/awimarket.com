<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title>Admin Dashboard</title>

        <!--Morris Chart CSS -->
		<link rel="stylesheet" href="admin/assets/plugins/morris/morris.css">

        <link href="admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        
        
        
        <link rel="stylesheet" href="css/home.css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="admin/assets/js/modernizr.min.js"></script>
        
        <link href="admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="admin/assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
         

    </head>


    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="#" class="logo"><span>Admin<span> Dashboard</span></span></a>
                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">

                    </div>

                </div>
            </div>

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                          
                        </ul>
                        <!-- End navigation menu  -->
                    </div>
                </div>
            </div>
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            
           

<!-- Modal -->

            <div class="container">
                
                <?php $searchvalue = ''; $count_searches=''; ?>
                
                @foreach(Session::get('Stat')[0] as $val)
                
                
                     <?php $searchvalue .= $val->searchvalue.','; ?>
         
                
                     <?php $count_searches .= $val->searches_count.','?>
                
                @endforeach
                
                <?php  
                
                      $count_searches = substr($count_searches,0,strlen($count_searches)-1); 
                
                       $searchvalue = substr($searchvalue,0,strlen($searchvalue)-1);
                
                ?>
              
                <div id='topSearchLabel' style='display:none;'>
                             <?php echo $searchvalue;  ?>
                </div>
                
            
                  <div id='mintopSearch' style='display:none;'>
                     <?php echo  min(explode(',',$count_searches));  ?>
                  </div>
                
                  <div id='maxtopSearch' style='display:none;'>
                     <?php echo max(explode(',',$count_searches));  ?>
                  </div>
                
                  <div id='topSearchValue' style='display:none;'>
                     <?php echo $count_searches; ?>
                  </div>
                
                  <?php $searchvalue = ''; $count_searches=''; ?>
                
                @foreach(Session::get('Stat')[1] as $val)
                
                
                     <?php $searchvalue .= $val->searchvalue.','; ?>
         
                
                     <?php $count_searches .= $val->searches_count.','?>
                
                @endforeach
                
                <?php  
                
                      $count_searches = substr($count_searches,0,strlen($count_searches)-1); 
                
                       $searchvalue = substr($searchvalue,0,strlen($searchvalue)-1);
                
                ?>
              
                <div id='failedSearchLabel' style='display:none;'>
                             <?php echo $searchvalue;  ?>
                </div>
                
            
                  <div id='minfailedSearch' style='display:none;'>
                     <?php echo  min(explode(',',$count_searches));  ?>
                  </div>
                
                  <div id='maxfailedSearch' style='display:none;'>
                     <?php echo max(explode(',',$count_searches));  ?>
                  </div>
                
                  <div id='failedSearchValue' style='display:none;'>
                     <?php echo $count_searches; ?>
                  </div>
                
                
                  <?php $searchvalue = ''; $count_searches=''; ?>
                
                @foreach(Session::get('Stat')[2] as $val)
                
                
                     <?php $searchvalue .= $val->users.','; ?>
         
                
                     <?php $count_searches .= $val->searches_count.','?>
                
                @endforeach
                
                <?php  
                
                      $count_searches = substr($count_searches,0,strlen($count_searches)-1); 
                
                       $searchvalue = substr($searchvalue,0,strlen($searchvalue)-1);
                
                ?>
              
                <div id='TopSellerLabel' style='display:none;'>
                             <?php echo $searchvalue;  ?>
                </div>
                
            
                  <div id='minTopSeller' style='display:none;'>
                     <?php echo  min(explode(',',$count_searches));  ?>
                  </div>
                
                  <div id='maxTopSeller' style='display:none;'>
                     <?php echo max(explode(',',$count_searches));  ?>
                  </div>
                
                  <div id='TopSellerValue' style='display:none;'>
                     <?php echo $count_searches; ?>
                  </div>
                
                    <?php $searchvalue = ''; $count_searches=''; ?>
             
                     <?php $searchvalue = 'Sellers'; ?>
         
                     <?php $count_searches = sizeof(Session::get('Stat')[3]); ?>
              
              
              
                <div id='NewSellerLabel' style='display:none;'>
                             <?php echo $searchvalue;  ?>
                </div>
                
            
                  <div id='minNewSeller' style='display:none;'>
                     <?php echo  0;  ?>
                  </div>
                
                  <div id='maxNewSeller' style='display:none;'>
                     <?php echo $count_searches;  ?>
                  </div>
                
                  <div id='NewSellerValue' style='display:none;'>
                     <?php echo $count_searches; ?>
                  </div>
                
                       <div class="row" style="margin-bottom:50px;">
                                <div class="col-lg-12">
                                    
                                    <div class="col-lg-12" style='margin-bottom:10px;' >
                                        <a href='AdminLogout' class='pull-right'>Logout</a>
                                    </div>
                                    
                                    <div class="col-lg-12" style='margin-bottom:50px;' >
                                        <form method='get' action='admindashboard' >
                                      <fieldset>
                                        <legend>Chart Search Filter</legend>
                                        <select name='week' style='display:inline;width:20%;' class="form-control" title="week">
                                            <option>1</option>
                                            <option value='8'>2</option>
                                            <option value='15'>3</option>
                                            <option value='23'>4</option>
                                        </select>
                                        
                                
                                        
                                         <select name='month' style='display:inline;width:20%;' class="form-control" title="Month">
                                          
                                          <?php $a=0; while($a<12){ ?>
                                                <?php $a++; ?>
                                            <?php echo '<option>'.$a.'</option>' ; ?>
                                            
                                          <?php } ?>  
                                           
                                        </select>
                                        
                                         <select name='year'  style='display:inline;width:20%;' class="form-control" title="Year">
                                          
                                          <?php $a=2017; while($a<2100){ ?>
                                                <?php $a++; ?>
                                            <?php echo '<option>'.$a.'</option>' ; ?>
                                            
                                          <?php } ?>  
                                           
                                        </select>
                                        <button class='btn btn-primary' style='display:inline;' type='submit'>Search</button>  
                                         </fieldset>
                                            
                                        </form>
                                        <br>
                                        @if(Request::get('month')!=null)
                                        
                                        <H2>Search Result For :- &nbsp; <b>Month</b> : {{Request::get('month')}} &nbsp; 
                                        
                                            <b>Year </b> : {{Request::get('year')}} 
                                        
                                        &nbsp;
                                        
                                        <b>Week</b> : {{Request::get('week')}} </H2>
                                        
                                        @endif
                                    </div>

                                      <div class="col-lg-6" >
                        <div class="card-box" style='overflow:auto;'>
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                  
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Top Searches</h4>

                            <canvas id="topSearches" height="300"></canvas>

                        </div>
                    </div><!-- end col-->
                    
                                      <div class="col-lg-6" >
                        <div class="card-box" style='overflow:auto;'>
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                             
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Failed Searches</h4>

                            <canvas id="failedSearches" height="300"></canvas>

                        </div>
                    </div><!-- end col-->
                    
                    <div class="col-lg-6" >
                        <div class="card-box" style='overflow:auto;'>
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                   
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Top Seller</h4>

                            <canvas id="TopSeller" height="300"></canvas>

                        </div>
                    </div><!-- end col-->
                    
                         <div class="col-lg-6" >
                        <div class="card-box" style='overflow:auto;'>
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                   
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">New Seller</h4>

                            <canvas id="NewSeller" height="300"></canvas>

                        </div>
                    </div><!-- end col-->
                                </div><!-- end col -->

                            </div>
                
                       <div class="row" style="margin-bottom:50px;">
                                <div class="col-lg-12">

                                    <ul class="nav nav-tabs">
                                        <li role="presentation" {{Session::get('photo_error') !== null || Session::get('bill_edit')!== null || Session::get('photo_error_side')!== null || Session::get('side_edit')!== null || Session::get('nav_error')!== null || Session::get('nav_edit')!== null || Session::get('photo_error_logo')!== null   ? '' : 'class=active'}} >
                                            <a href="#home11" role="tab" data-toggle="tab">Users</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#profile1" role="tab" data-toggle="tab">Contact Us</a>
                                        </li>
                                        
                                        <li role="presentation" {{Session::get('photo_error') !== null  ? 'class=active' : ''}}>
                                            <a href="#billboard" role="tab" data-toggle="tab">Slider (Billboard)</a>
                                        </li>
                                        
                                        <li role="presentation" {{Session::get('bill_edit')!== null  ? 'class=active' : ''}} >
                                            <a href="#all_billboard" role="tab" data-toggle="tab">All Billboards</a>
                                        </li>
                                        
                                         <li role="presentation" {{Session::get('photo_error_side') !== null  ? 'class=active' : ''}}>
                                            <a href="#sidebar1" role="tab" data-toggle="tab">Slider (Sidebar)</a>
                                        </li>
                                        
                                        <li role="presentation" {{Session::get('side_edit')!== null  ? 'class=active' : ''}} >
                                            <a href="#all_sidebar" role="tab" data-toggle="tab">All Sidebars</a>
                                        </li>
                                        
                                        <li role="presentation" {{Session::get('nav_error') !== null  ? 'class=active' : ''}}>
                                            <a href="#navigations1" role="tab" data-toggle="tab">Add Navigation</a>
                                        </li>
                                        
                                        <li role="presentation" {{Session::get('nav_edit')!== null  ? 'class=active' : ''}} >
                                            <a href="#all_navs" role="tab" data-toggle="tab">All Navigation</a>
                                        </li>
                                        
                                         <li role="presentation" {{Session::get('photo_error_logo')!== null  ? 'class=active' : ''}} >
                                            <a href="#logo1" role="tab" data-toggle="tab">Logo</a>
                                        </li>
                                        
                                        
                                        
                                        
                                        
                                       
                                    </ul>
                                    <div class="tab-content background-color-white">
                                        <div role="tabpanel" class="tab-pane fade  clearfix " id='all-ads' style="overflow:auto;">
                                            <table  class="table table-striped table-bordered th datatable" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                              <th>S/No</th>
                              <th>First name</th>
                              <th>Last name</th>
                              <th>Phone</th>
                              <th>Company name</th>
                              <th>Seller Url</th>
                              <th>Address</th>
                              <th>Reg. Date</th> 
                              <th>Edit</th>  
                              <th>Delete</th> 
                              <th>Verify</th> 
                            </tr>
                           </thead>
                           <tbody>
                          
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                              <th>Email</th>
                              <th>First name</th>
                              <th>Last name</th>
                              <th>Phone</th>
                              <th>Company name</th>
                              <th>Seller Url</th>
                              <th>Address</th>
                              <th>Reg. Date</th>
                              <th>Edit</th>  
                              <th>Delete</th> 
                              <th>Verify</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('photo_error') !== null || Session::get('bill_edit')!== null || Session::get('photo_error_side') !== null || Session::get('side_edit')!== null || Session::get('nav_error')!== null || Session::get('nav_edit')!== null || Session::get('photo_error_logo')!== null ? '' : 'in active'}} clearfix " id="home11" style="overflow:auto;">
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                              <th>Email</th>
                              <th>First name</th>
                              <th>Last name</th>
                              <th>Phone</th>
                              <th>Company name</th>
                              <th>Seller Url</th>
                              <th>Address</th>
                              <th>Reg. Date</th> 
                              <th>Edit</th>  
                              <th>Delete</th> 
                              <th>Verify</th> 
                            </tr>
                           </thead>
                           <tbody>
                            @foreach($all_users as $users)
                            
                            <tr>
                             
                              <td>{{$users->user_email}}</td>
                              <td>{{$users->firstname}}</td>
                              <td>{{$users->lastname}}</td>
                              <td>{{$users->phone}}</td>
                              <td>{{$users->company_name}}</td>
                              <td>{{$users->seller_url}}</td>
                              <td>{{$users->address}}</td>
                              <td>{{date('d-m-Y H:i:s',strtotime($users->created_at))}}</td> 
                              <td align="center"><i class="fa fa-pencil" style="cursor:pointer;" aria-hidden="true" data-toggle="modal" data-target="#exampleModal{{$users->id}}"></i></td> 
                              <td align="center"><a href="delete_user?id={{$users->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              <td>
                                  @if($users->verified == 0)
                                  
                                  <a href='verify_user?user_id={{$users->id}}'><button class="btn btn-primary">Verify</button>
                                      
                                  @else
                                      Verified
                                  @endif
                                
                                      
                                  
                              </td> 
                            </tr>
                            
                            
                            
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                              <th>Email</th>
                              <th>First name</th>
                              <th>Last name</th>
                              <th>Phone</th>
                              <th>Company name</th>
                              <th>Seller Url</th>
                              <th>Address</th>
                              <th>Reg. Date</th>
                              <th>Edit</th>  
                              <th>Delete</th> 
                              <th>Verify</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="profile1">
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                              <th>Full name</th>
                              <th>Email</th>
                              <th>Subject</th>
                              <th>Message</th>
                              <th>Date</th>
                            
                            </tr>
                           </thead>
                           <tbody>
                            @foreach($all_contact as $con)
                            
                            <tr>
                             
                              <td>{{$con->fullname}}</td>
                              <td>{{$con->email}}</td>
                              <td>{{$con->subject}}</td>
                              <td>{{$con->message}}</td>
                              <td>{{date('d-m-Y H:i:s',strtotime($con->created_at))}}</td>
                              
                            </tr>
                            
                         
                            
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                              <th>Full name</th>
                              <th>Email</th>
                              <th>Subject</th>
                              <th>Message</th>
                              <th>Date</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('photo_error')!== null  ? 'in active' : ''}}" id="billboard">
                                            <form method='post' action='add-billboard' enctype="multipart/form-data">
                                            
                                                 @if(Session::get('photo_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                                             
                                                 @endif

                                                 @if(Session::get('photo_error') == '2')
                                                    <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                       
                                                @csrf
                                                <input type='file' name="photo" />
                                                <br>
                                                <input class='form-control' type='text' name='hyperlink'  placeholder='Link e.g https://www.facebook.com' />
                                                <br>
                                                <button type="submit" class='btn btn-primary sub-bill'><i class="fa fa-spinner fa-spin admin-bill-spin hide" aria-hidden="true"></i>&nbsp;Add Billboard</button> 
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('bill_edit')!== null  ? 'in active' : ''}}" id="all_billboard" >
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                             <th>Picture</th>
                              <th>Hyperlink</th>
                              <th>Date</th> 
                              <th>Edit</th>
                              <th>Delete</th> 
                            
                            </tr>
                           </thead>
                           <tbody>
                            @foreach($all_billboard as $con)
                            
                            <tr>
                             
                              <td><a href="billboard/{{$con->picture}}">{{$con->picture}}</a></td>
                              <td>{{$con->link}}</td>                        
                              <td>{{date('d-m-Y H:i:s',strtotime($con->created_at))}}</td>
                              <td align="center"><i class="fa fa-pencil" style="cursor:pointer;" aria-hidden="true" data-toggle="modal" data-target="#exampleModala{{$con->id}}"></i></td> 
                              <td align="center"><a href="delete_billboards?id={{$con->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                            </tr>
                            
                            <div class="modal fade" id="exampleModala{{$con->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Edit Billboard</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <div id='errors-user' style='background-color: red;color:white;'>
                                             @if(Session::get('bill_edit') == '1')
                                              @foreach($errors->all() as $error)
                                                   {{$error}}<BR>
                                              @endforeach
                                             @endif
                                         </div>
                                          <form method="post" action="edit_billboard?id={{$con->id}}" enctype="multipart/form-data" >
                                             @csrf
                                             <input type="file" name="photo" class="form-control" /> 
                                             <br>
                                             <input type="text" name="hyperlink" class="form-control" value="{{$con->link}}" /> 
                                             <br>
                                             <button type='submit' class="editBill btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-edit-spin2 hide" aria-hidden="true"></i> &nbsp; Edit</button>
                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                     </div>
                                   </div>
                                </div>
  
                            </div>
                         
                            
                         
                            
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                               <th>Picture</th>
                              <th>Hyperlink</th>
                              <th>Date</th> 
                              <th>Edit</th>
                              <th>Delete</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('photo_error_side')!== null  ? 'in active' : ''}}" id="sidebar1">
                                            <form method='post' action='add-sidebar' enctype="multipart/form-data">
                                            
                                                 @if(Session::get('photo_error_side') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                                             
                                                 @endif

                                                 @if(Session::get('photo_error_side') == '2')
                                                    <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                       
                                                @csrf
                                                <input type='file' name="photo" />
                                                <br>
                                                <input class='form-control' type='text' name='hyperlink'  placeholder='Link e.g https://www.facebook.com' />
                                                <br>
                                                <button type="submit" class='btn btn-primary sub-side'><i class="fa fa-spinner fa-spin admin-side-spin hide" aria-hidden="true"></i>&nbsp;Add Sidebar</button> 
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('side_edit')!== null  ? 'in active' : ''}}" id="all_sidebar" >
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                             <th>Picture</th>
                              <th>Hyperlink</th>
                              <th>Date</th> 
                              <th>Edit</th>
                              <th>Delete</th> 
                            
                            </tr>
                           </thead>
                           <tbody>
                            @foreach($all_sidebars as $con)
                            
                            <tr>
                             
                              <td><a href="sidebar/{{$con->picture}}">{{$con->picture}}</a></td>
                              <td>{{$con->link}}</td>                        
                              <td>{{date('d-m-Y H:i:s',strtotime($con->created_at))}}</td>
                              <td align="center"><i class="fa fa-pencil" style="cursor:pointer;" aria-hidden="true" data-toggle="modal" data-target="#exampleModalside{{$con->id}}"></i></td> 
                              <td align="center"><a href="delete_sidebar?id={{$con->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                            </tr>
                            
                            <div class="modal fade" id="exampleModalside{{$con->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Edit Sidebar</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <div id='errors-user' style='background-color: red;color:white;'>
                                             @if(Session::get('side_edit') == '1')
                                              @foreach($errors->all() as $error)
                                                   {{$error}}<BR>
                                              @endforeach
                                             @endif
                                         </div>
                                          <form method="post" action="edit_sidebar?id={{$con->id}}" enctype="multipart/form-data" >
                                             @csrf
                                             <input type="file" name="photo" class="form-control" /> 
                                             <br>
                                             <input type="text" name="hyperlink" class="form-control" value="{{$con->link}}" /> 
                                             <br>
                                             <button type='submit' class="editBill btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-edit-spin2 hide" aria-hidden="true"></i> &nbsp; Edit</button>
                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                     </div>
                                   </div>
                                </div>
  
                            </div>
                         
                            
                         
                            
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                               <th>Picture</th>
                              <th>Hyperlink</th>
                              <th>Date</th> 
                              <th>Edit</th>
                              <th>Delete</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('nav_error')!== null  ? 'in active' : ''}}" id="navigations1">
                                            <form method='post' action='add-nav' enctype="multipart/form-data">
                                            
                                                 @if(Session::get('nav_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                                             
                                                 @endif

                                                 @if(Session::get('nav_error') == '2')
                                                    <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                       
                                                @csrf
                                                <input class='form-control' type='text' name='text'  placeholder='name' />
                                                <br>
                                                <input class='form-control' type='text' name='hyperlink'  placeholder='Link e.g https://www.facebook.com' />
                                                <br> 
                                                <input class='form-control' type='text' name='position'  placeholder='Position' />
                                                <br>
                                                <button type="submit" class='btn btn-primary nav-side'><i class="fa fa-spinner fa-spin admin-nav-spin hide" aria-hidden="true"></i>&nbsp;Add Nav</button> 
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('nav_edit')!== null  ? 'in active' : ''}}" id="all_navs" >
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                             <th>Name</th>
                              <th>Hyperlink</th>
                              <th>Position</th>
                              <th>Date</th> 
                              <th>Edit</th>
                              <th>Delete</th> 
                            
                            </tr>
                           </thead>
                           <tbody>
                            @foreach($all_navs as $con)
                            
                            <tr>
                             
                              <td>{{$con->text}}</td>
                              <td>{{$con->link}}</td> 
                              <td>{{$con->position}}</td>     
                              <td>{{date('d-m-Y H:i:s',strtotime($con->created_at))}}</td>
                              <td align="center"><i class="fa fa-pencil" style="cursor:pointer;" aria-hidden="true" data-toggle="modal" data-target="#exampleModalnav{{$con->id}}"></i></td> 
                              <td align="center"><a href="delete_nav?id={{$con->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                            </tr>
                            
                            <div class="modal fade" id="exampleModalnav{{$con->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Edit Nav</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <div id='errors-user' style='background-color: red;color:white;'>
                                             @if(Session::get('nav_edit') == '1')
                                              @foreach($errors->all() as $error)
                                                   {{$error}}<BR>
                                              @endforeach
                                             @endif
                                         </div>
                                          <form method="post" action="edit_nav?id={{$con->id}}" enctype="multipart/form-data" >
                                             @csrf
                                             <input type="text" name="text" class="form-control" value="{{$con->text}}" /> 
                                             <br>
                                             <input type="text" name="hyperlink" class="form-control" value="{{$con->link}}" /> 
                                             <br> 
                                             <input class='form-control' type='text' name='position' value="{{$con->position}}"  placeholder='Position' />
                                             <br>
                                             <button type='submit' class="editnav btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-nav-spin2 hide" aria-hidden="true"></i> &nbsp; Edit</button>
                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                     </div>
                                   </div>
                                </div>
  
                            </div>
                         
                            
                         
                            
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                              <th>Name</th>
                              <th>Hyperlink</th>
                              <th>Position</th>
                              <th>Date</th> 
                              <th>Edit</th>
                              <th>Delete</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{ Session::get('photo_error_logo')!== null  ? 'in active' : ''}}" id="logo1">
                                            <form method='post' action='add-logo' enctype="multipart/form-data">
                                            
                                                 @if(Session::get('photo_error_logo') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                                             
                                                 @endif

                                                 @if(Session::get('photo_error_logo') == '2')
                                                    <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                       
                                                @csrf
                                                <input type='file' name="photo" />
                                                <br>
                                                <button type="submit" class='btn btn-primary sub-logo'><i class="fa fa-spinner fa-spin admin-logo-spin hide" aria-hidden="true"></i>&nbsp;Add Logo</button> 
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div><!-- end col -->


                              

                            </div>
                
                       <div class="row" style="margin-bottom:50px;">
                                <div class="col-lg-12">

                                    <ul class="nav nav-tabs">
                                        <li role="presentation" {{ Session::get('tag_error')!== null || Session::get('photo_error_side')!== null || Session::get('side_edit')!== null || Session::get('nav_error')!== null || Session::get('nav_edit')!== null || Session::get('photo_error_logo')!== null || Session::get('city_error')!== null || Session::get('admin_error')!== null  ? '' : 'class=active'}} >
                                            <a href="#home12" role="tab" data-toggle="tab">Add Categorys</a>
                                        </li>
                                        
                                         <li role="presentation"  >
                                            <a href="#all_categorys" role="tab" data-toggle="tab" >All Categorys</a>
                                        </li>
                                        
                                        <li role="presentation" {{Session::get('tag_error')!== null  ? 'class=active' : ''}}  >
                                            <a href="#tags" role="tab" data-toggle="tab" >Add Tags</a>
                                        </li>
                                        
                                        <li role="presentation"  >
                                            <a href="#all_tags" role="tab" data-toggle="tab">All Tags</a>
                                        </li>
                                        
                                         <li role="presentation" {{Session::get('admin_error')!== null  ? 'class=active' : ''}} >
                                            <a href="#add-admin" role="tab" data-toggle="tab" >Add Admin</a>
                                        </li>
                                        
                                        <li role="presentation"  >
                                            <a href="#all_admin" role="tab" data-toggle="tab">All Admin</a>
                                        </li>
                                        
                                        <li role="presentation"  {{ Session::get('city_error')!== null  ? 'class=active' : ''}}>
                                            <a href="#add_city" role="tab" data-toggle="tab" >Add City</a>
                                        </li>
                                        
                                        <li role="presentation"  >
                                            <a href="#all_city" role="tab" data-toggle="tab">All City</a>
                                        </li>
                                        
                                         <li role="presentation" >
                                             <a href="#a-ds" role="tab" data-toggle="tab">All Ads</a>
                                        </li>
                                        
                                        <li role="presentation" >
                                             <a href="#reported-sellers" role="tab" data-toggle="tab">Reported Sellers</a>
                                        </li>
                                    
                                    </ul>
                                    <div class="tab-content background-color-white">
                                        
                                  <div role="tabpanel" class="tab-pane fade  clearfix " id='a-ds' style="overflow:auto;">
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                              <th>S/No</th>
                              <th>title</th>
                              <th>seller</th>
                              <th>category</th>
                              <th> Delete  </th>
                             
                            </tr>
                           </thead>
                           <tbody>
                               <?php $a=0; ?>
                               @foreach($all_ads as $v)
                               <tr>
                                   <?php $a++; ?>
                                   <td>{{$a}} </td>
                                   <td>{{$v->title}}</td>
                                   <td>{{$obj->getSellerName($v->user_id)}}</td>
                                   <td>{{$obj->getCategoryName($v->category)}} </td>
                                  
                                   <td align="center"><a href="delete_ads?id={{$v->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                               </tr>
                              
                               @endforeach
                          
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                               <th>S/No</th>
                              <th>title</th>
                              <th>seller</th>
                              <th>category</th>
                              <th> Delete  </th>
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        
                                        <div role="tabpanel" class="tab-pane fade  clearfix " id='reported-sellers' style="overflow:auto;">
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                              <th>S/No</th>
                              <th>seller</th>
                              <th>report reason</th>
                              <th>report description</th>
                            
                              <th> Delete  </th>
                             
                            </tr>
                           </thead>
                           <tbody>
                               <?php $a=0; ?>
                               @foreach($reports as $v)
                                   <?php $a++; ?>
                                   <td>{{$a}} </td>
                                   <td>{{$obj->getSellerName($v->seller)}}</td>
                                   <td>{{$v->report_reason}}</td>
                                   <td>{{$v->reportDesc}} </td>
                                  
                                   <td align="center"><a href="delete_report?id={{$v->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                              
                               @endforeach
                          
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                               <th>S/No</th>
                              <th>seller</th>
                              <th>report reason</th>
                              <th>report description</th>
                            
                              <th> Delete  </th>
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade  clearfix  {{Session::get('city_error') == null ? '' : 'in active'}}" id="add_city" style="overflow:auto;">
                                            <form method="post" action="add_city" enctype='multipart/form-data' >
                                                @csrf
                                                 @if(Session::get('city_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                          
                                                 @endif

                                                 @if(Session::get('city_error') == '2')
                                                                       <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                                 <input type="text" name="city" placeholder="City" class="form-control" value="{{Session::get('city')}}" />                                
                                                 <br>
                                                 <input type="file" name="city_pic"  />
                                                 <br>
                                                <button type='submit' class="citycat btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-city-spin hide" aria-hidden="true"></i> &nbsp; Submit</button>
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('tag_error') !== null || Session::get('admin_error')!== null || Session::get('city_error') !== null ? '' : 'in active'}} clearfix " id="home12" style="overflow:auto;">
                                            <form method="post" action="add_category" >
                                                @csrf
                                                 @if(Session::get('cat_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                          
                                                 @endif

                                                 @if(Session::get('cat_error') == '2')
                                                                       <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                                <input type="text" name="category" placeholder="Category" class="form-control" value="{{Session::get('category')}}" />
                                                <br>
                                                <input type="text" name="Subcategory" placeholder="SubCategory" class="form-control" value="{{Session::get('subcategory')}}" />
                                                <br>
                                                <input type="text" name="Subs" placeholder="SubCategory" class="form-control" value="{{Session::get('subs')}}" />
                                                <br>
                                                <input type="text" name="icon_data" placeholder="Icon Data" class="form-control" value="" />
                                                <br>
                                                <button type='submit' class="subcat btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-cat-spin hide" aria-hidden="true"></i> &nbsp; Submit</button>
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('tag_error') == null ? '' : 'in active'}} clearfix " id="tags" style="overflow:auto;">
                                            <form method="post" action="add_tags" >
                                                @csrf
                                                 @if(Session::get('tag_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                          
                                                 @endif

                                                 @if(Session::get('tag_error') == '2')
                                                                       <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                               
                                                <input type="text" name="tag" placeholder="Tags" class="form-control" value="" />
                                                <br>
                                                <button type='submit' class="sub-tag btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-tag-spin hide" aria-hidden="true"></i> &nbsp; Submit</button>
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade {{Session::get('admin_error') == null ? '' : 'in active'}} clearfix " id="add-admin" style="overflow:auto;">
                                            <form method="post" action="add_admin" >
                                                @csrf
                                                 @if(Session::get('admin_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                          
                                                 @endif

                                                 @if(Session::get('admin_error') == '2')
                                                                       <div style='background-color:blue;color:white;'>
                                                        Successful         
                                                      
                                                    </div>
                                             
                                                 @endif

                                                 <br>
                                               
                                                <input type="text" name="username" placeholder="Username" class="form-control" value="" />
                                                <br>
                                                <input type="text" name="password" placeholder="Password" class="form-control" value="" />
                                                <br>
                                                <button type='submit' class="sub-admin btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-admin-spin hide" aria-hidden="true"></i> &nbsp; Submit</button>
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="all_city" >
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                                
                             <th>S/No</th>
                             
                             <th>city</th>
                             
                             <th>picture</th>
                             
                              <th>Created At</th>
                              
                              <th>Edit</th> 
                           
                              <th>Delete</th> 
                            
                            </tr>
                           </thead>
                           <tbody>
                               <?php $cat_iterator = 0; ?>
                            @foreach($all_cities as $con)
                            
                            <?php $cat_iterator++; ?>
                            
                            <tr>
                              <td>{{$cat_iterator}}</td>
                              <td>{{$con->city}}</td>
                              <td>
                                  
                                  @if($con->pic_name !== '')
                                  
                                     <a href='city_pic/{{$con->pic_name}}'>{{$con->pic_name}}</a>
                                  
                                  @endif
                                  
                              </td>
                              <td>{{date("d-m-Y H:i:s",strtotime($con->created_at))}}</td> 
                              <td align="center"><i class="fa fa-pencil" style="cursor:pointer;" aria-hidden="true" data-toggle="modal" data-target="#exampleModalcity{{$con->id}}"></i></td>                             
                              <td align="center"><a href="delete_city?id={{$con->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                            </tr>
                            
                              
                         
                       
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                                     <th>S/No</th>
                             
                             <th>city</th>
                             
                             <th>picture</th>
                             
                              <th>Created At</th>
                              
                              <th>Edit</th> 
                           
                              <th>Delete</th>
                              
                              
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="all_admin" >
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                                
                             <th>S/No</th>
                             <th>Username</th>
                             
                              <th>Created At</th>
                           
                              <th>Delete</th> 
                            
                            </tr>
                           </thead>
                           <tbody>
                               <?php $cat_iterator = 0; ?>
                            @foreach($all_admin as $con)
                            
                            <?php $cat_iterator++; ?>
                            
                            <tr>
                              <td>{{$cat_iterator}}</td>
                              <td>{{$con->username}}</td>
                              <td>{{date("d-m-Y H:i:s",strtotime($con->created_at))}}</td> 
                              
                              <td align="center"><a href="delete_admin?id={{$con->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                            </tr>
                       
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                             <th>S/No</th>
                             <th>Username</th>
                             
                              <th>Created At</th>
                           
                              <th>Delete</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="all_tags" >
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                                
                             <th>S/No</th>
                             <th>Tag Name</th>
                              <th>Created At</th>
                           
                              <th>Delete</th> 
                            
                            </tr>
                           </thead>
                           <tbody>
                               <?php $cat_iterator = 0; ?>
                            @foreach($all_tags as $con)
                            
                            <?php $cat_iterator++; ?>
                            
                            <tr>
                              <td>{{$cat_iterator}}</td>
                              <td>{{$con->tag_name}}</td>
                              <td>{{date("d-m-Y H:i:s",strtotime($con->created_at))}}</td> 
                              
                              <td align="center"><a href="delete_tags?id={{$con->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                            </tr>
                       
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                              <th>S/No</th>
                             <th>Tag Name</th>
                              <th>Created At</th>
                           
                              <th>Delete</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="all_categorys" >
                                            <table  class="table table-striped table-bordered th example_admin" style="width:100%;word-wrap:break-word;
              table-layout: fixed;">
                           <thead>
                            <tr>
                                
                             <th>S/No</th>
                             <th>Category Name</th>
                              <th>log. Date</th>
                           
                              <th>Delete</th> 
                            
                            </tr>
                           </thead>
                           <tbody>
                               <?php $cat_iterator = 0; ?>
                            @foreach($all_categorys as $con)
                            
                            <?php $cat_iterator++; ?>
                            
                            <tr>
                              <td>{{$cat_iterator}}</td>
                              <td>{{$con->cat_name}}</td>
                              <td>{{date("d-m-Y H:i:s",strtotime($con->created_at))}}</td> 
                              
                              <td align="center"><a href="delete_category?id={{$con->id}}" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                              
                            </tr>
                            
                            <div class="modal fade" id="exampleModalcategory{{$con->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <div id='errors-user' style='background-color: red;color:white;'>
                                             @if(Session::get('cat_edit') == '1')
                                              @foreach($errors->all() as $error)
                                                   {{$error}}<BR>
                                              @endforeach
                                             @endif
                                         </div>
                                         <form method="post" action="edit_category?id={{$con->id}}" >
                                                @csrf
                                                 @if(Session::get('editcat_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                          
                                                 @endif


                                                 <br>
                                                <input type="text" name="category" placeholder="Category" class="form-control" value="{{$con->cat_name}}"/>
                                                <br>
                                                <input type="text" name="Subcategory" placeholder="SubCategory" class="form-control" value="{{$objs_admin->getSubs1($con->id)}}" />
                                                <br>
                                                <input type="text" name="Subs" placeholder="SubCategory" class="form-control" value="{{$objs_admin->getSubs2($objs_admin->getSubs1_ids($con->id))}}" />
                                                <br>
                                                <button type='submit' class="editcat btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-cat-spin2 hide" aria-hidden="true"></i> &nbsp; Edit</button>
                                            </form>
                                         
                                     </div>
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                     </div>
                                   </div>
                                </div>
  
                            </div>
                         
                            
                         
                            
                            @endforeach
                       
                           
                          </tbody>
                          <tfoot>
                           <tr>
                              
                              <th>S/No</th>
                             <th>Category Name</th>
                              <th>log. Date</th>
                       
                              <th>Delete</th> 
                           </tr>
                         </tfoot>
                    </table>
                                        </div>
                                       
                                    </div>
                                </div><!-- end col -->  

                            </div>
                
                       <div class="row background-color-white" style="margin-bottom:50px;padding:50px;">
                           
                           <H2>Promoted Ads Settings</H2>
                           
                           <br>
                            
                                        
                                       
                                            <table  class="table table-striped table-bordered th datatable" style="width:100%;
              table-layout: fixed;">
                           <thead>
                            <tr>
                             
                              <th>S/No</th>
                              <th>plans</th>
                              <th>1 month Boost Start</th>
                              <th>3 month Boost Start</th>
                              <th>6 month Boost Start</th>
                              <th>1 month Boost Business</th>
                              <th>3 month Boost Business</th>
                              <th>6 month Boost Business</th>
                              <th>1 month Boost Premium</th>
                              <th>3 month Boost Premium</th>
                              <th>6 month Boost Premium</th>
                              <th>Edit </th>
                              
                             
                            </tr>
                           </thead>
                           <tbody>
                               <?php $a=0; ?>
                               @foreach($all_plans as $v)
                               
                               <tr>
                                   <?php $a++; ?>
                                   <td>{{$a}} </td>
                                   <td>{{$v->plans}}</td>
                                   <td>{{$v->onemonth_BoostStart}}</td>
                                   <td>{{$v->threemonth_BoostStart}} </td>
                                   <td>{{$v->sixmonth_BoostStart}} </td>
                                   <td>{{$v->onemonth_boostBusiness}}</td>
                                   <td>{{$v->threemonth_boostBusiness}} </td>
                                   <td>{{$v->sixmonth_boostBusiness}} </td>
                                   <td>{{$v->onemonth_boostPremium}}</td>
                                   <td>{{$v->threemonth_boostPremium}} </td>
                                   <td>{{$v->sixmonth_boostPremium}} </td>
                                   <td><i class="fa fa-pencil" style="cursor:pointer;" aria-hidden="true" data-toggle="modal" data-target="#p-ads{{$v->id}}"></i></td>
                                  
                               </tr>
                               
                                <div class="modal fade" id="p-ads{{$v->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Edit Promoted Ads</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <div id='errors-user' style='background-color: red;color:white;'>
                                             @if(Session::get('edit_p_ads') == '1')
                                              @foreach($errors->all() as $error)
                                                   {{$error}}<BR>
                                              @endforeach
                                             @endif
                                         </div>
                                         <form method="post" action="edit_promotedAds?id={{$v->plans}}&ids={{$v->id}}" >
                                               @csrf


                                               
                                                <input type="text" name="one_month_boost_start" placeholder="One Month Boost Start" class="form-control" value="{{$v->onemonth_BoostStart}}" title="One Month" />
                                                <br>
                                                <input type="text" name="three_month_boost_start" placeholder="Three Month Boost Start" class="form-control" value="{{$v->threemonth_BoostStart}}" title="Three Month" />
                                                <br>
                                                <input type="text" name="six_month_boost_start" placeholder="Six Month Boost Start" class="form-control" value="{{$v->sixmonth_BoostStart}}" title="Six Month" />
                                                <br>
                                                <input type="text" name="one_month_boost_business" placeholder="One Month Boost Business" class="form-control" value="{{$v->onemonth_boostBusiness}}" title="One Month" />
                                                <br>
                                                <input type="text" name="three_month_boost_business" placeholder="Three Month Boost Business" class="form-control" value="{{$v->threemonth_boostBusiness}}" title="Three Month" />
                                                <br>
                                                <input type="text" name="six_month_boost_business" placeholder="Six Month Boost Business" class="form-control" value="{{$v->sixmonth_boostBusiness}}" title="Six Month" />
                                                <br>
                                                 <input type="text" name="one_month_boost_premium" placeholder="One Month Boost Premium" class="form-control" value="{{$v->onemonth_boostPremium}}" title="One Month" />
                                                <br>
                                                <input type="text" name="three_month_boost_premium" placeholder="Three Month Boost Premium" class="form-control" value="{{$v->threemonth_boostPremium}}" title="Three Month" />
                                                <br>
                                                <input type="text" name="six_month_boost_premium" placeholder="Six Month Boost Premium" class="form-control" value="{{$v->sixmonth_boostPremium}}" title="Six Month" />
                                                <br>
                                                <button type='submit' class="editcat btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-cat-spin2 hide" aria-hidden="true"></i> &nbsp; Edit</button>
                                            </form>
                                         
                                     </div>
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                     </div>
                                   </div>
                                </div>
  
                            </div>
                         
                            
                         
                              
                               @endforeach
                          
                           
                          </tbody>
                          <tfoot>
                           <tr>
                             <th>S/No</th>
                              <th>plans</th>
                              <th>1 month Boost Start</th>
                              <th>3 month Boost Start</th>
                              <th>6 month Boost Start</th>
                              <th>1 month Boost Business</th>
                              <th>3 month Boost Business</th>
                              <th>6 month Boost Business</th>
                              <th>1 month Boost Premium</th>
                              <th>3 month Boost Premium</th>
                              <th>6 month Boost Premium</th>
                              <th>Edit </th>
                           </tr>
                         </tfoot>
                    </table>
                                       
                                  
                                       
                                    </div>
                                </div><!-- end col -->  

                            </div>
                
                       

              

            </div>
         

        </div>
        
        
       



      
        
     
        
         <!-- jQuery  -->
       <!-- <script src="admin/assets/js/jquery.min.js"></script> -->
       <!-- <script src="admin/assets/js/bootstrap.min.js"></script> -->
    
        @foreach($all_cities as $con)
                            
                            
                            
                             
                             <div class="modal fade" id="exampleModalcity{{$con->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Edit City</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                       
                                         <form method="post" action="edit_city?id={{$con->id}}" >
                                                @csrf
                                                 @if(Session::get('editcity_error') == '1')
                                                    <div style='background-color: red;color:white;'>
                                                               
                                                      @foreach($errors->all() as $error)
                                                            {{$error}}<BR>
                                                      @endforeach
                                                    </div>
                          
                                                 @endif


                                               
                                                <input type="text" name="city" placeholder="City" class="form-control" value="{{$con->city}}" />
                                                <br>
                                                <input type="file" name="city_pic" />
                                                <br>
                                                <button type='submit' class="editcity btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-editcity-spin2 hide" aria-hidden="true"></i> &nbsp; Edit</button>
                                            </form>
                                         
                                     </div>
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                     </div>
                                   </div>
                                </div>
  
                            </div>
                         
                            
                         
                       
                            @endforeach
                            
                            
        @if(Session::get('verify') == 1)
        
        <script>
            
            alert('Successful');
            
        </script>
        
        @endif
        
        <?php Session::put('verify',null); ?>
                       
        
       
       
         @if(Session::get('editcity_error') == '1')
        
        
            <script>
                 $("#exampleModalcity{{Request::get('id')}}").modal();      
            </script>
       
        @elseif(Session::get('editcity_error') == '2')
             
             <script>
                alert('Successful');
            </script>
       
        
        @endif
        
        
         @if(Session::get('user_email_error') == '1')
        
        
        
       
            <script>
                 $("#exampleModal{{Request::get('user_id')}}").modal();      
            </script>
       
        @elseif(Session::get('user_email_error') == '2')
             
             <script>
                alert('Successful');
            </script>
       
        
        @endif
        
         @if(Session::get('bill_edit') == '1')
        
        
        
       
            <script>
                 $("#exampleModala{{Request::get('id')}}").modal();      
            </script>
       
        @elseif(Session::get('bill_edit') == '2')
             
             <script>
                alert('Successful');
            </script>
       
        
        @endif
        
        @if(Session::get('editcat_error') == '1')
        
        
        
       
            <script>
                 $("#exampleModalcategory{{Request::get('id')}}").modal();      
            </script>
       
        @elseif(Session::get('editcat_error') == '2')
             
             <script>
                alert('Successful');
            </script>
       
        
        @endif
        
        
         @if(Session::get('side_edit') == '1')
        
        
        
       
            <script>
                 $("#exampleModalside{{Request::get('id')}}").modal();      
            </script>
       
        @elseif(Session::get('side_edit') == '2')
             
             <script>
                alert('Successful');
            </script>
       
        
        @endif
        
        @if(Session::get('edit_p_ads') == '1')
        
            <script>
                 $("#p-ads{{Request::get('id')}}").modal();      
            </script>
       
        @elseif(Session::get('edit_p_ads') == '2')
             
             <script>
                alert('Successful');
            </script>
       
        
        @endif
        
        
          
        
         @if(Session::get('nav_edit') == '1')
        
        
        
       
            <script>
                 $("#exampleModalnav{{Request::get('id')}}").modal();      
            </script>
       
        @elseif(Session::get('nav_edit') == '2')
             
             <script>
                alert('Successful');
            </script>
       
        
        @endif
        
        
        
        <?php Session::put('editcity_error',null); ?>
        
        <?php Session::put('editcat_error',null); ?>
        
        <?php Session::put('tag_error',null); ?>
        
        <?php Session::put('photo_error',null); ?>
        
        <?php Session::put('admin_error',null); ?>
        
        <?php Session::put('cat_error',null); ?>
        
        <?php Session::put('logo',null); ?>
        
        <?php Session::put('photo_error_side',null); ?>
        
         <?php Session::put('user_email_error',null); ?>
        
        <?php Session::put('bill_edit',null); ?>
        
        <?php Session::put('side_edit',null); ?>
        
        <?php Session::put('city_error',null); ?>
        
        <?php Session::put('editcity_error',null); ?>
        
        <?php Session::put('nav_error',null); ?>
        
        <?php Session::put('nav_edit',null); Session::put('edit_p_ads',null); ?>
        
        <div style="display:none;" >
                    <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Line Chart</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Line Chart</h4>

                            <canvas id="lineChart" height="300"></canvas>

                        </div>
                    </div><!-- end col-->

                    <div class="col-lg-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Bar Chart</h4>

                            <canvas id="bar" height="300"></canvas>
                        </div>
                    </div><!-- end col-->

                </div>
                <!-- end row -->


                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Pie Chart</h4>

                            <canvas id="pie" height="300"></canvas>

                        </div>
                    </div><!-- end col-->

                    <div class="col-lg-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Donut Chart</h4>

                            <canvas id="doughnut" height="300"></canvas>

                        </div>
                    </div><!-- end col-->

                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Polar area Chart</h4>

                            <canvas id="polarArea" height="300"> </canvas>

                        </div>
                    </div><!-- end col-->

                    <div class="col-lg-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30"> Radar Chart</h4>

                            <canvas id="radar" height="300"></canvas>

                        </div>
                    </div><!-- end col-->
                </div>
            </div>
        
        @foreach($all_users as $users)
                            
                           
                            
                            <div class="modal fade" id="exampleModal{{$users->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">User Info</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <div id='errors-user' style='background-color: red;color:white;'>
                                             @if(Session::get('user_email_error') == '1')
                                              @foreach($errors->all() as $error)
                                                   {{$error}}<BR>
                                              @endforeach
                                             @endif
                                         </div>
                                          <form method="post" action="edit_user?user_id={{$users->id}}">
                                             @csrf
                                             <input type="text" name="user_email" class="form-control" value="{{$users->user_email}}" /> 
                                             <br>
                                             <input type="text" name="user_pass" class="form-control" value=" " /> 
                                             <br>
                                             <button type='submit' class="editEmail btn btn-primary" style='width:100%;'><i class="fa fa-spinner fa-spin admin-edit-spin hide" aria-hidden="true"></i> &nbsp; Edit Email</button>
                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                     </div>
                                   </div>
                                </div>
  
                            </div>
                            
                            @endforeach
                            
                              <!-- jQuery  -->
       <!-- jQuery  -->
        <script src="admin/assets/js/jquery.min.js"></script>
        <script src="admin/assets/js/bootstrap.min.js"></script>
        <script src="admin/assets/js/detect.js"></script>
        <script src="admin/assets/js/fastclick.js"></script>

        <script src="admin/assets/js/jquery.slimscroll.js"></script>
        <script src="admin/assets/js/jquery.blockUI.js"></script>
        <script src="admin/assets/js/waves.js"></script>
        <script src="admin/assets/js/wow.min.js"></script>
        <script src="admin/assets/js/jquery.nicescroll.js"></script>
        <script src="admin/assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="admin/assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<script src="admin/assets/plugins/morris/morris.min.js"></script>
		<script src="admin/assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <script src="admin/assets/pages/jquery.dashboard.js"></script>
        
              <!-- jQuery  -->
        <script src="admin/assets/js/jquery.min.js"></script>
        <script src="admin/assets/js/bootstrap.min.js"></script>
        <script src="admin/assets/js/detect.js"></script>
        <script src="admin/assets/js/fastclick.js"></script>
        <script src="admin/assets/js/jquery.slimscroll.js"></script>
        <script src="admin/assets/js/jquery.blockUI.js"></script>
        <script src="admin/assets/js/waves.js"></script>
        <script src="admin/assets/js/wow.min.js"></script>
        <script src="admin/assets/js/jquery.nicescroll.js"></script>
        <script src="admin/assets/js/jquery.scrollTo.min.js"></script>

     
        
     

        <!-- App js -->
   
        
          <!-- jQuery  -->
        <script src="admin/assets/js/jquery.min.js"></script>
        <script src="admin/assets/js/bootstrap.min.js"></script>
        <script src="admin/assets/js/detect.js"></script>
        <script src="admin/assets/js/fastclick.js"></script>
        <script src="admin/assets/js/jquery.slimscroll.js"></script>
        <script src="admin/assets/js/jquery.blockUI.js"></script>
        <script src="admin/assets/js/waves.js"></script>
        <script src="admin/assets/js/wow.min.js"></script>
        <script src="admin/assets/js/jquery.nicescroll.js"></script>
        <script src="admin/assets/js/jquery.scrollTo.min.js"></script>

        <!-- Chart JS -->
        <script src="admin/assets/plugins/chart.js/chart.min.js"></script>
        <script src="admin/assets/pages/jquery.chartjs.init.js"></script>
        
        
               <script src="admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="admin/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="admin/assets/plugins/datatables/jszip.min.js"></script>
        <script src="admin/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="admin/assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="admin/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="admin/assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.scroller.min.js"></script>

        <!-- Datatable init js -->
        <script src="admin/assets/pages/datatables.init.js"></script>

        <!-- App js -->
           <script src="admin/assets/js/jquery.core.js"></script>
        <script src="admin/assets/js/jquery.app.js"></script>
        

        <script type="text/javascript">
            $(document).ready(function() {
                $('.datatable').dataTable();
                $('.example_admin').dataTable();
                $('#datatable-keytable').DataTable( { keys: true } );
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            } );
            TableManageButtons.init();

        </script>

        
        <script type="text/javascript" src="js/app2.js"></script>

    </body>
</html>