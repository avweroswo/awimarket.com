<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">

<link rel="stylesheet" href="css/home.css" />


<link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">
<link rel="stylesheet" href="assets/extras/settings.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">

<script type="text/javascript" src="assets/js/jquery-min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script> 

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" type="text/css">

<link rel="stylesheet" href="css/home.css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  type="text/css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    
</head>
<body>
    

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

    <link rel="stylesheet" href="css/home.css" />
    <div class="home-background" style='min-height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')
         
         
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">


  
        @include('user-header.user-header')
        <!--
        
        <div class="row">
            <div class="col-sm-12 text-center " style="padding:30px;" >
                <div class="text-color-white dd-m-width-90" style="width:30%;margin: 0 auto;background-color:#3f51b5;cursor:pointer">
                    <span class="active-overlay dash-overlay" style="margin-right:20px;display:inline-block;padding:5px;" data-role="1" >WETIN <br> I DEY SELL</span>
                    <span class="dash-overlay" style="display:inline-block;padding:5px;" data-role="2">SELLERS WEY <br> I DEY FOLLOW</span>
                    <span class="dash-overlay" style="margin-left:20px;display:inline-block;padding:5px;" data-role="3" >WETIN <br> I SAVE <i class="fa fa-heart" aria-hidden="true"></i> </span>
                </div>
                
            </div> 
        </div>
        
        -->
        
        <div class="row" style="background-color:white;overflow:auto;padding-bottom:100px;">
            
            <div class="col-sm-12 " style="">
                <div class="margin-top-20 border-dashboard clearfix dd-m-width-90" style="width:50%;margin:0 auto;overflow:auto;padding:20px;">
                    <div id="accordion" class="panel-group">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"> <a href="#collapseB1" class="ddx" data-toggle="collapse"> My details </a> </h4>
</div>
<div class="panel-collapse collapse in" id="collapseB1">
<div class="panel-body">
<form method="post" action="UpdateDetails" >
    <br>
    @if(Session::get('success') == 1)
    
    <div style='color:blue;'>
        Edit Successful
    </div>
    
    <?php Session::put('success',null); ?>
    
    
    
    @endif
    
    <div style='color:red;'>
      @if(Session::get('error1') == 1)  
    <div style='color:red;'>
      @if(isset($errors)) 
         @foreach($errors->all() as $error)
            {{$error}}<BR>
         @endforeach
      @endif
      
    <?php Session::put('error1',null); ?>  
      
       </div>
      
   @endif
   
    </div>
           
                                   
   
    <br>
    
    @csrf
<div class="form-group">
<label class="control-label">First Name</label>
<input class="form-control" placeholder="Jhon" name="firstname" type="text" value="{{Auth::user()->firstname}}">
</div>
<div class="form-group">
<label class="control-label">Last name</label>
<input class="form-control" placeholder="Doe" type="text" name="lastname" value="{{Auth::user()->lastname}}">
</div>
<div class="form-group">
<label class="control-label">Email</label>
<input class="form-control" placeholder="jhon.deo&copy;example.com" name="email" type="email" value="{{Auth::user()->user_email}}">
</div>
<div class="form-group">
<label class="control-label">Address</label>
<input class="form-control" name="address" placeholder=".." type="text" value="{{Auth::user()->address}}">
</div>
<div class="form-group">
<label for="Phone" class="control-label">Phone</label>
<input class="form-control" id="Phone" name="phone" placeholder="08161827947" type="text" value="{{Auth::user()->phone}}">
</div>

<div class="form-group">
<label class="control-label">Company</label> &nbsp;
<input class="form-control company-1" type='checkbox' <?php echo Auth::user()->company_name !== '' ? 'checked' : '' ?> />
</div>
    
<div class="form-group tag-box" style="display:none;">
<label class="control-label">Company name</label> 
<input class="form-control" name="company_name" type='text'  value="{{Auth::user()->company_name }}" />
</div>
    
<div class="form-group">
<label class="control-label">Tags</label> 
<input class="form-control tag-box" type="text" value="<?php echo Auth::user()->tags; ?>" readonly />    
</div>
 
<div class="form-group tagbox" style="<?php echo Auth::user()->company_name !== '' ? '' : 'display:none;' ?> ">
<label class="control-label">Tags</label>
<select    data-live-search="true" multiple name="tags[]">
    <option>Fashion</option>
    <option>School</option>
    <option>Drugs</option>
</select>
</div>
    
    
<div class="form-group">
<label class="control-label">I Want Delivery Services</label> &nbsp;
<input class="form-control d-services" name="offer_d_services" type='checkbox' value="1" <?php echo Auth::user()->offer_d_services !== '0' ? 'checked' : '' ?> >
</div>
 

<div class="form-group abt-company" style="<?php echo Auth::user()->company_name !== '' ? '' : 'display:none;' ?>">
<label class="control-label">About your Company</label> &nbsp;
<textarea class="form-control" col='45' rows='5' name="about_company" >{{Auth::user()->about_company}}</textarea>
</div>
    
<div class="form-group " >
<label class="control-label">Seller Url</label> &nbsp;
<input class="form-control" name="seller_url" placeholder="seller url" type="text" value="{{Auth::user()->seller_url}}" />
</div>
    
<div class="form-group hide">
<label class="control-label">Facebook account map</label>
<div class="form-control"> <a class="link" href="fb.com">Jhone.doe</a>
<a class=""> <i class="fa fa-minus-circle"></i></a>
</div>
</div>
<div class="form-group">
<button type="submit" class="btn btn-common click-add update11"> <i class="fa fa-spinner fa-spin update-1 hide" aria-hidden="true"></i> &nbsp;Update</button>
</div>
</form>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"> <a aria-expanded="false" class="collapsed ddx" href="#collapseB2" data-toggle="collapse"> Settings </a> </h4>
</div>
<div style="height: 0px;" aria-expanded="false" class="panel-collapse collapse" id="collapseB2">
<div class="panel-body">
<form action='changepassword1' method='post'>
    
    @csrf
    <br>
    @if(Session::get('success2') == 1)
    
    <div style='color:blue;'>
        Edit Successful
    </div>
    
    <?php Session::put('success2',null); ?>
    
    @endif
    <br>
  @if(Session::get('error2') == 1)  
    <div style='color:red;'>
      @if(isset($errors)) 
         @foreach($errors->all() as $error)
            {{$error}}<BR>
         @endforeach
      @endif
      
    <?php Session::put('error2',null); ?>
      
       </div>
      
   @endif
                                   
   
<div class="form-group">
<div class="checkbox">
<label><input type="checkbox" name='enable_comments'  <?php echo Auth::user()->enable_comments == 0 ? '' : 'checked' ?>>Comments are enabled on my ads </label>
</div>
</div>
<div class="form-group">
<label class="control-label">New Password</label>
<input class="form-control" name='password' placeholder="" type="password">
</div>
<div class="form-group">
<label class="control-label">Confirm Password</label>
<input class="form-control" name='password_confirm' placeholder="" type="password">
</div>
<div class="form-group">
<button type="submit" class="btn btn-common click-add update22"><i class="fa fa-spinner fa-spin update-2 hide" aria-hidden="true"></i>&nbsp;Update</button>
</div>
</form>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"> <a aria-expanded="false" class="collapsed" href="#collapseB3" data-toggle="collapse"> Preferences </a> </h4>
</div>
<div style="height: 0px;" aria-expanded="false" class="panel-collapse collapse" id="collapseB3">
<div class="panel-body">
<div class="form-group">
<div class="col-sm-12">
<form action='update_receive_newsletter' method='post'>

     @csrf
    <br>
    @if(Session::get('success3') == 1)
    
    <div style='color:blue;'>
        Edit Successful
    </div>
    
    <?php Session::put('success3',null); ?>
    
    @endif
    <br>
<div class="checkbox">
<label><input type="checkbox" name='newsletter'>I want to receive newsletter. </label>
</div>
<div class="checkbox">
<label><input type="checkbox" name='advice'>I want to receive advice on buying and selling. </label>
</div>
<div class="form-group">
<button type="submit" class="btn btn-common click-add update33"><i class="fa fa-spinner fa-spin update-3 hide" aria-hidden="true"></i> &nbsp; Update</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"> <a aria-expanded="false" class="collapsed" href="#collapseB4" data-toggle="collapse"> Connect with </a> </h4>
</div>
<div style="height: 0px;" aria-expanded="false" class="panel-collapse collapse" id="collapseB4">
<div class="panel-body">
<div class="form-group">
<div class="col-sm-12">
    <a href='login/facebook'>
       <div><i class="fa fa-facebook-official" aria-hidden="true" style="color:blue;"></i> &nbsp; Connect with facebook </div>
    </a>
    
    <a href='login/twitter'>
        <div><i class="fa fa-twitter-square" aria-hidden="true" style="color:#00aced;"></i> &nbsp; Connect with twitter </div>
    </a>
    
    <a href='login/google'>
        <div><i class="fa fa-google" aria-hidden="true" style="color:#da5946;"></i> &nbsp;  Connect with Google</div>
    </a>
    
    
</div>
</div>
</div>
</div>
</div>
</div>
               </div>
      </div> 
    </div>

   


      <!--fin buttons -->



<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>



<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>

        <script type="text/javascript" src="js/app2.js"></script>
        
      
  
    
    
      

        
        @include('footer.footer')
        
        
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>