<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">



<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<!-- <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css"> -->

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css"type="text/css"> 

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
    
	<!-- End WOWSlider.com HEAD section -->
        
<script type="text/javascript" src="assets/js/jquery-min.js"></script> 
<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">



<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  type="text/css">

<link rel="stylesheet" href="css/home.css" />
    
</head>
<body>
    
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1631938846852679',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

    
    <div class="home-background" style='min-height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')


  

	
     

        
        <div class='row' style="overflow:auto;margin-top:180px;margin-bottom:180px;">
                
                  <div class='col-sm-12  back-color-white width-50-e centralize float-none height-50  clearfix '  >
                     
                      <ul class="nav nav-tabs">
                         <li class="{{Session::get('error_login') == null  && Request::get('login') !== "1" ? 'active' : ''}}"><a data-toggle="tab" href="#home">Register</a></li>
                         <li class='<?php if(Session::get('error_login') !==null or Request::get('login') == "1" ){echo "active"; }?> '><a data-toggle="tab" href="#profile">Login</a></li>
                      </ul>
                      <div class="tab-content" >
                          <div class="tab-pane fade {{Session::get('error_login') == null  && Request::get('login') !== "1" ? 'in active' : ''}}  clearfix" id="home" role="tabpanel" aria-labelledby="home-tab" style="overflow:auto;">
                              <form action="register2" method="post" >
                                  @csrf
                                      <div  class="col-sm-12 ">
                               <div class="row" >
                                 <a href='https://www.facebook.com/v2.12/dialog/oauth?
  client_id=1631938846852679
  &redirect_uri=https://demo.awimarket.com
  &state={state-param}'>   
                                <div class='col-sm-6 bar fb ' >
                                    <div class='col-sm-2 text-left social-icon-font' >
                                       <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    </div>
                                    <div class='col-sm-10 text-center margin-top-8' >
                                       Register with Facebook
                                    </div>
                                </div>
 </a>
                    
                                <div class='col-sm-6 bar google-chrome' style='' >
                                    <div class='col-sm-2 text-left social-icon-font' >
                                      <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                                    </div>
                                    <div class='col-sm-10 text-center margin-top-8' >
                                      Register with Google
                                    </div>
                              
                                </div>
                               </div>
                                 <div class="row margin-top-20">                      
                                   <div class='bar-e bar twitter' style='margin: auto;
    min-height: 45px;' >
                                      <div class='col-sm-2 text-left social-icon-font' >
                                       <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                      </div>
                                      <div class='col-sm-10 text-center margin-top-8' >
                                        Register with Twitter
                                      </div>                       
                                   </div>
                                 </div>
                              </div>
                              <div class='row '>
                                <div class="col-sm-12" style="color:red;">
                                   <br>
                                   @if(Session::get('error') !== null)
                                       Please verify your phone number before submitting 
                                       <?php Session::put('error',null); ?>
                                   @endif
                                   
                                   @if(Session::get('error2') !== null)
                                       Code invalid please generate another one 
                                       <?php Session::put('error2',null); ?>
                                   @endif
                                   
                                   @if(isset($errors)) 
                                    @foreach($errors->all() as $error)
                                    {{$error}}<BR>
                                    @endforeach
                                   @endif
                             
                                      
                                </div>
                                <div class='col-sm-6 width-53 margin-top-20 ' >
                                  <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'>@</span>
                                     <input id="email" type="text" class="form-control" name="email" placeholder="Email" value='{{Session::get('email') == null ? '' : Session::get('email')}}' style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                                </div>
                                <div class='col-sm-6 width-53 margin-top-20 ' >
                                  <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="
glyphicon glyphicon-lock" aria-hidden="true"></i></span>
                                     <input id="password" type="password" class="form-control" name="password" placeholder='Password' value='{{Session::get('password') == null ? '' : Session::get('password')}}' style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                                </div>
                              </div>
                              
                              <div class='row '>
                                <div class='col-sm-6 width-53 margin-top-20 ' >
                                  <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="glyphicon glyphicon-user"></i></span>
                                     <input id="firstname" type="text" class="form-control" name="firstname" value='{{Session::get('firstname') == null ? '' : Session::get('firstname')}}'  placeholder="First name" style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                                </div>
                                <div class='col-sm-6 width-53 margin-top-20 ' >
                                  <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="glyphicon glyphicon-user"></i></span>
                                     <input id="lastname" type="text" class="form-control" name="lastname" placeholder="Last name" value='{{Session::get('lastname') == null ? '' : Session::get('lastname')}}' style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                                </div>
                              </div>
                              
                               <div class='row '>
                                <div class='col-sm-12  margin-top-20 ' >
                                  <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="glyphicon glyphicon-phone" aria-hidden="true"></i></span>
                                     <input id="phone-1" type="text" class="form-control padding-top-20 padding-bottom-20" name="phone-1" placeholder="Phone" value='{{Session::get('phone-1') == null ? '' : Session::get('phone-1')}}'  />
                                  </div>
                                </div>  
                               </div>
                                  
                                  <br>
                                  <meta name="csrf-token" content="{{ csrf_token() }}">
                               <div class='row '>
                                <div class='col-sm-12 ' >
                                    <div class='row '>
                                       <div class='col-sm-7' >
                                          <div class="input-group">
                                          <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="glyphicon glyphicon-phone" aria-hidden="true"></i></span>
                                           <input id="code" type="text" class="form-control padding-top-20 padding-bottom-20" name="code" placeholder="Enter code" value='{{Session::get('code') == null ? '' : Session::get('code')}}'  />
                                         </div>
                                       </div>
                                       <div class='col-sm-5' >
                                        <button type="button" id="gen-code1"  class='btn width-100 color-purple text-color-white click-add ' style='padding:10px;'><i class="fa fa-spinner fa-spin gen-spin hide" aria-hidden="true"></i>Generate code</button>
                                       </div>
                                    </div>
                                </div>  
                               </div>
                              
                              <br>
                              
                              <div class='row '>
                                <div class='col-sm-12' >
                                 
                                    <input type="checkbox" name="terms" value="1" {{Session::get('terms') == null ? '' : 'checked'}}> &nbsp; I agree with rules
                                    <!-- <label class="form-check-label" for="checkbox106">Classic checkbox</label> -->
                                  </div>
                                </div>  
                    
                              
                              <br>
                              
                              <div class='row '>
                                <div class='col-sm-12 text-center' >                                                
                                      <button class='btn width-100 color-purple text-color-white click-add reg-btn ' ><i class="fa fa-spinner fa-spin reg-spin hide" aria-hidden="true"></i>&nbsp;Register</button>          
                                </div>
                              </div>
                              <br>
                              </form>
                          
                              
                          </div>
                          <div class="tab-pane fade <?php if(Session::get('error_login') !==null or Request::get('login') == "1" ){echo "in active"; }?>  " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                              <div class="row">
                               <div  class="col-sm-12 ">
                               <div class="row" >
                                 <a href='https://www.facebook.com/v2.12/dialog/oauth?
  client_id=1631938846852679
  &redirect_uri=https://demo.awimarket.com
  &state={state-param}'>   
                                <div class='col-sm-6 bar fb ' >
                                    <div class='col-sm-2 text-left social-icon-font' >
                                       <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    </div>
                                    <div class='col-sm-10 text-center margin-top-8' >
                                       Register with Facebook
                                    </div>
                                </div>
 </a>
                    
                                <div class='col-sm-6 bar google-chrome' style='' >
                                    <div class='col-sm-2 text-left social-icon-font' >
                                      <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                                    </div>
                                    <div class='col-sm-10 text-center margin-top-8' >
                                      Register with Google
                                    </div>
                              
                                </div>
                               </div>
                                 <div class="row margin-top-20">                      
                                   <div class='bar-e bar twitter' style='margin: auto;
    min-height: 45px;' >
                                      <div class='col-sm-2 text-left social-icon-font' >
                                       <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                      </div>
                                      <div class='col-sm-10 text-center margin-top-8' >
                                        Register with Twitter
                                      </div>                       
                                   </div>
                                 </div>
                              </div>
                          </div>
                          <form action='authenticate0' method='post'>
                               @csrf
                          <div class="row">
                              
                                 
                              
                              <div class='col-sm-12' style='color:red;'>
                                  @if(Session::get('error_login') == 1)
                                      Invalid login credentials
                                     
                                      
                                  @elseif(Session::get('error_login') == 2)
                                       
                                          Wrong Captcha Value Entered
                                          
                                  
                                       
                                  @endif
                              </div>
                              
                              <?php Session::put('error_login',null); ?>
                              
                           

                              <div class="col-sm-12 margin-top-20">
                                  <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'>@</span>
                                     <input id="email_login" type="text" class="form-control" name="email_login" value='{{Session::get('email_login') == null ? '' : Session::get('email_login')}}' placeholder="Email" style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                              </div>
                              
                              <br>
                              
                              <div class="col-sm-12 margin-top-20">
                                    <div class="input-group">
                                     <span class="input-group-addon" style='padding-left:10px !important;padding-right:10px !important;'><i class="
glyphicon glyphicon-lock" aria-hidden="true"></i></span>
                                     <input id="password_login" type="password" class="form-control" name="password_login" value='{{Session::get('password_login') == null ? '' : Session::get('password_login')}}' placeholder='Password' style='padding-top: 20px;
                                        padding-bottom: 20px;' />
                                  </div>
                              </div>
                              
                          </div> 
                          <div class="row">
                              <?php $obj1->createCaptcha(); ?>
                              <div class="col-sm-12" align="center" style="margin-top:10px;">
                                  <img src="font/captcha.png" /> &nbsp; <input type="text" name="captcha_entered" value="" placeholder="Enter" style="width:10%;border:solid 1px black;" />
                                 
                              </div>         
                          </div>
                            <div class="row margin-top-20" style="margin-bottom:20px;">
                            
                              <div class="col-sm-3">
                                  <button class="color-purple text-color-white sign-in-button click-add"><i class="fa fa-spinner fa-spin login-spin hide" aria-hidden="true"></i>&nbsp;Sign in</button>
                              </div>
                             
                              <div class="col-sm-6  text-color-purple margin-top-10">
                                  <a href='forgotpass'>Forgot your password?</a>
                              </div>
                              
                          </div> 
</form>
                          </div>  
                      </div>
                      
                          
                 
                          
                 </div>
                    
          
               
              </div>

   





<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>


<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>

        <script type="text/javascript" src="js/app2.js"></script>
  
    
    
      

        
        @include('footer.footer')
        
        
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>