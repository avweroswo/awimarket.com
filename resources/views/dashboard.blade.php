<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">



<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<!-- <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css"> -->

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css"type="text/css"> 

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	
        
<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


<link href="http://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  type="text/css">

<link rel="stylesheet" href="css/home.css" />

</head>
<body onload='unFixFooter()'>
    


<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

    
    <div class="home-background" style='min-height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')


  

	

        
        @include('user-header.user-header')
        
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=1631938846852679&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your share button code -->

        
        <div class="row">
            <div class="col-sm-12 text-center " style="padding:30px;" >
                <div class="text-color-white dd-m-width-90 small-font" style="width:30%;margin: 0 auto;background-color:#3f51b5;cursor:pointer">
                    <span class="active-overlay dash-overlay" onclick="$('.dash-overlay').removeClass('active-overlay');$(this).addClass('active-overlay');$('.t2').hide();$('.t3').hide();$('.t1').show()"  style="margin-right:20px;display:inline-block;padding:5px;" data-role="1" >WETIN <br> I DEY SELL</span>
                    <span class="dash-overlay" onclick="$('.dash-overlay').removeClass('active-overlay');$(this).addClass('active-overlay');$('.t2').show();$('.t3').hide();$('.t1').hide()" style="display:inline-block;padding:5px;" data-role="2">SELLERS WEY <br> I DEY FOLLOW</span>
                    <span class="dash-overlay" onclick="$('.dash-overlay').removeClass('active-overlay');$(this).addClass('active-overlay');$('.t2').hide();$('.t3').show();$('.t1').hide();"  style="margin-left:20px;display:inline-block;padding:5px;" data-role="3" >WETIN <br> I SAVE <i class="fa fa-heart" aria-hidden="true"></i> </span>
                </div>
                
            </div> 
        </div>
        
        <div class="row" style="background-color:white;overflow:auto;padding-bottom:100px;">
            
            <div class="col-sm-12 " style="">
                <div class="margin-top-20 border-dashboard clearfix dd-m-width-90" style="width:50%;margin:0 auto;overflow:auto;padding:20px;">
                    <div style="border-bottom:1px solid #D3D3D3;min-height:40px; " class='bottom-40'>  
                        <div class="text-color-purple pull-left dd-m f-size purple-underline f-ads pointer-style" onclick='' ><a href='dashboard'>FREE ADS</a> </div> 
                         <div class="text-color-purple text-center left-27 p-ads pointer-style" onclick='' style="display:inline-block;"> 
                             <a href='dashboard?id=promoted'>
                                PROMOTED
                             </a>
                              </div>
                         
                         <div class="text-color-purple pull-right dd-m f-size" ><a href='createAd'><button class='click-add'>Click to sell</button></a></div> 
                    </div>
                    @if(Session::get('promote-ads') == null)
                         <div class="tabs1 t1">
                         <table id="example" class="table table-striped table-bordered th " style="width:100%">
                           <thead>
                            <tr>
                             <!-- <th ><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           
                            </tr>
                           </thead>
                           <tbody>
                             @foreach( $all_ads as $val)
                           
                                  <tr>
                              <!-- <td><input type="checkbox" /></td> -->
                                      <td><a href='adDetails?id={{$val->id}}'><img src="ad_photo/{{$obj->getPhoto($val->id)}}" width="300" height="262" alt="" class="img-box1" ></a></td>
                              <td>
                                  <div style='word-wrap:break-word;max-width:200px;'>
                                      {{$val->details}}
                                  </div>
                              </td>
                              <td>&#8358;{{number_format($val->price)}}</td>
                              <td class="action-td">
                                  <?php if($obj->checkAd($val->id) == 2){ ?>
                                    <a href="promoteAd?id={{$val->id}}" class='btn btn-primary btn-xs click-properties' style='margin-bottom:-15px;'>PROMOTE</a>
                                  <?php } ?>
                                 <a href='createAd?id={{$val->id}}' class="btn btn-primary btn-xs" style="margin-bottom:10px;"> <i class="fa fa-pencil-square-o"></i> Edit</a>                  
                                 <a style="margin-bottom:10px;" class="btn btn-info btn-xs " data-toggle="modal" data-target="#reportSeller_{{$val->id}}" >
    <i class="fa fa-share-square-o"></i> Share</a>
                                 <a href='delete_ads?id={{$val->id}}' class="btn btn-danger btn-xs"> <i class=" fa fa-trash"></i> Delete</a>
                              </td>
                
                            </tr>
                            
                            <div class="modal fade" id="reportSeller_{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Share On Social Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    
      <div class="modal-body">
          	<!-- Search Form -->
				
					
                                            <div class="row">
                                              
                                                <div class="col-sm-12">
                                                    
                                                        <a style="margin-bottom:10px;" class="fb-share-button"  data-layout="button_count" data-size="small" data-mobile-iframe="true" 
    data-href="https://www.awimarket.com/adDetails?id={{$val->id}}" 
    data-layout="button_count" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fawimarket.com/adDetails?id={{$val->id}}%2F&amp;src=sdkpreparse" >
    <i class="fa fa-facebook-official" aria-hidden="true" style="font-size:100px;"></i>
                                                        </a>
                                                   <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                    &nbsp; &nbsp; &nbsp; &nbsp;
                                                    
                                                    <?php $url = 'https://www.awimarket.com/adDetails?id='.$val->id; ?>
                                                    
                                                    <a href="https://twitter.com/intent/tweet?text={{$url}}" >
                                                    
                                                        <i class="fa fa-twitter-square" aria-hidden="true" style="font-size:100px;"></i>
                                                        
                                                    </a>
                                                    
                                                    &nbsp; &nbsp; &nbsp; &nbsp;
                                                    
                                                    <a href="https://plus.google.com/share?url={{$url}}" >
                                                    
                                                        <i class="fa fa-google-plus-square" aria-hidden="true" style="font-size:100px;color:red"></i>
                                                        
                                                    </a>
                                                    
                                                    
                                                        
                                          
                                                    
                                                </div>
                                               
                                            </div>
					
				</div> <!-- /.rbm_search -->

      
      <div class="modal-footer">
          
        <!--
       
           <button type="Submit" class="btn btn-primary" style='z-index:99999999'>Submit</button>
        
        -->
        
        <button type="button" class="btn btn-primary" data-dismiss="modal" style='z-index:99999999'>Close</button>
       
      
     
        </div>

  </div>
  </div>
</div>
                         
                             @endforeach
                       
                          </tbody>
                          <tfoot>
                           <tr>
                             <!-- <th><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           </tr>
                         </tfoot>
                    </table>
                 
                
            </div>
                    @else
                    
                         <div class="tabs1 t1">
                         <table id="example" class="table table-striped table-bordered th " style="width:100%">
                           <thead>
                            <tr>
                             <!-- <th ><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           
                            </tr>
                           </thead>
                           <tbody>
                             @foreach( $all_ads as $val)
                             
                    
                            
                                <?php if($obj->checkAd($val->id) == 1){ ?>
                             
                                  <tr>
                              <!-- <td><input type="checkbox" /></td> -->
                                      <td><a href='adDetails?id={{$val->id}}'>
                                              <img src="ad_photo/{{$obj->getPhoto($val->id)}}" width="300" height="262" alt="" class="img-box1" >
                                          </a>    
                                              </td>
                              <td>
                                  <div style='word-wrap:break-word;max-width:200px;'>
                                      {{$val->details}}
                                  </div>
                                  </td>
                              <td>&#8358;{{number_format($val->price)}}</td>
                              <td class="action-td">
                                  <?php if($obj->checkAd($val->id) == 2){ ?>
                                    <a href="promoteAd?id={{$val->id}}" class='btn btn-primary btn-xs click-properties' style='margin-bottom:-15px;'>PROMOTE</a>
                                  <?php } ?>
                                 <a href='createAd?id={{$val->id}}' class="btn btn-primary btn-xs" style="margin-bottom:10px;"> <i class="fa fa-pencil-square-o"></i> Edit</a>
                                 <a class="btn btn-info btn-xs" style="margin-bottom:10px;"> <i class="fa fa-share-square-o"></i> Share</a>
                                 <a href='delete_ads?id={{$val->id}}' class="btn btn-danger btn-xs"> <i class=" fa fa-trash"></i> Delete</a>
                              </td>
                
                            </tr>
                            
                                <?php } ?>
                            
                             @endforeach
                       
                          </tbody>
                          <tfoot>
                           <tr>
                             <!-- <th><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           </tr>
                         </tfoot>
                    </table>
                 
                
            </div>
                    
                    @endif
                    <div class="tabs1 t2" style='display:none;'>
                         <table id="example2" class="table table-striped table-bordered th " style="width:100%">
                           <thead>
                            <tr>
                             <!-- <th ><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           
                            </tr>
                           </thead>
                           <tbody>
                            
                              @foreach($follow_list as $val)
                                     <tr>
                             <!-- <td><input type="checkbox" /></td> -->
                                                 <td><a href='adDetails?id={{$val->id}}'>
                                             
                                            <img src="ad_photo/{{$obj->getPhoto($val->followed_id)}}" width="300" height="262" alt="" class="img-box1" >
                                                     </a>
                                            </td>
                              <td>{{$obj->getAdDetails($val->followed_id)}}</td>
                              <td>&#8358;{{number_format($obj->getAdPrice($val->followed_id))}}</td>
                              <td class="action-td">
                                 <!-- <a class="btn btn-primary btn-xs click-properties" style="margin-bottom:-15px;">PROMOTE</a> -->
                                 <!-- <a class="btn btn-primary btn-xs" style="margin-bottom:10px;"> <i class="fa fa-pencil-square-o"></i> Edit</a> -->
                                 <a class="btn btn-info btn-xs" style="margin-bottom:10px;"> <i class="fa fa-share-square-o"></i> Share</a>
                                 <a href='delete_followed_ads?id={{$val->id}}' class="btn btn-danger btn-xs"> <i class=" fa fa-trash"></i> Delete</a>
                              </td>
                
                            </tr>
                            
                          @endforeach
                            
                            
                       
                          </tbody>
                          <tfoot>
                           <tr>
                             <!-- <th><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           </tr>
                         </tfoot>
                    </table>
                 
                
            </div>
         
                
                <div class="tabs1 t3 " style='display:none;'>
                         <table id="example3" class="table table-striped table-bordered th " style="width:100%">
                           <thead>
                            <tr>
                              <!--<th ><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           
                            </tr>
                           </thead>
                           <tbody>
                               
                               
                                @foreach($saved_list as $val)
                                     <tr>
                             <!-- <td><input type="checkbox" /></td> -->
                              <td>
                                 <a href='adDetails?id={{$val->id}}'>
                                  <img src="ad_photo/{{$obj->getPhoto($val->saved_id)}}" width="300" height="262" alt="" class="img-box1" >
                                 </a>  
                                  </td>
                              <td>{{$obj->getAdDetails($val->saved_id)}}</td>
                              <td>&#8358;{{number_format($obj->getAdPrice($val->saved_id))}}</td>
                              <td class="action-td">
                                 <!-- <a class="btn btn-primary btn-xs click-properties" style="margin-bottom:-15px;">PROMOTE</a> -->
                                 <!-- <a class="btn btn-primary btn-xs" style="margin-bottom:10px;"> <i class="fa fa-pencil-square-o"></i> Edit</a> -->
                                 <a class="btn btn-info btn-xs" style="margin-bottom:10px;"> <i class="fa fa-share-square-o"></i> Share</a>
                                 <a href='delete_saved_ads?id={{$val->id}}' class="btn btn-danger btn-xs"> <i class=" fa fa-trash"></i> Delete</a>
                              </td>
                
                            </tr>
                            
                          @endforeach
                            
                               
                            
                          </tbody>
                          <tfoot>
                           <tr>
                             <!-- <th><input type="checkbox" /></th> -->
                              <th>Photo</th>
                              <th>Ad Details</th>
                              <th>Price</th>
                              <th>Option</th>
                           </tr>
                         </tfoot>
                    </table>
                 
                
            </div>
        </div>
      </div> 
    </div>
        
        @if(Session::get('alert') == 1)
        
            <script>
                alert('Successful');
            </script>
        
        @endif
        
        @if(Session::get('del1') == 1)
        
            <script>
                
                alert('Deletion Successful');
                
                
            </script>
            
            <?php Session::put('del1',null); ?>
        
        
        @endif


    
    
    
                    <!--fin buttons -->



<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>
        <script src="admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="admin/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="admin/assets/plugins/datatables/jszip.min.js"></script>
        <script src="admin/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="admin/assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="admin/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="admin/assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="admin/assets/plugins/datatables/dataTables.scroller.min.js"></script>

        <!-- Datatable init js -->
        <script src="admin/assets/pages/datatables.init.js"></script>



<script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>

        <script type="text/javascript" src="js/app2.js"></script>
        
        <script>
            
            $(document).ready(function() {
               $('#example').DataTable({
  "lengthChange": false
});
            } );
            
             $(document).ready(function() {
               $('#example2').DataTable({
  "lengthChange": false
});
            } );
            
             $(document).ready(function() {
               $('#example3').DataTable({
  "lengthChange": false
});
            } );
            
            
        </script>
  
    
    
      <?php Session::put('alert',null) ; Session::put('promote-ads',null); ?>

        
        @include('footer.footer')
        
        
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>