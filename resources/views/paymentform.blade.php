<form id='form-pay' method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form" style='display:none;'>
        <div class="row" style="margin-bottom:40px;">
          <div class="col-md-8 col-md-offset-2">
            <p>
                <div>
                    Featured Ad &#8358;
                    <span id='amt'></span>
                </div>
            </p>
            <input type="hidden" name="email" value="{{Auth::user()->user_email}}"> {{-- required --}}
            <input type="hidden" name="orderID" value="time()">
            <input type="hidden" name="category" id='category' value="{{$featured->plans}}">
            <input type="text" style='display:none;' name="type" id='type' value="onemonth_BoostStart">
            <input type="text" style='display:none;' name="amount" id='amount' value="{{($featured->onemonth_BoostStart) * 100}}"> {{-- required in kobo --}}
            <input type="hidden" name="quantity" value="3">
            <input type="hidden" name="metadata" value="{{ json_encode($array = ['key_name' => 'value',]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
            <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
            <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
            {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

             <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}


            <p>
              <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">
              <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
              </button>
            </p>
          </div>
        </div>
</form>