<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="GrayGrids Team">
<title>Awi Market - Marketplace 4 Deltans</title>

<link rel="shortcut icon" href="assets/img/favicon.png">



<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
	<!-- End WOWSlider.com HEAD section -->
   

<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">



<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"  type="text/css">

 <link rel="stylesheet" href="css/home.css" />   
 

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>
    
    <div class="home-background" style='height:100%;overflow-y:auto;overflow-x:hidden!important;'>

         @include('header.header')
         
         
        <div id="content" style="margin-top:150px;">
<div class="container">
<div class="row">

<div class="product-info">
<div class="col-sm-8">
<div class="inner-box ads-details-wrapper">
<h2>{{$ads->title}}</h2>
<p class="item-intro"><span class="poster"><a href='sellerprofile?id={{$ads->user_id}}'>{{$obj->getSellerName($ads->user_id)}}</a> <span class="ui-bubble is-member">post am on </span> <span class="date">{{date('d-m-Y H:i:s',strtotime($ads->created_at))}}</span> E Location <span class="location">{{$obj->getCityName($ads->region)}}</span></p>
<div id="owl-demo" class="owl-carousel owl-theme">
    
@foreach($obj->getAllPhoto($ads->id) as $values)

<div class="item">
<img src="ad_photo/{{$values->pic_name}}" style='height:500px!important;' alt="">
</div>

@endforeach

</div>
</div>
<div class="box">
<h2 class="title-2"><strong>Wetin The Seller Talk</strong></h2>
<div class="row">
<div class="ads-details-info col-md-8">
<p class="mb15" style='word-wrap:break-word;'>{{$ads->details}}</p>
</div>
<div class="col-md-4">
<aside class="panel panel-body panel-details">
<ul>
<li>
<p class=" no-margin "><strong>Price:</strong> &#8358;{{$ads->price}}</p>
</li>
<li>
<p class="no-margin"><strong>Type:</strong> <a href="#">{{$obj->getCategoryName($ads->category)}}</a></p>
</li>
<li>
<p class="no-margin"><strong>The Area:</strong> <a href="#">{{$obj->getCityName($ads->region)}}</a></p>
</li>
<!--
<li>
<p class=" no-margin "><strong>Condition:</strong> Tear Rubber</p>
</li>
<li>
<p class="no-margin"><strong>Brand:</strong> <a href="#">Apple</a></p>
</li>
-->
</ul>
</aside>
<div class="ads-action">
<ul class="list-border">

<li>
    <i class=" fa fa-phone"></i> <span class="pointer-style hidden_nos" >08XXXXXXXXX</span><span class="hide h-nos1">{{$ads->phone}}</span>
</li>

    
<li>
    
  <i class="fa fa-star-o pointer-style rating" data-value='1' aria-hidden="true" style="font-size:30px;"></i> 
  &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value='2' aria-hidden="true" style="font-size:30px;"></i>
    &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value='3' aria-hidden="true" style="font-size:30px;"></i>
    &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value='4' aria-hidden="true" style="font-size:30px;"></i>
    &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value='5' aria-hidden="true" style="font-size:30px;"></i>
  
</li>

<li>
  Ad Rating: &nbsp; {{intval($rating_percentage)}}%
</li>


  <li>
        <span id='report_button' class=' pointer-style' data-toggle="modal" data-target="#reportSeller" >Report</span>
  </li>


@if(Auth::check())

     <li>
        <span  class='save_am pointer-style' >Save am </span>
     </li>

     <li>
       <span class='follow_am pointer-style' >Follow am </span>
     </li>
     
@endif



<li>
  <a href='sellerprofile?id={{$ads->user_id}}'>
    <i class=" fa fa-user"></i> Seller Name na {{$obj->getSellerName($ads->user_id)}}
  </a>
</li>
<!--
<li>
<a href="#"> <i class=" fa fa-heart"></i> Save Am</a></li>
<li>
-->
<a href="#"> <i class="fa fa-share-alt"></i> Show Other Pipo On </a>
<div class="social-link">
<input type="text" id="ad_id" value="{{Request::get('id')}}" style="display:none;" />

       <?php $url = 'https://www.awimarket.com/adDetails?id='.$ads->id; ?>

<a class="twitter" target="_blank" data-original-title="twitter" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fawimarket.com/adDetails?id={{$ads->id}}%2F&amp;src=sdkpreparse" data-toggle="tooltip" data-placement="top"><i class="fa fa-twitter"></i></a>
<a class="facebook" target="_blank" data-original-title="facebook" href="https://twitter.com/intent/tweet?text={{$url}}" data-toggle="tooltip" data-placement="top"><i class="fa fa-facebook"></i></a>
<a class="google" target="_blank" data-original-title="google-plus" href="https://plus.google.com/share?url={{$url}}" data-toggle="tooltip" data-placement="top"><i class="fa fa-google"></i></a>

</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-4">
<div class="inner-box">
<div class="widget-title">
<h4>Advertisement</h4>
</div>

@if($sidebar_ads != null)
 <?php if($obj->checkAd($val->id) == 1){ ?>
  <img src="ad_photo/{{$obj1->getPhoto($sidebar_ads->id)}}" alt="">
   <?php } ?>
@endif

</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.3s">
<div class="features-icon">
<i class="lnr lnr-star">
</i>
</div>
<div class="features-content">
<h4>
Fraud Protection
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.6s">
<div class="features-icon">
<i class="lnr lnr-chart-bars"></i>
</div>
<div class="features-content">
<h4>
No Extra Fees
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.9s">
<div class="features-icon">
<i class="lnr lnr-spell-check"></i>
</div>
<div class="features-content">
<h4>
Verified Data
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.9s">
<div class="features-icon">
<i class="lnr lnr-smile"></i>
</div>
<div class="features-content">
<h4>
Friendly Return Policy
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>

  

        
        <div class="wrapper" style='margin-bottom:100px;'>

<section id="categories-homepage">
<div class="container">
<div class="row">
<div class="col-md-12">

<section class="featured-lis">
<div class="container">
<div class="row">
<div class="col-md-12 wow fadeIn" data-wow-delay="0.5s">
<h3 class="section-title" style="text-transform:uppercase;">Other things wey you fit like</h3>
<div id="new-products" class="owl-carousel">

    @foreach($featured_ads as $val)

@if($obj1->checkAd($val->id) == 1)

<div class="item">
<div class="product-item">
<div class="carousel-thumb">
<a href="adDetails?id={{$val->id}}" >
<img src="ad_photo/{{$obj1->getPhoto($val->id)}}" height="150" width="200" alt=""></a>

<div class="overlay">
<a href="adDetails?id={{$val->id}}"><i class="fa fa-link"></i></a>
</div>
</div>
<a href="adDetails?id={{$val->id}}" class="item-name">{{substr($val->title,0,20)}}</a>
<span class="price">&#8358;{{number_format($val->price)}}</span>
</div>
</div>

@endif

@endforeach

</div>
</div>
</div>
</div>
</section>


</div>
    
<div class="modal fade" id="reportSeller" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Report Seller</h5>
      </div>
     <form action="reportSeller" method="get" >
      <div class="modal-body">
          	<!-- Search Form -->
				
					
                                            <div class="row">
                                              
                                                <div class="col-sm-12">
                                                    
                                                         @if(Session::get('report') == '1')
                                                         
                                                           
                                                            
                                                                 <div style='background-color: red;color:white;'>
                                                                     @foreach($errors->all() as $error)
                                                                     {{$error}}<BR>
                                                                     @endforeach
                                                                </div>
                                                         @endif
                                                    
                                                        <input type="text" class="form-control" name="seller" value="{{$ads->user_id}}" style="display:none;" />
                                                        <br>
                                                        <select name="reportreason" >
                                                            <option value=''>Select</option>
                                                            <option>7</option>
                                                            <option>8</option>
                                                        </select>
                                                        <br>
                                                        <textarea  class="form-control" name="reportDescription" placeholder='Report Description' ></textarea>
                                                        <br>
                                                        <input type="text" class="form-control"  name="id" value="{{$ads->id}}" style="display:none;" />
                                                        <br>
                                                        
                                               
                                                    
                                                </div>
                                               
                                            </div>
					
				</div> <!-- /.rbm_search -->

      
      <div class="modal-footer">
       
        <button type="Submit" class="btn btn-primary" style='z-index:99999999'>Submit</button>
        
        <button type="button" class="btn btn-primary" data-dismiss="modal" style='z-index:99999999'>Close</button>
       
      </div>
      </form> <!-- /form -->
  </div>
  </div>
</div>
    
    @if(Session::get('report') == '1')
    
          <script>
        
                $('#reportSeller').modal();
        
          </script>
    
    @endif
    
     @if(Session::get('report') == '2')
    
          <script>
        
                alert('Report Sent Successfully');
        
          </script>
    
    @endif
     
<?php Session::put('report',null);  ?>


</div>

<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>
    

<script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>

        <script type="text/javascript" src="js/app2.js"></script>
  
    
    
      
  
    </div>
    
        </div>
        
        @include('footer.footer')
        
        
    </div>
    
    
    
    
    
    
        
    
    
    
    
    
    </body>
</html>