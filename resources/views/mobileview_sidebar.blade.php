<!doctype html>
<div class='m-view' >
<html class='m-view' >
<head>
    

    

    
    <link rel="preconnect" href="//static.jiji.ng">
    <link rel="preconnect" href="https://images1.jiji.ng/">
    <link rel="preconnect" href="https://images2.jiji.ng/">
    <link rel="preconnect" href="https://images3.jiji.ng/">
    <link rel="preconnect" href="//maxcdn.bootstrapcdn.com/">
    
        <link rel="preconnect" href="//www.googleadservices.com/">
        <link rel="preconnect" href="//www.googletagservices.com/">
        <link rel="preconnect" href="//www.googletagmanager.com/">
    
    <link rel="preconnect" href="//www.google-analytics.com/">
    <link rel="preconnect" href="//connect.facebook.net/">
    <link rel="preconnect" href="//creativecdn.com/">
    
       <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">

<link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">

<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">

<link rel="stylesheet" href="assets/fonts/line-icons/line-icons.css" type="text/css">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">

<link rel="stylesheet" href="assets/extras/animate.css" type="text/css">

<link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">

<link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

<link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">

<link rel="stylesheet" href="assets/css/thumbnail-slider.css" type="text/css">
    
<script src="assets/js/thumbnail-slider.js" type="text/javascript"></script>

 <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
  
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"  type="text/css">

<script async="" src="https://creativecdn.com/tags?type=script&amp;id=pr_ti68pBbuUVxQ1dKUPPVS&amp;ncm=1"></script></head>
 <link rel="stylesheet" href="css/home.css" />

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 	
    
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
    
  
        <link href="css/portal-html4.min.aa048.css" type="text/css" rel="stylesheet"/>
    
  <link href="css/premium-landing-html4.min.a4f33.css" type="text/css" rel="stylesheet"/>


    
    
  <title>My account | Jiji.ng</title>

    

    
    
    

    <meta property="fb:app_id" content="294481384086302" />
    <meta property="fb:pages" content="932615913423241" />
    <meta property="fb:admins" content="alfred.camelopard" />
    <meta property="og:title" content="My account | Jiji.ng" />
    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="Jiji.ng — #1 Nigerian Online Marketplace"/>
    <meta name="apple-itunes-app" content="app-id=966165025">
    <meta name="application-name" content="Jiji.ng">
    <link rel="home" href="https://jiji.ng">

    

    

    

    

    
      <meta property="al:web:url" content="https://jiji.ng/sc/premium-services" />
      <meta property="al:web:should_fallback" content="false" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="msvalidate.01" content="43D104F179A249CABB5BEBF4D7A485B4" />
    
    <link rel="shortcut icon" href="/static/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/static/img/favicon.ico" type="image/x-icon">
    <link rel="manifest" href="/manifest.json">
    
 
    
    

    
        
    <script>
        window.path_for_static = "jiji.ng/static/";
        window.app_cookie_domain = ".jiji.ng";
        window.app_ga_params_cookie_name = "__ga_params";
        window.app_ga_params_data_attr_name = "ga_params";
        window.app_link_click_cookie_data_attr_name = "click-cookie-save-value";
        window.app_link_click_cookie_class = "js-click-cookie-save";
        window.is_dev = null;
    </script>

    

    
    
        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
            { (i[r].q=i[r].q||[]).push(arguments)}
            ,i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            var userId = "2749236";
            var config = {'cookieDomain': "jiji.ng"};
            if (userId) {
                config['userId'] = userId.toString();
            }
            ga('create', 'UA-54943881-1', config);
            ga('require', 'GTM-N3XPSQV');

            
            ga('set', 'dimension1', 'direct');
            
            
            ga('set', 'dimension2', 'web.facebook.com');
            
            ga('set', 'dimension3', 'week70');
            ga('set', 'dimension4', 'app_easy_apply:True;app_jiji_3969_paid_services:True;criteo_bidder:True;a_a2:True;app_rate_app_or_contact_us:False;app_send_message_text_chat:True;app_add_other_make:True;ctr_boost_v2:True;app_gather_acc:False;similar_ads_marketing:True;app_force_registration:False;app_advert_image_contact_buttons:True;app_gather_packages:False;app_google_plus_auth:True;app_category_unifying_verticalization:True;app_track_internet:True;app_advert_static_contact_buttons:False;jiji_3168_top_after_renew:False;ctr_factoring:True;app_category_real_estate_page:True;app_second_page_tops:True;app_advert_floating_seemore_button:True;app_mandatory_registration:True;test_flag:False;a_a:False;app_advert_new_contact_btn:True;app_post_ad_sorted_cat:True;app_force_fb_share:True;app_category_contact_buttons:True;personalised_should:True;jiji_3871_most_ctr_adverts:True;app_category_facebook_invite_banner:False;register_before_postad:True;app_live_chat:True;app_push_as_gallery:True;app_category_jobs_page:False;ctr_boost_4:True;ctr_boost_3:True;ctr_boost_2:False;ctr_boost_1:True;app_category_real_estate_test3:True;app_swipeable_adverts:True;app_sell_text_tab:True');
            ga('set', 'dimension5', 'app_easy_apply:True;app_jiji_3969_paid_services:True;criteo_bidder:True;a_a2:True;app_rate_app_or_contact_us:False;app_send_message_text_chat:True;app_add_other_make:True;ctr_boost_v2:True;app_gather_acc:False;similar_ads_marketing:True;app_force_registration:False;app_advert_image_contact_buttons:True;app_gather_packages:False;app_google_plus_auth:True;app_category_unifying_verticalization:True;app_track_internet:True;app_advert_static_contact_buttons:False;jiji_3168_top_after_renew:False;ctr_factoring:True;app_category_real_estate_page:True;app_second_page_tops:True;app_advert_floating_seemore_button:True;app_mandatory_registration:True;test_flag:False;a_a:False;app_advert_new_contact_btn:True;app_post_ad_sorted_cat:True;app_force_fb_share:True;app_category_contact_buttons:True;personalised_should:True;jiji_3871_most_ctr_adverts:True;app_category_facebook_invite_banner:False;register_before_postad:True;app_live_chat:True;app_push_as_gallery:True;app_category_jobs_page:False;ctr_boost_4:True;ctr_boost_3:True;ctr_boost_2:False;ctr_boost_1:True;app_category_real_estate_test3:True;app_swipeable_adverts:True;app_sell_text_tab:True');
            var ua = navigator.userAgent || navigator.vendor || window.opera;
            ga('set', 'dimension7',(ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1) ? 'Facebook' : 'None');
            ga('require', 'displayfeatures');
            ga('send', 'pageview');
        </script>
    




    
        
    

    
    

    

    <!-- Facebook Pixel Code -->
    <script>
    
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('addPixelId', '406794749719739', {"em": "eroindaclub@gmail.com", "fn": "Igho-Godwin", "ln": "Adeshola", "ph": null});
    fbq('track', 'PageView');


    
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=406794749719739&ev=PageView&noscript=1"
    /></noscript>

    <!-- End Facebook Pixel Code -->

    <meta name="theme-color" content="#558B2F">
</head>
<body id="base">
 
    
<script type="text/javascript">(function(e){var t;e&&(t=new Date,t.setTime((new Date).getTime()+3e5),document.cookie="_js="+e+";domain=.Jiji.ng;path=/;expires="+t.toUTCString())})("C7A828FF689A4A32C1E68DF22D376A2A80BA16A5D78782A550030A146172F03A22FA29F748DD2A71B9E864EEA521C00A6D6B42C812CF05E43B3AC7631EF618563749F6643BD6EA");</script>

    

    
    
    

    @include('paymentform')

    
    

    
    

    
    


    
        <!-- flag app_promo use for test. Don`t remove it, please  -->
    

    

    <div class="js-body-wrapper b-body-wrapper"
         id="js-vue-scope"
         data-web-id="1528123295##317e4e312f002bc8267825c7ced004b1d22178f4">
        
  


        
    


        
            <div class="h-ph-10 h-pb-10 h-pos-rel h-bg-white">
                
                @include('header.header')
    

                
  <div class="h-mh-10-negative">
      
  <div class="h-mb-10 h-mh-10">
    <a class="b-list-title h-a-without-underline"
        data-bazooka="go-back-link"
        href="/profile.html">
      <div class="h-dflex h-width-100p">
        <div class="h-flex-1-0 h-dflex h-flex-cross-center">
          <div class="b-list-title__arrow">
              <i class="h-icon icon-arrow-left"></i>
          </div>
          <div class="b-list-title__link h-bold">
              Premium Services
              

              

              
          </div>
        </div>
        
      </div>
    </a>
  </div>

  </div>

  
  <div class="h-flex b-professional-services__tabblock">
    <a href="promoteAd" class="b-professional-services__tab       
       "
       >
        Featured Plans
    </a>
      &nbsp;
    <a  href="sidebarAd" class="b-professional-services__tab  b-professional-services__tab_active " style='height:50px;'     
              >
        Sidebar Plans
    </a>
      &nbsp;
    <a href="billboardAd" class="b-professional-services__tab 
              "          
              >
        Billboard Plans
    </a>
  </div>


  <div class="b-professional-services__text">
      Promote
      <b>all your ads for <span id="for_month">month</span></b>
      and get your ads  <b>boosted and highlighted</b> in search and categories.
  </div>

  
  
  <div class="b-link-tabs h-text-center">
    
      <a id="month1"
        class="b-link-tabs__a
               h-pointer
               js-link-tabs__a
               
                 b-link-tabs__a_active
               "
        onclick="set_prices(1);setType(1);">
         1 month</a>
    
      <a id="month3"
        class="b-link-tabs__a
               h-pointer
               js-link-tabs__a
               "
        onclick="set_prices(3);setType(2);">
         3 months</a>
    
      <a id="month6"
        class="b-link-tabs__a
               h-pointer
               js-link-tabs__a
               "
        onclick="set_prices(6);setType(3);">
         6 months</a>
    
  </div>


  
  <div class="h-mh--10">
    <div class="js-select-package b-professional-services__packageblock">
      
      
        
        
        
        

        <div class="b-package">
          <div id="boost_start_unit"
               name="boost_business_1"
               class="b-package__unit b-package__unit_business">
            
            

            <i class="h-icon
                      icon-profile-active_boost
                      b-package__unit-active_boost"
               style="display: none">
            </i>
            <div class="b-package__unit-title">
              Boost
              <br>
              
                Start
              
            </div>
            <div class="b-package__unit-price">
              ₦
              <span id="start-price">
                  {{$featured->onemonth_BoostStart}}
              </span>
            </div>
            <button
              id="boost_business_1_button"
              data-package-id="boost_business_1"
              data-package-name="Boost Business 1 month"
              class="js-buy_button_boost
                     b-button b-button--secondary
                     b-button--color-disabled
                     b-button--shadow
                     h-width-80
                     b-button--border-radius
                     h-ph-15
                     b-button--size-small-2
                     h-mt-10">
                BUY
              </button>
          </div>
          <div class="b-package__advantages">
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-crown2"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     <i>5x</i> more clients
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-search_promo"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     Search results promotion
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-renew2"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     Automatic renewal of all ads every 3 days
                  </div>
              </div>
            

            <div class="b-package__advantages-item
                        
                          h-hidden-force
                        ">
              <div class="b-package__advantages-item__icon">
                <i class="h-icon icon-profile-moneybox">
                </i>
              </div>
              <div class="b-package__advantages-item__text h-dark-red">
                Save ₦
                <span id="discount_business">
                  0
                </span>
              </div>
            </div>
          </div>
        </div>
      
        
        
        
        

        <div class="b-package">
          <div id="boost_business_unit"
               name="boost_premium_1"
               class="b-package__unit b-package__unit_premium">
            
              <div class="b-package__unit-active">
                <i class="h-icon icon-profile-active"></i>
              </div>
            
            

            <i class="h-icon
                      icon-profile-active_boost
                      b-package__unit-active_boost"
               style="display: none">
            </i>
            <div class="b-package__unit-title">
              Boost
              <br>
              
                Business
              
            </div>
            <div class="b-package__unit-price">
              ₦
              <span id="business-price">
                 {{$featured->onemonth_boostBusiness}}
              </span>
            </div>
            <button
              id="boost_premium_1_button"
              data-package-id="boost_premium_1"
              data-package-name="Boost Premium 1 month"
              class="js-buy_button_boost
                     b-button b-button--secondary
                     b-button--color-disabled
                     b-button--shadow
                     h-width-80
                     b-button--border-radius
                     h-ph-15
                     b-button--size-small-2
                     h-mt-10">
                BUY
              </button>
          </div>
          <div class="b-package__advantages">
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-crown2"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     <i>7x</i> more clients
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-search_promo"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     Search results promotion
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-renew2"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     24 hours automatic renewal of all ads
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-sms"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     SMS-notifications on new messages
                  </div>
              </div>
            

            <div class="b-package__advantages-item
                        
                          h-hidden-force
                        ">
              <div class="b-package__advantages-item__icon">
                <i class="h-icon icon-profile-moneybox">
                </i>
              </div>
              <div class="b-package__advantages-item__text h-dark-red">
                Save ₦
                <span id="discount_premium">
                  0
                </span>
              </div>
            </div>
          </div>
        </div>
      
        
        
        
        

        <div class="b-package">
          <div id="boost_vip_unit"
               name="boost_premium_unit"
               class="b-package__unit b-package__unit_vip">
            
            
              <div class="b-personal-discount-label
                         "
                   id="vip-personal-discount-label">
                23%
              </div>
            

            <i class="h-icon
                      icon-profile-active_boost
                      b-package__unit-active_boost"
               style="display: none">
            </i>
            <div class="b-package__unit-title">
              Boost
              <br>
              
                Premium
              
            </div>
            <div class="b-package__unit-price">
              ₦
              <span id="premium-price">
                  {{$featured->onemonth_boostPremium}}
              </span>
            </div>
            <button
              id="boost_vip_1_button"
              data-package-id="boost_vip_1"
              data-package-name="Boost VIP 1 month"
              class="js-buy_button_boost
                     b-button b-button--secondary
                     b-button--color-disabled
                     b-button--shadow
                     h-width-80
                     b-button--border-radius
                     h-ph-15
                     b-button--size-small-2
                     h-mt-10">
                BUY
              </button>
          </div>
          <div class="b-package__advantages">
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-crown2"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     <i>10x</i> more clients
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-search_promo"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     Search results promotion
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-renew2"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     12 hours automatic renewal of all ads
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-sms"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     SMS-notifications on new messages
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          
                             important
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-crown3"></i>
                </div>
                  
                    <span class="h-bold h-mr-5 js-free-tops-count">
                      5
                    </span>
                  
                  <div class="b-package__advantages-item__text">
                     FREE TOPS!!!
                  </div>
              </div>
            
              <div class="b-package__advantages-item
                          ">
                <div class="b-package__advantages-item__icon">
                  <i class="h-icon icon-profile-medal"></i>
                </div>
                  
                  <div class="b-package__advantages-item__text">
                     Website link inclusion
                  </div>
              </div>
            

            <div class="b-package__advantages-item
                        ">
              <div class="b-package__advantages-item__icon">
                <i class="h-icon icon-profile-moneybox">
                </i>
              </div>
              <div class="b-package__advantages-item__text h-dark-red">
                Save ₦
                <span id="discount_vip">
                  6000
                </span>
              </div>
            </div>
          </div>
        </div>
      
    </div>
  </div>


  
  <div class="h-ph-10">
    <div class="b-professional-services__text
                h-text-left h-grey js-promotion">
      The maximum number of boosted adverts of one seller per day is 20.
      The logic of the promotion of adverts is based on the clients´ preferences.
      It means that some adverts can be boosted more frequently than others what
      brings you the best promotion results.
    </div>

    
  <div class="h-text-center h-pt-20 js-promotion">
    <a href="/compare-ad-types"
       class="b-button
              b-button--primary
              b-button--border-radius
              b-button--shadow
              qa-compare-ad-types">
        Compare ad types
    </a>
  </div>


    <div class="b-professional-services__title">About Boost Plans</div>
      <div class="b-professional-services__infoblock">
      <div class="b-professional-services__infoblock__img">
        <img src="/static/img/premium-landing-mobile/promotion-01.png" alt="">
      </div>
      <div class="b-professional-services__infoblock__text">
        <p>Highlighted on the first page</p>
         Boosted ads are highlighted on the first pages of the search results and categories. Boosted ads are also shown up under Top Ads and are promoted in Trending Ads section.
      </div>
    </div>

    <div class="b-professional-services__infoblock">
      <div class="b-professional-services__infoblock__img">
        <img src="/static/img/premium-landing-mobile/promotion-02.png" alt="">
      </div>
      <div class="b-professional-services__infoblock__text">
        <p><b>Boost your sales</b> with more advanced packages offered</p>
        We place your boosted ads on the first page alongside other boosted ads.  For your ads to show up as boosted more frequently, we advise you to choose the better package. The more advanced package you have, the more views you get and visitors you attract.
      </div>
    </div>

    <div class="b-professional-services__infoblock">
      <div class="b-professional-services__infoblock__img">
        <img src="/static/img/premium-landing-mobile/promotion-03.png" alt="">
      </div>
      <div class="b-professional-services__infoblock__text">
        <p><b>Auto renew</b> of all boosted ads</p>
        We show boosted ads even higher with auto renew feature. You don’t need to spend your time on renewing ads and get your ads actual, we do that automatically for you.
      </div>
    </div>

    <div class="b-professional-services__infoblock">
      <div class="b-professional-services__infoblock__img">
        <img src="/static/img/premium-landing-mobile/promotion-04.png" alt="">
      </div>
      <div class="b-professional-services__infoblock__text">
        <p><b>Email</b> promotion</p>
        We’ve developed special algorithms which track what users like most and email them such ads.
        Boosted ads we send with higher priority, so you have access up to 400 thousands of Jiji.ng registered users.
      </div>
    </div>

    
  <div class="b-professional-services__infoblock">
    <a href="#"
       rel="nofollow"
       class="b-button
              b-button--primary
              b-button--shadow
              b-button--border-radius
              h-ph-15
             h-width-200">
        CHOOSE NOW
    </a>
  </div>


    
        <div class="b-sellers">
    <div class="container">
      <div class="b-professional-services__title b-sellers__items__title">
        How many clients use
      </div>
      <div class="b-sellers__items__wrapper">
        <div class="b-sellers__items">
          <i class="h-icon icon-profile-premium-info-1"></i>
          <div class="b-sellers__items__text">
            <div>
                <span class="b-sellers__items__number">
                    15,622
                </span>
                sellers
            </div>
            have already bought
            
                Boost
            
            Promo to increase sales
          </div>
        </div>
        <div class="b-sellers__items">
          <i class="h-icon icon-profile-premium-info-2"></i>
          <div class="b-sellers__items__text">
            <div>
                <span class="b-sellers__items__number">
                    24,081,880
                </span>
                buyers
            </div>
            a month search goods on Jiji
            and the number is constantly growing
          </div>
        </div>
        <div class="b-sellers__items">
          <i class="h-icon icon-profile-premium-info-3"></i>
          <div class="b-sellers__items__text">
            <div>
                <span class="b-sellers__items__number">
                    292,616
                </span>
                ads
            </div>
            
                are boosted
            
            on Jiji right now
          </div>
        </div>
      </div>
    </div>
  </div>
    

    <div class="b-professional-services__title">Frequently Asked Question</div>

    <div class="b-faq">
      <div class="b-faq__titl">
        Benefits you get in comparison with standard ads
      </div>
      <div class="b-faq__icon">
        <i class="h-icon icon-profile-diagram"></i>
      </div>
      <div class="b-faq__deck">
        Standard ads usually get only 10% of visitors due to new ads constantly appearing on site. Boost ads get 5 times more clients, because we show them more often and you get more clients.
      </div>
    </div>

    <div class="b-faq">
      <div class="b-faq__titl">
        We care a lot of efficiency and we have only
        happy clients
      </div>
      <div class="b-faq__icon">
        <i class="h-icon icon-profile-feedback"></i>
      </div>
      <div class="b-faq__deck">
        We ask feedback every our client and if we turn moneyback if we receive unpleasant client. But, for now it 0,1% of all clients and mostly because something wrong with ads, not with traffic we are driving to Jiji.
      </div>
    </div>

    <div class="b-faq">
      <div class="b-faq__titl">
        If I don’t buy Boost packages what I loose?
      </div>
      <div class="b-faq__icon">
        <i class="h-icon icon-profile-cacl"></i>
      </div>
      <div class="b-faq__deck">
        You may stay with standard ads, you still get clients, but much less than your peers who bought Boost packages for our ads. That will lead to less sales. We recommend to try Boost packages, it’s a great way to get much more clients and you always have right to turn your money back.
      </div>
    </div>

    <div class="b-faq">
      <div class="b-faq__titl">
        How this magic with boost packages works?
      </div>
      <div class="b-faq__icon">
        <i class="h-icon icon-profile-money"></i>
      </div>
      <div class="b-faq__deck">
        We show boosted ads more often and much higher than standard ads. Also we automatically renewing boosted ads, so you don’t need to spend your time on renewing your ads.        </div>
    </div>

    <div class="b-faq">
      <div class="b-faq__titl">
        How to do all things right with boost packages?
      </div>
      <div class="b-faq__icon">
        <i class="h-icon icon-profile-cards"></i>
      </div>
      <div class="b-faq__deck">
        Pay with card. We use secure gateways of Interswitch and European banks, so all transactions are 100% secure.
      </div>
    </div>
  </div>


  <div class="b-professional-services__title">
      Feedback from our clients
  </div>
  
    <div class="b-professional-services__wall">
      <div class="b-professional-services__wall__user-info-block">
        <div class="b-professional-services__wall__user-info-block__avatar">
          <a href="https://play.google.com/store/people/details?id=100830108592340531527"
             class="b-professional-services__wall__user-info-block__avata__img"
             style="background-image:url('https://lh3.googleusercontent.com/-qfn7bd3CgPU/AAAAAAAAAAI/AAAAAAAAAHs/T8Ljl8eF0XM/w150-h150-rw/photo.jpg');"
          ></a>
        </div>
        <div class="b-professional-services__wall__user-info-block__name">
          Destiny Okon
        </div>
      </div>
      <div class="b-professional-services__wall__text">
        ‘Nice app Perfect site to buy and sell online...I love it a lot....keep up the good works..’
        <a href="https://play.google.com/store/people/details?id=100830108592340531527" class="h-block b-professional-services__wall__soc">
          <i class="h-icon icon-profile-soc-gp"></i>
        </a>
      </div>
    </div>
  
    <div class="b-professional-services__wall">
      <div class="b-professional-services__wall__user-info-block">
        <div class="b-professional-services__wall__user-info-block__avatar">
          <a href="https://play.google.com/store/people/details?id=102804057276722086709"
             class="b-professional-services__wall__user-info-block__avata__img"
             style="background-image:url('https://lh5.googleusercontent.com/-y5SF_CCHY2I/AAAAAAAAAAI/AAAAAAAAAFk/lEkbwS50s-I/w150-h150-rw/photo.jpg');"
          ></a>
        </div>
        <div class="b-professional-services__wall__user-info-block__name">
          Olutechnology
        </div>
      </div>
      <div class="b-professional-services__wall__text">
        ‘Best Customer Support The platform is excellent for local business owners and it exposes manufacturers and sellers to both local and international buyers.’
        <a href="https://play.google.com/store/people/details?id=102804057276722086709" class="h-block b-professional-services__wall__soc">
          <i class="h-icon icon-profile-soc-gp"></i>
        </a>
      </div>
    </div>
  
    <div class="b-professional-services__wall">
      <div class="b-professional-services__wall__user-info-block">
        <div class="b-professional-services__wall__user-info-block__avatar">
          <a href="https://play.google.com/store/people/details?id=111324150655425822557"
             class="b-professional-services__wall__user-info-block__avata__img"
             style="background-image:url('https://lh6.googleusercontent.com/-QSgI8Ii6a6k/AAAAAAAAAAI/AAAAAAAAAHM/dxAlWVMLf0g/w150-h150-rw/photo.jpg');"
          ></a>
        </div>
        <div class="b-professional-services__wall__user-info-block__name">
          SAMSON JOHN
        </div>
      </div>
      <div class="b-professional-services__wall__text">
        ‘Wonderful app Sold so many stuff, asap. That&#39;s a 5 star for you. Quick customer care. Thumbs up’
        <a href="https://play.google.com/store/people/details?id=111324150655425822557" class="h-block b-professional-services__wall__soc">
          <i class="h-icon icon-profile-soc-gp"></i>
        </a>
      </div>
    </div>

  
  <div class="b-benefits">
    <div class="container">
      <div class="h-flex b-block-title_light">
        Benefits of Boost Plans
      </div>

      <div class="b-benefits__wrapper">
        <div class="b-benefits__block">
          <div class="b-benefits__block__icon">
            <i class="h-icon icon-profile-benefits-largest"></i>
          </div>
          <div class="b-benefits__block__title">
            Jiji is the largest
          </div>
          <div class="b-benefits__block__deck">
            We are proud of the fact that we have the biggest traffic in Nigeria.
          </div>
        </div>

        <div class="b-benefits__block">
          <div class="b-benefits__block__icon">
            <i class="h-icon icon-profile-benefits-clients"></i>
          </div>
          <div class="b-benefits__block__title">
              Jiji has more clients
          </div>
          <div class="b-benefits__block__deck">
            We get our traffic only from relevant sources like Google and Facebook.
          </div>
        </div>

        <div class="b-benefits__block">
          <div class="b-benefits__block__icon">
            <i class="h-icon icon-profile-benefits-algorithms"></i>
          </div>
          <div class="b-benefits__block__title">
            Relevancy promotion algorithms
          </div>
          <div class="b-benefits__block__deck">
            We promote your ads only to those users who could be interested in
            your products. That gives you the biggest number of real clients.
          </div>
        </div>

        <div class="b-benefits__block">
          <div class="b-benefits__block__icon">
            <i class="h-icon icon-profile-benefits-support"></i>
          </div>
          <div class="b-benefits__block__title">
            Personal Support
          </div>
          <div class="b-benefits__block__deck">
            We have dedicated support who understand your needs very well and we
            guarantee you that you will be satisfied.
          </div>
        </div>

        <div class="b-benefits__block">
          <div class="b-benefits__block__icon">
              <i class="h-icon icon-profile-benefits-pig"></i>
          </div>
          <div class="b-benefits__block__title">
            Fair price
          </div>
          <div class="b-benefits__block__deck">
            We charge you in naira, so you won't pay any extra commissions.
          </div>
        </div>

        <div class="b-benefits__block">
          <div class="b-benefits__block__icon">
              <i class="h-icon icon-profile-benefits-secure"></i>
          </div>
          <div class="b-benefits__block__title">
              Jiji is secure
          </div>
          <div class="b-benefits__block__deck">
            We use secure gateways of Interswitch and European banks, so all
              transactions are 100% secure.
          </div>
        </div>
      </div>
    </div>
  </div>


  
  <div class="b-professional-services__infoblock">
    <a href="#"
       rel="nofollow"
       class="b-button
              b-button--primary
              b-button--shadow
              b-button--border-radius
              h-ph-15
             h-width-200">
        CHOOSE NOW
    </a>
  </div>


  

  <div class="js-popups-wrap b-fix-popup__wrapp">
    <div class="b-fix-popup__flex">

    
      
    

    
    <div id="success_popup"
 class="js-payment-popup qa-payment-success-popup b-payment-success-popup h-hidden">
        <div
            class="h-text-center h-bg-lightgrey h-pt-15 h-pb-10 h-ph-10"
            style="box-shadow: 0 1px 2px 0 rgba(0,0,0,.2)">
            <img
                class="h-width-auto h-inline-block"
                src="//jiji.ng/static/img/premium-landing/ok_pic.png">
            <h4 class="h-bold h-font-14">CONGRATULATIONS!</h3>
            <h4 class="h-font-14">YOUR PAYMENT HAS BEEN SUCCESSFULLY PROCESSED</h3>
        </div>
        <div class="h-text-center h-pt-15 h-pb-20 h-ph-10">
            <p class="h-mb-10">
                
                    <b class='js-package-name'>Package</b> is
                                  activated.Get more clients - post more ads.
                
            </p>
            <a
                target="_top"
                class="b-button b-button--primary b-button--shadow b-button--border-radius"
                
                    href="/add-free-ad.html"
                
                >POST AD</a>
        </div>

<a href="#" class="js-close h-icon icon-close b-payment-success-popup__close"></a>
    </div>

    
    <div
        id="decline_popup"
        class="js-payment-popup qa-payment-decline-popup b-payment-decline-popup h-hidden">
        <h3 class="b-payment-decline-popup__pachage-name">Buy <span class="js-package-name"></span></h3>
        <header class="h-bg-lightgrey">
            <h4 class="h-mv-0 h-mb-5">Payment error</h4>
            <div class="js-descr b-payment-decline-popup__error-descr">
            </div>
        </header>

        <section class="h-bg-lightgrey">
            <div class="h-hflex b-payment-decline-popup__data-row">
                <div class="h-flex-1-0-50p">Amount</div>
                <div class="h-flex-1-0-50p"><b><span class="js-amount"></span> ₦</b></div>
            </div>
            <div class="h-hflex b-payment-decline-popup__data-row h-mb-40">
                <div class="h-flex-1-0-50p">Type</div>
                <div class="h-flex-1-0-50p"><span class="js-package-name">...</span></div>
            </div>
            <button
                id="js-retry_button"
                class="js-retry b-button b-button--primary b-button--shadow b-button--border-radius b-button--size-medium h-mb-20">
                Try again
            </button>
            <div>
                <span class="b-payment-decline-popup__safety">Your transaction details are encrypted and are never shared with third parties.</span>
            </div>
        </section>
        
            <a href="#" class="js-close h-icon icon-close b-payment-decline-popup__close"></a>
        
    </div>

    
    <div
        id="refuse_popup"
        class="js-payment-popup qa-payment-refuse-popup b-payment-refuse-popup h-hidden h-text-center">

        <header class="h-bg-lightgrey h-text-center h-mb-15">
            <h3><b>Oooops!</b></h3>
        </header>
        <div class="h-ph-20">
            <img
                class="h-mb-20"
                style="max-width:390px;"
                src="//jiji.ng/static/img/premium-landing/promotion-01.png">
            <p class="js-package-message"></p>
            <button class="js-close b-button b-button--border-radius h-mb-20">Close</button>
        </div>

        <a href="#" class="js-close h-icon icon-close b-payment-refuse-popup__close"></a>
    </div>

    
    <div
        id="confirm_boost_purchase_popup"
        class="js-payment-popup
               qa-confirm-boost-purchase-popup
               b-confirm-boost-purchase-popup
               h-hidden
               h-text-center">
        <div>
            <p class="h-ph-25 h-mt-30 h-mb-10">
                Are you sure you want to buy <b class="js-package">this package</b>?
                New package will be activated once your previous package is expired.
            </p>
            <div class="
                b-confirm-boost-purchase-popup__nav
                h-hflex
                h-flex-main-center
                ">
                <button class="
                    js-ok
                    b-button
                    b-button--primary
                    b-button--border-radius
                    b-button--shadow
                    h-mh-10
                    ">
                    Ok
                </button>
                <button class="
                    js-close
                    b-button
                    b-button--border-radius
                    b-button--shadow
                    h-mh-10
                    ">
                    Cancel
                </button>
            </div>
        </div>

        <a href="#" class="js-close h-icon icon-close b-confirm-boost-purchase-popup__close"></a>
    </div>

    </div>
  </div>
  
    
        
    


            </div>
        

        
            <footer class="h-pv-20 h-flex-0-0 js-mobile-footer">
    <div class="h-ph-15">
      <div class="h-dflex h-pb-10">
          <div class="h-flex-0-0-50p">
            <a href="https://itunes.apple.com/us/app/jiji.ng/id966165025"
               target="_blank"
               rel="nofollow"
               data-ga_params='["iOS_App", "Click_button"]'
               class="h-inline-block"
            >
                <i class="h-icon icon-app_store"></i>
            </a>
          </div>
          <div class="h-flex-0-0-50p">
           <a href="https://play.google.com/store/apps/details?id=ng.jiji.app&referrer=utm_source%3Djiji_site%26utm_medium%3Dmobile"
               target="_blank"
               rel="nofollow"
               data-ga_params='["Android_App", "Click_button"]'
               class="h-inline-block"
            >
                <i class="h-icon icon-google_play"></i>
            </a>
          </div>
      </div>

      <div class="h-pv-10">
        <div class="h-dflex h-flex-wrap h-mb-15">
          
            <a href="https://jiji.ng/hiring.html"
               rel="nofollow" target="_blank"
               class="b-footer__link"
            >We are hiring!</a>
          
          <a href="https://jiji.ng/faq"
             rel="nofollow" target="_blank"
             class="b-footer__link"
          >FAQ</a>
          <a href="https://jiji.ng/rules.html"
             rel="nofollow" target="_blank"
             class="b-footer__link"
          >Rules</a>
          <a href="https://jiji.ng/privacy.html"
             rel="nofollow" target="_blank"
             class="b-footer__link"
          >Privacy</a>
          <a href="https://jiji.ng/billing-policy.html"
             rel="nofollow" target="_blank"
             class="b-footer__link"
          >Billing policy</a>
          <a href="https://jiji.ng/contacts.html"
             rel="nofollow" target="_blank" class="b-footer__link"
          >Contact Us</a>
          <a href="https://jiji.ng/safety-tips.html"
             rel="nofollow" target="_blank" class="b-footer__link"
          >Jiji Tips</a>
          <a href="https://jiji.ng/company"
              rel="nofollow" target="_blank" class="b-footer__link"
          >Jiji Sellers</a>
          <a href="https://jiji.ng/brand"
              rel="nofollow" target="_blank" class="b-footer__link"
          >Brand</a>
        </div>

        <div class="h-text-center">
          <a href="/"
              class="b-footer__link"
          >Free Classifields in Nigeria</a>
        </div>

      </div>

      @include('footer.footer')
    </div>
  </footer>
        
    </div>

    <div class="js-subscribe-push-notification"></div>

    
        
    <div class="js-subscribe-push-notification-block b-subscribe-push-notification"
        data-subscribe="b-subscribe-push-notification__link"
        data-close="b-subscribe-push-notification__close"
        data-tracking-url="/web_push/tracking"
        style="display: none"
    >
        <div class="container h-dflex h-flex-main-center h-flex-cross-center h-height-100p h-pos-rel h-flex-dir-column">
            <div>
                Enable Push notification.
            </div>
            <span class="b-subscribe-push-notification__link">
                Click here
            </span>

            <div class="b-subscribe-push-notification__close"></div>
        </div>
    </div>

    

    
        <script  src="js/portal-html4-libs-js.min.2ea91.js" type="text/javascript"></script>
        <script  src="js/portal-html4-js.min.812c1.js" type="text/javascript"></script>
    

    
    <script type="text/javascript">
        (function($, csrf_token){
            $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
                        xhr.setRequestHeader("X-CSRFToken", csrf_token);
                    }
                }
            });
        })($, "1528123295##317e4e312f002bc8267825c7ced004b1d22178f4");
    </script>


    
  
        
    <script type="text/javascript">
    (function(){
        var x = document.createElement('script'); x.async = true;
        x.src = "//creativecdn.com/tags?type=script&id=pr_ti68pBbuUVxQ1dKUPPVS&ncm=1";
        document.getElementsByTagName('head')[0].appendChild(x);
    }());
    </script>

        
    

    
        

        <!-- Google Code for Качественный переход Conversion Page -->
        
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 974703807;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "kZNuCOXu3VoQv5nj0AM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/974703807/?label=kZNuCOXu3VoQv5nj0AM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>

            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 872666061;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "V6dMCNzwmWoQzaePoAM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/872666061/?label=V6dMCNzwmWoQzaePoAM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>

            <!-- Google Code for Conversion Page -->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 962248679;
            var google_conversion_language = "en";
            var google_conversion_format = "3";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "OSmcCMPC0GAQ5__qygM";
            var google_remarketing_only = false;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/962248679/?label=OSmcCMPC0GAQ5__qygM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript>
        
    

    
    
        
    


        
    
  
    <!-- End Google Tag Manager -->
    
   <div id="featureflags" data-value="app_easy_apply:True;app_jiji_3969_paid_services:True;criteo_bidder:True;a_a2:True;app_rate_app_or_contact_us:False;app_send_message_text_chat:True;app_add_other_make:True;ctr_boost_v2:True;app_gather_acc:False;similar_ads_marketing:True;app_force_registration:False;app_advert_image_contact_buttons:True;app_gather_packages:False;app_google_plus_auth:True;app_category_unifying_verticalization:True;app_track_internet:True;app_advert_static_contact_buttons:False;jiji_3168_top_after_renew:False;ctr_factoring:True;app_category_real_estate_page:True;app_second_page_tops:True;app_advert_floating_seemore_button:True;app_mandatory_registration:True;test_flag:False;a_a:False;app_advert_new_contact_btn:True;app_post_ad_sorted_cat:True;app_force_fb_share:True;app_category_contact_buttons:True;personalised_should:True;jiji_3871_most_ctr_adverts:True;app_category_facebook_invite_banner:False;register_before_postad:True;app_live_chat:True;app_push_as_gallery:True;app_category_jobs_page:False;ctr_boost_4:True;ctr_boost_3:True;ctr_boost_2:False;ctr_boost_1:True;app_category_real_estate_test3:True;app_swipeable_adverts:True;app_sell_text_tab:True"></div>

        
    

        
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=294481384086302&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    
  <script  src="js/premium-landing-html4-js.min.7af3b.js" type="text/javascript"></script>


    
    
        
    


    
    
    
   
    


    <!-- Not OperaMini -->
    <script type="text/javascript">document.addEventListener("DOMContentLoaded", function(event) { document.getElementsByTagName("body")[0].className += " DOMisLoaded";window.jiji.vars.rtbhouse_tracking = true;
new window.premium_landing_view(
      'boost',
      {"boost_business_1": {"amount": 1, "duration": 30, "main_type": "boost", "personal_discount_percent": 0, "price": 7999, "price_for_item": 7999, "price_old": 7999, "title": "Boost Business 1 month", "total_discount": 0}, "boost_business_3": {"amount": 3, "duration": 90, "main_type": "boost", "personal_discount_percent": 0, "price": 16999, "price_for_item": 5666, "price_old": 16999, "title": "Boost Business 3 months", "total_discount": 6998}, "boost_business_6": {"amount": 6, "duration": 180, "main_type": "boost", "personal_discount_percent": 0, "price": 29999, "price_for_item": 4999, "price_old": 29999, "title": "Boost Business 6 months", "total_discount": 17995}, "boost_premium_1": {"amount": 1, "duration": 30, "main_type": "boost", "personal_discount_percent": 0, "price": 14999, "price_for_item": 14999, "price_old": 14999, "title": "Boost Premium 1 month", "total_discount": 0}, "boost_premium_3": {"amount": 3, "duration": 90, "main_type": "boost", "personal_discount_percent": 0, "price": 29999, "price_for_item": 9999, "price_old": 29999, "title": "Boost Premium 3 months", "total_discount": 14998}, "boost_premium_6": {"amount": 6, "duration": 180, "main_type": "boost", "personal_discount_percent": 0, "price": 53999, "price_for_item": 8999, "price_old": 53999, "title": "Boost Premium 6 months", "total_discount": 35995}, "boost_vip_1": {"amount": 1, "duration": 30, "main_type": "boost", "personal_discount_percent": 23, "price": 19999, "price_for_item": 19999, "price_old": 25999, "title": "Boost VIP 1 month", "total_discount": 6000}, "boost_vip_3": {"amount": 3, "duration": 90, "main_type": "boost", "personal_discount_percent": 0, "price": 39999, "price_for_item": 13333, "price_old": 39999, "title": "Boost VIP 3 months", "total_discount": 19998}, "boost_vip_6": {"amount": 6, "duration": 180, "main_type": "boost", "personal_discount_percent": 0, "price": 75999, "price_for_item": 12666, "price_old": 75999, "title": "Boost VIP 6 months", "total_discount": 43995}},
      false,
      null,
      ["business", "premium", "vip"],
      {"1": "5", "3": "10", "6": "25"}
    )
window.jiji.analytics.set_visits_cookie("4");
$('input, textarea').placeholder();; });</script>
    
     <script>
        if(!window.jiji) { window.jiji = {} }
        window.jiji.flagFeatures = {"a_a": true, "a_a2": false, "app_advert_floating_seemore_button": false, "app_advert_image_contact_buttons": true, "app_advert_new_contact_btn": true, "app_advert_static_contact_buttons": false, "app_category_contact_buttons": true, "app_category_facebook_invite_banner": false, "app_category_jobs_page": true, "app_category_real_estate_page": true, "app_category_real_estate_test3": true, "app_category_unifying_verticalization": true, "app_delivery": true, "app_discount_landing_design": true, "app_easy_apply": true, "app_force_fb_share": true, "app_force_registration": false, "app_gather_acc": false, "app_gather_packages": false, "app_google_plus_auth": true, "app_mandatory_registration": true, "app_post_ad_sorted_cat": true, "app_push_as_gallery": false, "app_rate_app_or_contact_us": true, "app_second_page_tops": false, "app_send_message_text_chat": true, "app_swipeable_adverts": true, "app_track_internet": true, "criteo_bidder": true, "fuzzy_search": true, "jiji_3168_top_after_renew": true, "jiji_3871_most_ctr_adverts": true, "jiji_3969_recommendation_system": true, "personalised_should": false, "sms_infobip": false, "test_flag": false}
    </script>
        <script src="js/portal-libs-js.min.e4938.js.download" type="text/javascript"></script><div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="><div class="lb-nav"><a class="lb-prev" href="https://jiji.ng/sc/premium-services"></a><a class="lb-next" href="https://jiji.ng/sc/premium-services"></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div></div>
        <script src="js/portal-js.min.0a32f.js.download" type="text/javascript"></script>
    

    
    <script type="text/javascript">document.addEventListener("DOMContentLoaded", function(event) { document.getElementsByTagName("body")[0].className += " DOMisLoaded";window.jiji.vars.rtbhouse_tracking = true;

jiji.views.premiumLanding(
        jQuery,
        {"boost_business_1": {"amount": 1, "duration": 30, "main_type": "boost", "ngn_format": "{{$featured->onemonth_boostBusiness}}", "ngn_old_format": "{{$featured->onemonth_boostBusiness}}", "personal_discount_percent": 0, "price_for_item": "{{$featured->onemonth_boostBusiness}}", "title": "Boost Business 1 month", "total_discount": "0"}, "boost_business_3": {"amount": 3, "duration": 90, "main_type": "boost", "ngn_format": "{{$featured->threemonth_boostBusiness}}", "ngn_old_format": "{{$featured->threemonth_boostBusiness}}", "personal_discount_percent": 0, "price_for_item": "5,666", "title": "Boost Business 3 months", "total_discount": "6,998"}, "boost_business_6": {"amount": 6, "duration": 180, "main_type": "boost", "ngn_format": "{{$featured->sixmonth_boostBusiness}}", "ngn_old_format": "{{$featured->sixmonth_boostBusiness}}", "personal_discount_percent": 0, "price_for_item": "4,999", "title": "Boost Business 6 months", "total_discount": "17,995"}, "boost_premium_1": {"amount": 1, "duration": 30, "main_type": "boost", "ngn_format": "{{$featured->onemonth_boostPremium}}", "ngn_old_format": "{{$featured->onemonth_boostPremium}}", "personal_discount_percent": 0, "price_for_item": "14,999", "title": "Boost Premium 1 month", "total_discount": "0"}, "boost_premium_3": {"amount": 3, "duration": 90, "main_type": "boost", "ngn_format": "{{$featured->threemonth_boostPremium}}", "ngn_old_format": "{{$featured->threemonth_boostPremium}}", "personal_discount_percent": 0, "price_for_item": "9,999", "title": "Boost Premium 3 months", "total_discount": "14,998"}, "boost_premium_6": {"amount": 6, "duration": 180, "main_type": "boost", "ngn_format": "{{$featured->sixmonth_boostPremium}}", "ngn_old_format": "{{$featured->sixmonth_boostPremium}}", "personal_discount_percent": 0, "price_for_item": "8,999", "title": "Boost Premium 6 months", "total_discount": "35,995"}, "boost_start_1": {"amount": 1, "duration": 30, "main_type": "boost", "ngn_format": "{{$featured->onemonth_BoostStart}}", "ngn_old_format": "{{$featured->onemonth_BoostStart}}", "personal_discount_percent": 0, "price_for_item": "6,999", "title": "Boost Start 1 month", "total_discount": "0"}, "boost_start_3": {"amount": 3, "duration": 90, "main_type": "boost", "ngn_format": "{{$featured->threemonth_BoostStart}}", "ngn_old_format": "15,999", "personal_discount_percent": 0, "price_for_item": "5,333", "title": "Boost Start 3 months", "total_discount": "4,998"}, "boost_start_6": {"amount": 6, "duration": 180, "main_type": "boost", "ngn_format": "{{$featured->sixmonth_BoostStart}}", "ngn_old_format": "{{$featured->sixmonth_BoostStart}}", "personal_discount_percent": 0, "price_for_item": "4,666", "title": "Boost Start 6 months", "total_discount": "13,995"}},
        null,
        ["start", "business", "premium"]
      );
window.jiji.analytics.set_visits_cookie("done");; });</script>
    <!-- DesktopVersion -->

    <!-- MobileVersion -->
    <!-- DEBUG INFO
        
    -->
   
  <script src="./Premium Services on Jiji.ng ▷ Boost your ads_files/premium-landing.min.0f17c.js.download" type="text/javascript"></script>

<script type="text/javascript" src="js/app2.js"></script>
      
<div class="h-height-0 h-overflow-hidden">
    <img height="1" width="1" alt="" src="https://n.mailfire.io/online/30"/>
</div>



<script type="text/javascript" src="assets/js/material.min.js"></script>
<script type="text/javascript" src="assets/js/material-kit.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/wow.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>  
 
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 	
    
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>       
    
</body>
</html>

</div>
