<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\models\codes;
use App\models\photo;
use App\models\users;
use App\models\tags;
use Request;
use Jusibe;
use Hash;
use Validator;
use Session;
use Auth;
use Image;
use Illuminate\Validation\Rule;
use ImageOptimizer;


class RegisterController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
    public function addTemPhoto(){
         $data = Request::file('photo');
       
        
      //  echo 2;
          
          if (Request::hasFile('photo')) {
              
               $image = Request::file('photo');
               $name = time().'.'.$image->getClientOriginalExtension();
               $destinationPath = public_path('/temp');
               $image->move($destinationPath, $name);        
               ImageOptimizer::optimize($destinationPath.'/'.$name);
          
               echo $name;
         
          
          }
          
         // var_dump($data);
    
    }
    
    public function update_receive_newsletter(){
        
        $value = Request::all();
        
       
 
            $obj = users::find(Auth::id());
            
            if(isset($value['newsletter'])){
                $obj->newsletter = 1; 
            }
            else{
                $obj->newsletter = 0; 
            }
            
            if(isset($value['advice'])){
                $obj->advice = 1; 
            }
            else{
                $obj->advice = 0; 
            }
            
          
            $obj->save();
            
            Session::put('success3','1');
            
            return redirect('userprofile');
        
    }
    
    public function changepassword()
    {
        
        $value = Request::all();
        
        $rules = [
            
            'password' => 'required',
            'password_confirm' => 'required|same:password' 
         
        ];
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            
            Session::put('error2','1');
            return redirect('userprofile')->withErrors($validator);
            
            
        }
        else{
            $obj = users::find(Auth::id());
            
            $obj->password = Hash::make($value['password']);
            
            if(isset($value['enable_comments'])){
            
                $obj->enable_comments = 1;
                    
            }
            else{
                $obj->enable_comments = 0;
            }
            
            $obj->save();
            
            Session::put('success2','1');
            
            return redirect('userprofile');
        }
        
    }
    
    public function UpdateDetails(){
        
        $value = Request::all();
        
        $rules = [
            'email' => 'required|unique:users,user_email,'.Auth::id().'|max:100|email',
            'seller_url' => 'required|unique:users,seller_url,'.Auth::id().'|max:100',
            'phone' => 'required|numeric|unique:users',
            'firstname' => 'required',
            'lastname' => 'required',
         
         
        ];
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            Session::put('error1','1');
            return redirect('userprofile')->withErrors($validator);
        }
        else{
        
        $obj = users::find(Auth::id());
        
        $obj->user_email = $value['email'];
        
        $obj->phone = $value['phone'];
        
        $obj->company_name = $value['company_name'];
        
        $obj->address = $value['address'];
        
        $obj->offer_d_services = $value['offer_d_services'];
        
        $obj->firstname = $value['firstname'];
        
        $obj->lastname =  $value['lastname'];
        
        $tags = new tags();
        
        $tags = '';
        
        if(isset($value['tags']))
        {
          foreach($value['tags'] as $tg)
          {
            $tags .= $tg .',';
               
          }
          
           $tags = substr($tags,0,strlen($tags)-1);
        }
        
       
        
        $obj->tags = $tags ;
        
        $obj->about_company =  $value['about_company'];
        
        $obj->seller_url =  $value['seller_url'];
       
        $obj->save();
        
        Session::put('success','1');
        
        return redirect('userprofile');
        
        
                
        }
    }
    
    public function register()
    {
        $value = Request::all();
        
        if(!isset($value['terms'])){
        
          $value['terms']='';
        }
        
        $rules = [
            'email' => 'required|unique:users,user_email|max:100|email',
            'password' => 'required',
            'phone-1' => 'required|numeric|unique:users,phone',
            'firstname' => 'required',
            'lastname' => 'required',
            'terms' => 'required|min:1',
            'code' => 'required'
        ];
        
      //  echo $value['password'];
        
        Session::put('email',$value['email']);
        
        Session::put('password',$value['password']);
        
        Session::put('phone-1',$value['phone-1']);
        
        Session::put('firstname', $value['firstname']);
        
        Session::put('lastname',$value['lastname']);
        
        Session::put('terms',$value['terms']);
        
        Session::put('code',$value['code']);
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            return redirect('register2')->withErrors($validator);
        }
        else{
            
            $val = codes::where('phone',$value['phone-1'])->orderby('id','asc')->take(1)->get();
        
            if(sizeof($val) > 0){
             
                $code_obj = codes::find($val[0]['id']);
                
                $code_value = $val[0]['code'];
            
                $code_obj->delete();
            }
            else
            {
              Session::put('error','1');
              return redirect('register2');
            }
            
            if(!($code_value == $value['code']) ){
                Session::put('error2','1');
                return redirect('register2');
            }
        
            $obj = new users();
        
            $obj->user_email = $value['email'];
        
            $obj->password =  Hash::make($value['password']);
        
            $obj->phone = $value['phone-1'];
        
            $obj->firstname = $value['firstname'];
        
            $obj->lastname =  $value['lastname'];
            
            $obj->remember_token =  'Nil';
        
            $obj->save();
            
            Session::put('email',null);
        
            Session::put('password',null);
        
            Session::put('phone-1',null);
        
            Session::put('firstname', null);
        
            Session::put('lastname',null);
        
            Session::put('terms',null);
        
            Session::put('code',null);
            
        }
        
       
        
    }
    
    public function addPhoto(){
        
        $data = Request::file('photo');
       
        $validator = Validator::make(Request::all(), [
        'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg,JPG|max:2048',
        ]);
        
        //echo '4';
        
        if($validator->fails()){
            
            echo '1';
           
        }
        else{
            
          try{
          
          if (Request::hasFile('photo')) {
          $image = Request::file('photo');
          $name = time().'.'.$image->getClientOriginalExtension();
          $destinationPath = public_path('/userprofile_img');
          $image->move($destinationPath, $name);        
          ImageOptimizer::optimize($destinationPath.'/'.$name);
          
          $obj = users::find(Auth::id());
          $obj->pic_name = $name;
         // $obj->user_id = Auth::id();
          $obj->save();
          
           echo $name;
          
          }
          }
          catch(\Exception $e){
              echo $e;
          }
        //  

       // return back()->with('success','Image Upload successfully');
            }
   
        
        
        
        
    }
    
    public function genCode()
    {
        $value = Request::all();
        
        if(!isset($value['phone']))
        {
            echo "Please enter a value for phone no";
        }
        else
        {
            $code =  rand(123457824, 987694780);
            
            $code = substr($code,0,4);
            
            $payload = [
               'to' => $value['phone'],
               'from' => 'Awi market',
               'message' => $code
               ];
            
             
                $cod_id = codes::where('phone',$value['phone'])->orderby('id','asc')->take(1)->get();
                
                //var_dump($cod_id);
                
                if(sizeof($cod_id) > 0)
                {
                    $cd = codes::find($cod_id[0]['id']);
                    
                    $cd->delete();
                    
                    //return redirect('kk');
                }
                
            
            try{
                Jusibe::sendSMS($payload)->getResponse();
               
                $codes = new codes();
            
                $codes->code =  $code;
                    
                $codes->phone = $value['phone'];
            
                $codes->save();
                
                echo "code Sent";
            }
            catch (\Exception $ex) {
                echo "Error please try again";
            }
            
        }
        
    }
    
     
    
    
   
}
