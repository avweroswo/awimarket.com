<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\models\cities;
use App\models\codes;
use App\models\photo;
use App\models\users;
use App\models\tags;
use App\models\contact;
use App\models\billboard;
use App\models\sidebar;
use App\models\navigation;
use App\models\logo;
use App\models\ads;
use App\models\admin;
use App\models\category;
use App\models\categories;
use App\models\subcategory2;
use App\models\reportedSeller;
use Request;
use Jusibe;
use DB;
use Hash;
use Validator;
use Session;
use Auth;
use Image;
use Illuminate\Validation\Rule;
use ImageOptimizer;


class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function delete_report()
    {
        $obj = reportedSeller::find(Request::get('id'));
        
        $obj->delete();
        
        return redirect('admindashboard');
    }
    
    public function getStat($month,$year,$week,$week2)
    {
       if(Request::get('month')==null)
       {
          $month = date('m');
          $year  = date('Y');
          $week = 1;
          $week2 = $week+6;
       }
       
        $topSearches_month =  DB::table('allsearch')
                          ->select(DB::raw('count(id) as searches_count,  search_value as searchvalue'))
                          ->whereMonth('created_at', '=' , $month)
                          ->whereYear('created_at', '=' , $year)
                          ->whereBetween('created_at',[$year.'-'.$month.'-'.$week,$year.'-'.$month.'-'.$week2])
                          ->groupBy('search_value')
                          ->orderby('created_at','asc')
                          ->get();
        
         $failedSearches_month =  DB::table('failedsearch')
                          ->select(DB::raw('count(id) as searches_count, search_value as searchvalue'))
                          ->whereMonth('created_at', '=' , $month)
                          ->whereYear('created_at', '=' , $year)
                          ->whereBetween('created_at',[$year.'-'.$month.'-'.$week,$year.'-'.$month.'-'.$week2])
                          ->groupBy('search_value')
                          ->orderby('created_at','asc')
                          ->get();
         
        $topSellers =  DB::table('ads')
                          ->select(DB::raw('count(id) as searches_count,  user_id as users'))
                          ->whereMonth('created_at', '=' , $month)
                          ->whereYear('created_at', '=' , $year)
                          ->whereBetween('created_at',[$year.'-'.$month.'-'.$week,$year.'-'.$month.'-'.$week2])
                          ->groupBy('users')
                          ->orderby('created_at','asc')
                          ->get();
        
        $NewSellers =  DB::table('ads')
                          ->select(DB::raw('distinct(user_id) as user_id'))
                          ->whereMonth('created_at', '=' , $month)
                          ->whereYear('created_at', '=' , $year)
                          ->whereBetween('created_at',[$year.'-'.$month.'-'.$week,$year.'-'.$month.'-'.$week2])
                          //->groupBy('users')
                          ->orderby('created_at','asc')
                          ->get();
        
        //dd($NewSellers);
         
         return [$topSearches_month,$failedSearches_month,$topSellers,$NewSellers];
         
         
    }
    
    public function authenticate()
    {
        if(Auth::guard('admins')->attempt(['username' => Request::get('username'), 'password' => Request::get('password')])) {
          
            return redirect('admindashboard');
        }
        else{
            Session::put('admin_login',1);
            return redirect('adminLogin');
        }
    }
    
    public function login()
    {
        return view('admin.login');
    }
    
    public function admindashboard(){
            
        if(Auth::guard('admins')->check())
        {
   
           if(Request::get('month')==null)
            {
                $month = date('m');
                $year  = date('Y');
                $week = 1;
                $week2 = $week+6;
            }
            else{
                $month = Request::get('month');
                $year  = Request::get('year');
                $week  = Request::get('week');
                $week2 = $week+6;
                if($week == 23)
                {
                    $week2 = $week+8;
                }
                if($month<10)
                {
                    $month = '0'.$month; 
                }
               // dd($year);
            }
            
            Session::put('Stat',$this->getStat($month,$year,$week,$week2));
        
            $users = users::where('user_status','0')->get();
            
            $all_contact = contact::all();
            
            $all_billboard = billboard::all();
            
            $all_sidebars = sidebar::all();
            
            $all_navs = navigation::all();
            
            $all_categorys = category::all();
            
            $all_tags = tags::all();
            
            $all_admin = admin::all();
            
            $all_cities = cities::all();
            
            $objs_admin = new adminController();
            
            $reportedSeller = reportedSeller::all();
            
            Session::put('admin','1');
            
            $obj = new HomeController2();
            
           // dd(DB::table('plans')->get());
            
            return view('admin.dashboard')->with(['all_users' => $users,'obj'=>$obj,'all_plans'=>DB::table('plans')->get(),'all_ads'=>ads::all(),'reports'=>$reportedSeller,'all_cities'=>$all_cities,'all_admin'=>$all_admin,'all_tags'=>$all_tags, 'objs_admin' => $objs_admin, 'all_categorys' => $all_categorys,'all_contact' => $all_contact,'all_billboard' => $all_billboard,'all_sidebars' => $all_sidebars , 'all_navs' => $all_navs]);
            
        }
        else{
            return redirect('adminLogin');
        }
        
    }
    
    public function verify_user()
    {
        $users = users::find(Request::get('user_id'));
        
        $users->verified = 1;
        
        $users->save();
        
        Session::put('verify','1');
        
        return redirect('admindashboard');
    }
    
    public function EditPromotedAds()
    {
        $validator = Validator::make(Request::all(), [
         'one_month_boost_start' => 'required',
         'three_month_boost_start' => 'required',
         'six_month_boost_start'  => 'required',
            
         'one_month_boost_business' => 'required',
         'three_month_boost_business' => 'required',
         'six_month_boost_business'  => 'required',
            
         'one_month_boost_premium' => 'required',
         'three_month_boost_premium' => 'required',
         'six_month_boost_premium'  => 'required',
            
        ]);
        
        if($validator->fails()){
            
            Session::put('edit_p_ads','1');
         
            return redirect('admindashboard?id='.Request::get('ids'))->withErrors($validator);
            
        }
        else{
         
             $obj = DB::table('plans')->where('plans',Request::get('id'))
                 ->update([
                     
                     'onemonth_BoostStart' => Request::get('one_month_boost_start'),
                     
                     'threemonth_BoostStart' => Request::get('three_month_boost_start'),
                     
                     'sixmonth_BoostStart'  => Request::get('six_month_boost_start'),
                     
                     'onemonth_boostBusiness' => Request::get('one_month_boost_business'),
                     
                     'threemonth_boostBusiness' => Request::get('three_month_boost_business'),
                     
                     'sixmonth_boostBusiness'  => Request::get('six_month_boost_business'),
                     
                     'onemonth_boostPremium' => Request::get('one_month_boost_business'),
                     
                     'threemonth_boostPremium' => Request::get('three_month_boost_business'),
                     
                     'sixmonth_boostPremium'  => Request::get('six_month_boost_business'),
                            
                            
                     ]);
             
             Session::put('edit_p_ads','2');
             
             return redirect('admindashboard?id='.Request::get('ids'));
        }
        
        
        
        
        
        
    }
    
    public function addCity()
    {
        $validator = Validator::make(Request::all(), [
         'city' => 'required|unique:cities,city',
        ]);
        
        if($validator->fails()){
            
            Session::put('city_error',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
            $obj = new cities();
            $obj->city = Request::get('city');
            
            if (Request::hasFile('city_pic')) {
              
               $image = Request::file('city_pic');
               $name = time().'.'.$image->getClientOriginalExtension();
               $destinationPath = public_path('/city_pic');
               $image->move($destinationPath, $name);        
               ImageOptimizer::optimize($destinationPath.'/'.$name);
               $obj->pic_name = $name;
          
          
          }
          
            $obj->save();
          
            Session::put('city_error',2); 
          
            return redirect('admindashboard');
          
            
        }
        
    }
    
    public function delete_city()
    {
        $obj = cities::find(Request::get('id'));
        
        $obj->delete();
        
        Session::put('editcity_error',2); 
          
        return redirect('admindashboard'); 
        
        
    }
    
    public function edit_city()
    {
        $validator = Validator::make(Request::all(), [
         'city' => 'required|unique:cities,city,'.Request::get('id').'',
        ]);
        
        
        
        if($validator->fails()){
            
            Session::put('editcity_error',1); 
            
            return redirect('admindashboard?id='.Request::get('id'))->withErrors($validator);
           
        }
        else{
            
            $obj = cities::find(Request::get('id'));
            
             if (Request::hasFile('city_pic')) {
              
               $image = Request::file('city_pic');
               $name = time().'.'.$image->getClientOriginalExtension();
               $destinationPath = public_path('/city_pic');
               $image->move($destinationPath, $name);        
               ImageOptimizer::optimize($destinationPath.'/'.$name);
               $obj->pic_name = $name;
          
          
          }
          
            $obj->city = Request::get('city');
            $obj->save();
          
            Session::put('editcity_error',2); 
          
            return redirect('admindashboard');
          
            
        }
        
    }
    
    public function add_tags(){
        
        $validator = Validator::make(Request::all(), [
        'tag' => 'required|unique:tags,tag_name|max:200',
        ]);
        
        //echo '4';
        
        if($validator->fails()){
            
            Session::put('tag_error',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
        
          $obj = new tags();
          $obj->tag_name = Request::get('tag');
          $obj->save();
          
          Session::put('tag_error',2); 
          
          return redirect('admindashboard');
          
          
        
        }
        
    }
    
    public function getSubs1($id)
    {
       $sub = category::find($id)->subCategory;
       
       $text ='';
       
       foreach($sub as $val){
           
           $text.=$val->cat_name.',';
           
           
       }
       
       return substr($text,0,strlen($text)-1);
    }
    
    public function getSubs1_two($id)
    {
       $sub = category::find($id)->subCategory;
       
       $text ='';$text2 ='';
      
       foreach($sub as $val){
           
           $text.= $val->id.',';
           
           $text2.= $val->cat_name.',';
           
       }
       
       return substr($text,0,strlen($text)-1).';;'.substr($text2,0,strlen($text2)-1);
    }
    
    public function getSubs1_ids($id)
    {
       $sub = category::find($id)->subCategory;
       
       $text ='';
       
       foreach($sub as $val){
           
           $text.=$val->id.',';
           
           
       }
       
       return substr($text,0,strlen($text)-1);
    }
    
    public function getSubs2($ids)
    { 
       $ids_all = explode(',',$ids);
       
       $text ='';
       
       foreach($ids_all as $val){
               
           
           
           if(isset(categories::find($val)->subCategory2)){
               
            $i =0;
           
            $sub =  categories::find($val)->subCategory2;
            
             foreach($sub as $val){
                 
                 $i++;
                 
                if($val->cat_name !== ''){
           
                   $text.=$val->cat_name.',';
                }
                
                if($i == sizeof($sub)){
                    
                    $text = substr($text,0,strlen($text)-1);
            
                    $text.=';';
                
                }
           
            }
            
           
           
           }
           
            
       
        
       }
       
             return substr($text,0,strlen($text)-1);

    }
    
    public function getSubs2_two($id)
    { 
        
        $text='';$id_text='';
     
           
           
           if(isset(categories::find($id)->subCategory2)){
               
            $i =0;
           
            $sub =  categories::find($id)->subCategory2;
            
             foreach($sub as $val){
                 
                 $i++;
                 
                if($val->cat_name !== ''){
           
                   $text.=$val->cat_name.',';
                   
                   $id_text.=$val->id.',';
                }
                
               
            }
            
          
       }
       
             return substr($text,0,strlen($text)-1).'))'.substr($id_text,0,strlen($id_text)-1);

    }

    public function add_billboard()
    {
        
       
       
        $validator = Validator::make(Request::all(), [
        'photo' => 'required|mimes:jpeg,png,jpg,gif,svg,JPG|max:2048',
        ]);
        
        //echo '4';
        
        if($validator->fails()){
            
            Session::put('photo_error',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
        Session::put('photo_error',2); 
            
        $image = Request::file('photo');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/billboard');
        $image->move($destinationPath, $name);        
        ImageOptimizer::optimize($destinationPath.'/'.$name);
          
        $obj = new billboard();
        
        $obj->picture = $name;
                
        $obj->link = Request::get('hyperlink');
        
        $obj->save();
        
        Session::put('photo_error',2); 
            
        return redirect('admindashboard');
        
        }
        
    }
    
    public function add_logo()
    {
        
       
       
        $validator = Validator::make(Request::all(), [
        'photo' => 'required|mimes:jpeg,png,jpg,gif,svg,JPG|max:2048',
        ]);
        
        //echo '4';
        
        if($validator->fails()){
            
            Session::put('photo_error_logo',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
             $count = logo::all()->count();
             
             if($count == 0){
                 
                Session::put('photo_error_logo',2); 
            
                $image = Request::file('photo');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/logo');
                $image->move($destinationPath, $name);        
                ImageOptimizer::optimize($destinationPath.'/'.$name);
          
                $obj = new logo();
        
                $obj->logo = $name;
               
                $obj->save();
    
                return redirect('admindashboard');
            
             }
             else{
                 
                Session::put('photo_error_logo',2); 
            
                $image = Request::file('photo');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/logo');
                $image->move($destinationPath, $name);        
                ImageOptimizer::optimize($destinationPath.'/'.$name);
          
                $obj = logo::find(1);
        
                $obj->logo = $name;
               
                $obj->save();
    
                return redirect('admindashboard');
                 
             }
        
        
        
        }
        
    }
    
    public function add_sidebar()
    {
        
       
       
        $validator = Validator::make(Request::all(), [
        'photo' => 'required|mimes:jpeg,png,jpg,gif,svg,JPG|max:2048',
        ]);
        
        //echo '4';
        
        if($validator->fails()){
            
            Session::put('photo_error_side',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
        Session::put('photo_error_side',2); 
            
        $image = Request::file('photo');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/sidebar');
        $image->move($destinationPath, $name);        
        ImageOptimizer::optimize($destinationPath.'/'.$name);
          
        $obj = new sidebar();
        
        $obj->picture = $name;
                
        $obj->link = Request::get('hyperlink');
        
        $obj->save();
        
        Session::put('photo_error_side',2); 
            
        return redirect('admindashboard');
        
        }
        
    }
    
     public function add_admin()
    {
        
        $value = Request::all();
       
        $validator = Validator::make(Request::all(), [
            
        'username' => 'required|unique:admin|max:100',
            
        'password' => 'required',
       
        
        ]);
        
        //echo '4';
        
        if($validator->fails()){
            
            Session::put('admin_error',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
            $obj = new admin();
            
            $obj->username = $value['username'];
            
            $obj->password = Hash::make($value['password']);
            
            $obj->save();
            
            Session::put('admin_error',2); 
            
            return redirect('admindashboard')->withErrors($validator);
        }
        
    }
    
     public function add_nav()
    {
        
       
       
        $validator = Validator::make(Request::all(), [
            
        'text' => 'required|unique:navigation,text|max:200',
        'hyperlink' => 'required|max:200',
         'position' => 'required|numeric',
        
        ]);
        
        //echo '4';
        
        if($validator->fails()){
            
            Session::put('nav_error',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
        Session::put('nav_error',2); 
      
        $obj = new navigation();
        
        $obj->text = Request::get('text');
                
        $obj->link = Request::get('hyperlink');
        
        $obj->position = Request::get('position');
        
        $obj->save();
        
        Session::put('nav_error',2); 
            
        return redirect('admindashboard');
        
        }
        
    }
    
    public function add_category()
    {
        $value = Request::all();
        
        
        $rules = [
            'category' => 'required',
            
          
        ];
        
      
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            Session::put('cat_error',1); 
            Session::put('category',$value['category']);
            Session::put('subcategory',$value['Subcategory']);
            Session::put('subs',$value['Subs']);
            Session::put('icon_data',$value['icon_data']);
            return redirect('admindashboard')->withErrors($validator);
        }
        else{
            
           
            $obj = new category();
        
            $obj->cat_name = $value['category'];
            
            $obj->icon_data = Request::get('icon_data');
        
            $obj->save();
            
            $cat_id = category::orderby('id','desc')->take(1)->get()[0]->id ;
            
            $sub_cat_array = explode(",",Request::get('Subcategory'));
            
            $sub_cat_array2 = explode(";",Request::get('Subs'));
            
            $z =0;
            
            foreach($sub_cat_array2 as $values){
                
                $sub_cat_array3[$z] = explode(',',$values);
                
                $z++;
            }
            
            $cat_id2 = "";
            
            $obj_admin = new adminController();
            
            foreach($sub_cat_array as $values)
            {
                
                $cat_id2 .= $obj_admin->add_subCategory($values,$cat_id).',';
              
            }
            
            $cat_id2 = substr($cat_id2,0,strlen($cat_id2)-1);
            
            $cat_id2 = explode(",",$cat_id2);
            
           // dd($cat_id2);
            
         //   dd($sub_cat_array2);
            
            $i = 0;$j=0;
            
            for($q=0;$q<sizeof($sub_cat_array3);$q++)
            {
                for($q1=0;$q1<sizeof($sub_cat_array3[$q]);$q1++)
                {
                     
                     $obj_admin->add_subCategory2($sub_cat_array3[$q][$q1],$cat_id2[$q]);
                    
                }
            }
            
            foreach($sub_cat_array3 as $values){
                
                
           
            }
            
            Session::put('category','');
            Session::put('subcategory','');
            Session::put('subs','');
           
          
            Session::put('cat_error',2); 
            return redirect('admindashboard');
        
           
        }
    }    

     public function edit_category()
    {
        $value = Request::all();
        
        
        $rules = [
            'category' => 'required',
            
          
        ];
        
      
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            Session::put('cat_edit_error',1); 
            
            return redirect('admindashboard')->withErrors($validator);
        }
        else{
            
           
            $obj = category::find($value['id']);
        
            $obj->cat_name = $value['category'];
        
            $obj->save();
            
            $cat_id = $value['id'] ;
            
            $sub_cat_array = explode(",",Request::get('Subcategory'));
            
            $sub_cat_array2 = explode(";",Request::get('Subs'));
            
            $z =0;
            
            foreach($sub_cat_array2 as $values){
                
                $sub_cat_array3[$z] = explode(',',$values);
                
                $z++;
            }
            
            $cat_id2 = "";
            
            $obj_admin = new adminController();
            
            foreach($sub_cat_array as $values)
            {
                
                $cat_id2 .= $obj_admin->edit_subCategory($values,$cat_id).',';
              
            }
            
            $cat_id2 = substr($cat_id2,0,strlen($cat_id2)-1);
            
            $cat_id2 = explode(",",$cat_id2);
            
           // dd($cat_id2);
            
         //   dd($sub_cat_array2);
            
            $i = 0;$j=0;
            
            for($q=0;$q<sizeof($sub_cat_array3);$q++)
            {
                for($q1=0;$q1<sizeof($sub_cat_array3[$q]);$q1++)
                {
                     
                     $obj_admin->edit_subCategory2($sub_cat_array3[$q][$q1],$cat_id2[$q]);
                    
                }
            }
            
            foreach($sub_cat_array3 as $values){
                
                
           
            }
            
            Session::put('category','');
            Session::put('subcategory','');
            Session::put('subs','');
           
          
            Session::put('cat_error',2); 
            return redirect('admindashboard');
        
           
        }
    }        
  
    public function add_subCategory($cat_name,$cat_id)
    {
        $obj = new categories();
        
        $obj->cat_name = $cat_name;
        
        $obj->category_id = $cat_id;
       
        $obj->save();
        
        return categories::orderby('id','desc')->take(1)->get()[0]->id ;
     
    }
    
    public function add_subCategory2($cat_name,$cat_id)
    {
        $obj = new subCategory2();
        
        $obj->cat_name = $cat_name;
        
        $obj->categories_id = $cat_id;
       
        $obj->save();
        
        //return subCategory::latest()->first()->id;
     
    }
    
    public function edit_subCategory($cat_name,$cat_id)
    {
        
        if(categories::find($cat_id)!==null){
            
            $obj = categories::find($cat_id);
        
            $obj->cat_name = $cat_name;
        
            $obj->category_id = $cat_id;
       
            $obj->save();
            
            return $cat_id;
        }
        else{
            return $this->add_subCategory($cat_name,$cat_id);
        }
        
        
        
     
    }
    
    public function edit_subCategory2($cat_name,$cat_id)
    {
        
        if(subCategory2::find($cat_id)!==null){
            
            $obj = subCategory2::find($cat_id);
        
            $obj->cat_name = $cat_name;
        
            $obj->categories_id = $cat_id;
       
            $obj->save();
            
            return $cat_id;
        }
        else{
            return $this->add_subCategory2($cat_name,$cat_id);
        }
      
     
    }
    
    
    
    
     public function edit_sidebar()
    {
        
       
       
        $validator = Validator::make(Request::all(), [
        'photo' => 'required|mimes:jpeg,png,jpg,gif,svg,JPG|max:2048',
        ]);
        
        //echo '4';
        
        if(Request::hasfile('photo')){
        
        if($validator->fails()){
            
            Session::put('side_edit',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
        Session::put('side_edit',2); 
            
        $image = Request::file('photo');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/sidebar');
        $image->move($destinationPath, $name);        
        ImageOptimizer::optimize($destinationPath.'/'.$name);
          
        $obj = sidebar::find(Request::get('id'));
        
        $obj->picture = $name;
                
        $obj->link = Request::get('hyperlink');
        
        $obj->save();
        
        Session::put('side_edit',2); 
            
        return redirect('admindashboard');
        
        }
       }
       else{
           
        $validator = Validator::make(Request::all(), [
        'hyperlink' => 'required|max:100',
        ]);
        
         if($validator->fails()){
            
            Session::put('side_edit',1); 
            
            return redirect('admindashboard?id='.Request::get('id'))->withErrors($validator);
           
        }
        else{
            
             $obj = sidebar::find(Request::get('id'));
            
             $obj->link = Request::get('hyperlink');
        
             $obj->save();
        
             Session::put('side_edit',2); 
            
             return redirect('admindashboard');
        }
          
       }
        
    }
    
     public function edit_nav()
    {
        
       
       
       $validator = Validator::make(Request::all(), [
            
        'text' => 'required|unique:navigation,text,'.Request::get('id').'|max:200',
        'hyperlink' => 'required|max:200',
        'position' => 'required|numeric',
        
        ]);
        
   
        
        if($validator->fails()){
            
            Session::put('nav_edit',1); 
            
            return redirect('admindashboard?id='.Request::get('id'))->withErrors($validator);
           
        }
        else{
            
        Session::put('nav_edit',2); 
       
          
        $obj = navigation::find(Request::get('id'));
        
        $obj->text = Request::get('text');
        
        $obj->position = Request::get('position');
                
        $obj->link = Request::get('hyperlink');
        
        $obj->save();
        
        Session::put('nav_edit',2); 
            
        return redirect('admindashboard');
        
        }
        
     
        
    }
    
     public function edit_billboard()
    {
        
       
       
        $validator = Validator::make(Request::all(), [
        'photo' => 'required|mimes:jpeg,png,jpg,gif,svg,JPG|max:2048',
        ]);
        
        //echo '4';
        
        if(Request::hasfile('photo')){
        
        if($validator->fails()){
            
            Session::put('bill_edit',1); 
            
            return redirect('admindashboard')->withErrors($validator);
           
        }
        else{
            
        Session::put('bill_edit',2); 
            
        $image = Request::file('photo');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/billboard');
        $image->move($destinationPath, $name);        
        ImageOptimizer::optimize($destinationPath.'/'.$name);
          
        $obj = billboard::find(Request::get('id'));
        
        $obj->picture = $name;
                
        $obj->link = Request::get('hyperlink');
        
        $obj->save();
        
        Session::put('bill_edit',2); 
            
        return redirect('admindashboard');
        
        }
       }
       else{
           
        $validator = Validator::make(Request::all(), [
        'hyperlink' => 'required|max:100',
        ]);
        
         if($validator->fails()){
            
            Session::put('bill_edit',1); 
            
            return redirect('admindashboard?id='.Request::get('id'))->withErrors($validator);
           
        }
        else{
            
             $obj = billboard::find(Request::get('id'));
            
             $obj->link = Request::get('hyperlink');
        
             $obj->save();
        
             Session::put('bill_edit',2); 
            
             return redirect('admindashboard');
        }
          
       }
        
    }
    
    public function  delete_billboards(){
        
        $users = billboard::find(Request::get('id'));
         
        $users->delete();
            
        return redirect('admindashboard');
    }
    
    public function  delete_sidebar(){
        
        $users = sidebar::find(Request::get('id'));
         
        $users->delete();
            
        return redirect('admindashboard');
    }
    
     public function  delete_tags(){
        
        $users = tags::find(Request::get('id'));
         
        $users->delete();
            
        return redirect('admindashboard');
    }
    
    public function  delete_nav(){
        
        $users = navigation::find(Request::get('id'));
         
        $users->delete();
            
        return redirect('admindashboard');
    }
    
     public function  delete_admin(){
        
        $users = admin::find(Request::get('id'));
         
        $users->delete();
            
        return redirect('admindashboard');
    }
    
    public function delete_user(){
        
        $users = users::find(Request::get('id'));
        
        $users->user_status = '1';
        
        $users->save();
            
        return redirect('admindashboard');
        
    }
    
   
    
    public function delete_category(){
        
        $users = category::find(Request::get('id'));
        
        $users->delete();
            
        return redirect('admindashboard');
        
    }
    
    public function edit_user(){
        
        $value = Request::all();
        
        $rules = [
            
            'user_email' => 'required|unique:users,user_email,'.Request::get('user_id').'|max:100|email',
           // 'password_confirm' => 'required|same:password' 
         
        ];
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            Session::put('user_email_error','1');
            return redirect('admindashboard?user_id='.Request::get('user_id'))->withErrors($validator);
        }
        else{
        
            
        Session::put('user_email_error','2');
        
        $users = users::find(Request::get('user_id'));
        
        $users->user_email = Request::get('user_email');
        
        if(Request::get('user_pass')!='')
        {
        
           $users->password = Hash::make(Request::get('user_pass'));
        
        }
        
        $users->save();
            
        return redirect('admindashboard');
        
    }
    
    }
    
  
    
   
}
