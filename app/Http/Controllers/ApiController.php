<?php



namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Jusibe;
use Auth;
use Session;
use Socialite;
use App\models\billboard;
use App\models\rating;
use App\models\category;
use App\models\FollowList;
use App\models\SavedList;
use App\models\categories;
use App\models\subcategory2;
use App\models\cities;
use App\models\ads;
use App\models\adspic;
use App\models\users;
use App\models\navigation;
use DB;
use Image;
use Request;
use Carbon\Carbon;
use App\models\SubCatValues;
use App\models\MaxViews;
use App\models\popularcategories;
use App\models\failedsearch;
use App\models\allsearch;
use App\models\makeawish;
use Validator;
use App\Http\Controllers\HomeController2;

class ApiController extends BaseController
{
   
   private $url='http://localhost:8000/';
   
   
   
   function initialise(){
      // $url='http://localhost:8000/';
   }
   
   public function adDetailsPage()
   {
       header('Access-Control-Allow-Origin: *'); 
       
       Session::put('Api','1');
       $obj = new HomeController2();
       
       return $obj->adDetails();
       
   }
        
   
   public function navigation_all()
   {
        header('Access-Control-Allow-Origin: *'); 
       
       return json_encode(navigation::orderby('position','asc')->get());
       
   }
   
   public function setCategoryerror_to_null()
   {
       header('Access-Control-Allow-Origin: *');
       Session::put('category_error',null);
   }
   
   public function category_all()
   {
       header('Access-Control-Allow-Origin: *');
       return json_encode(category::all());
   }
   
   public function ApiGetFeaturedAds()
   {
       header('Access-Control-Allow-Origin: *');
       Session::put('Api','1');
       $obj = new HomeController2();
       return json_encode($obj->getFeatured_ads());
   }
   
   
   
   public function ads_tags()
   {
       header('Access-Control-Allow-Origin: *');
       
       $ads = ads::all();
       
       $a =0;
       
       foreach($ads as $value)
       {
           $category[$a] = $this->getCatName($value->category);
           
           $a++;
       }
       
       return json_encode([ads::all(),$category]);
   }
   
   public function GetApiHeaderAndFooter()
   {
       
        header('Access-Control-Allow-Origin: *');
       
       $header = '<style>
    .ui-widget-content {
    border: 1px solid #dddddd !important;
    background: white; 
    color: #333333 !important;
    z-index:99999999999 !important;
}
</style>
     <!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
		<link href="css/responsive_bootstrap_modal_popup.css" rel="stylesheet" media="all">  
<div class="header1 hh" style="" >
   
<div class="container-color-padding" >
             <div class="row">
              <div class="col-sm-12">
              
               <div class="pull-left" >
                   <div class="dropdown">
                       <button onclick="myFunction()" class="dropbtn border-radius-10">
                           <div class="ham-burger"></div>
                           <div class="ham-burger"></div>
                           <div class="ham-burger"></div>
                           <i class="fa fa-angle-down" style="display:none;"></i>
                       </button>
  <div id="myDropdown" class="dropdown-content" style="z-index:300;">
    <input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
    <a href="aboutus">About</a>
    <a href="faq">Faq</a>
    <a href="contactus">Contact</a>
    
    <div id="navs">
        
    </div>
   
    
  </div>
</div>
               </div>
                 
               
                 <div class="pull-left left-logo" >
                     <a href="/"> 
                         <img src="images/logo.svg" class="img-responsive" style="height:30px;padding-right:30px;"  />
                     </a> 
                 </div>
                  
                  <div class="pull-right" style="padding-right:10px;">
                   <input class="search-in" type="text" name="searchbbar" id="tags" style="border:1px solid black;padding:3px 15px;border-radius:5px;"/>
                     <a data-toggle="modal" href="#rbm_search" class="rbm_modal_trigger font-search click-add" style="margin:0px;padding:3px 15px;" data-backdrop="true">search</a> 
                     
                   <!-- <a data-toggle="modal" data-target="#exampleModalCenter" data-backdrop="true" class="font-search click-add" style="margin:0px;padding:3px 15px;" >search</a> -->
                 </div>
              
                  
              </div>
               
            
             </div>
           </div>
    




    
    <!-- Responsive Bootstrap Modal Popup -->
	<div id="rbm_search" class="modal fade rbm_modal rbm_form_general rbm_size_search rbm_center rbm_bd_semi_trnsp rbm_bd_black rbm_bg_white rbm_blue rbm_none_radius rbm_shadow_lg_black rbm_animate rbm_duration_md rbmFadeInDown rbm_easeOutQuint" role="dialog">

		<!-- Modal Dialog-->
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content" style="background-color:white;">

				<!-- Search Form -->
				<div class="rbm_search">
					<!-- form -->
					<form  action="searchresults" method="get">
						<div class="rbm_input_txt">
                                                   
                                                      <input type="search"  id="tags11" name="search_value" placeholder="i am looking for..." required>
                                                   
						</div>
						<div class="rbm_form_submit">
							<button onclick="search()" type="button" class="click-add">search</button>
						</div>
					</form> <!-- /form -->
				</div> <!-- /.rbm_search -->

			</div> <!-- /Modal content-->
		</div> <!-- /Modal Dialog-->
	</div> 

        


 <!--
 
 <nav class="navbar navbar-default ddx" style="">
        <div class="nav nav-justified navbar-nav">
 
            <form class="navbar-form navbar-search" role="search" align="center">
                <div class="input-group">
        
                    <input type="text" class="form-control padding-left-right-60" style="height:34px;" >
                
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-search  color-purple click-add">
                            <span class="glyphicon glyphicon-search"></span>
                            <span class="label-icon">Search</span>
                        </button>
                        <button type="button" class="btn  dropdown-toggle color-purple click-add" data-toggle="dropdown" >
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu" >
                            <li>
                                <a href="#">
                                    <span class="glyphicon glyphicon-user"></span>
                                    <span class="label-icon">Search By User</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-book"></span>
                                <span class="label-icon">Search By Organization</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>  
            </form>   
         
        </div>
    </nav>
 
 -->
</div>
<!--
    <div class="row">
		        
                    <div class="btn-float">
                        <button type="button" class="btn btn-default btn-circle btn-xl btn-lateral click-add color-purple color-white dx-m"><i class="fa fa-user"></i></button><br>
                        <button type="button" class="btn btn-default btn-circle btn-xl btn-lateral click-add color-purple color-white dx-m"><i class="fa fa-plus-circle"></i></button>

                    </div>
        
    </div>

-->
<div onclick="myFunction2()">
        <div class="navmenu navmenu-default navmenu-fixed-left offcanvas" style="z-index:7000">

<div class="close" data-toggle="offcanvas" data-target=".navmenu">
<i class="fa fa-close"></i>
</div>
<h3 class="title-menu">All Categories</h3>
<ul class="nav navmenu-nav" id="all_cat"> 
    
</ul>
</div> 
	<!-- End WOWSlider.com BODY section -->   
        
        <div class="tbtn wow pulse click-add" id="menu" data-wow-iteration="infinite" data-wow-duration="500ms" data-toggle="offcanvas" data-target=".navmenu">
            <p class="reduce-width-10"><i class="fa fa-file-text-o"></i> <span class="display-0">Categories</span></p>
</div>
        
                    <!--fin buttons -->

  <script>
  $( function() {
   
  } );
  </script>
  
  
 <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">   
 <link rel="stylesheet" href="'.$this->url.'Material-Floating-Toggle/collapzion.min.css">


<script src="'.$this->url.'Material-Floating-Toggle/collapzion.min.js"></script>

    <div id="btncollapzion" class=" btn_collapzion dx-m   pulse animated  " style="z-index:6000;"></div>

<script>


</script>

<style>
    a._collapz_parant._close:after,
    a._collapz_parant._open:after {
    content: "\f0a6";
    font-family: FontAwesome;
    font-size: 30px;
    position: relative;
    top: 16px;
    
    font-style: normal;
    color:inherit;
}

</STYLE>




        </div>
';
  
       $footer = '                 
            
            <script src="'.$this->url.'js/responsive_bootstrap_modal_popup_min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/material.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/material-kit.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/jquery.parallax.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/wow.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/main.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/form-validator.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/contact-form-script.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="'.$this->url.'assets/js/jquery.themepunch.tools.min.js"></script>
<script src="'.$this->url.'assets/js/bootstrap-select.min.js"></script>

        <script type="text/javascript" src="'.$this->url.'js/app2.js"></script>
 
            
      

        
        



<div class="row padding-10 color-purple footer1 ff" style="z-index:20000 !important;margin-left:0px !important;" >
        
            
          
          <div class="login">
          <div class="col-sm-12"  style="color:white;">
          <div class="col-sm-6 ">&COPY;'.date('Y',time()).' All Rights Reserved</div>
            <div class="col-sm-6 text-right" >
             <div class="bottom-social-icons social-icon">
               <a class="facebook" target="_blank" href="https://web.facebook.com/GrayGrids"><i class="fa fa-facebook"></i></a>
               <a class="twitter" target="_blank" href="https://twitter.com/GrayGrids"><i class="fa fa-twitter"></i></a>
               <a class="dribble" target="_blank" href="https://dribbble.com/"><i class="fa fa-dribbble"></i></a>
               <a class="flickr" target="_blank" href="https://www.flickr.com/"><i class="fa fa-flickr"></i></a>
               <a class="youtube" target="_blank" href="https://youtube.com"><i class="fa fa-youtube"></i></a>
               <a class="google-plus" target="_blank" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
               <a class="linkedin" target="_blank" href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a>
             </div>
          </div>
          </div>
          <div>
          </div>
          
 </div>
          
          <div class="SignedOut">
            <div class="col-sm-12 color-white " align="center"  >
             
                <a href="signup"> <button class="border-radius-10 click-properties">Register</button></a>
              &nbsp;<a href="signup?login=1"> <button class="border-radius-10 click-properties">Login</button></a>
            </div>
          </div>
        
            
         
        
  
        
</div>

        </div> ';
       
          return json_encode([$header,$footer]);
   }
   
   public function getCatName($id)
   {
              
      // Auth::loginUsingId(97);
       header('Access-Control-Allow-Origin: *');
       
       if(category::find($id))
       {

       
       return category::find($id)->cat_name ;
       
       }
   }
   
   public function login(){
       
       header('Access-Control-Allow-Origin: *');
       
   
       
      // Auth::loginUsingId(97);
       
       
       
   }
   
   public function getPopularCategory()
   {
       $pp1='';$pp2='';$a=0;$cat_id='';
       
       header('Access-Control-Allow-Origin: *');
       
       $popularcategories = popularcategories::all();
       
       $obj = new HomeController2();
       
       foreach($popularcategories as $val)
       {
           
       
               $pp1.= '<div  class="col-md-2 focus-grid">
						<a href="filter_location?index=category&cat_id='.$val->category_id.'">
							<div class="focus-border">
								<div class="focus-layout">
									<div class="focus-image"><i class="'.$obj->get_icon_data($val->category_id).'"></i></div>
									<h4 class="clrchg">'.$obj->getCategoryName($val->category_id).'</h4>
								</div>
							</div>
						</a>
					</div>';
               
               $a++;
               
               //$cat_id = $val->category_id;
               
               if($a == 6){
                   break;
               }
               
               
               
       }
       
       foreach($popularcategories as $val)
       {
           
            
            if($a >= 6)
            {
               $pp2.=  '<div  class="col-md-2 focus-grid">
						<a href="filter_location?index=category&cat_id='.$val->category_id.'">
							<div class="focus-border">
								<div class="focus-layout">
									<div class="focus-image"><i class="'.$obj->get_icon_data($val->category_id).'"></i></div>
									<h4 class="clrchg">'.$obj->getCategoryName($val->category_id).'</h4>
								</div>
							</div>
						</a>
					</div>';
            }
               
             $a++;  
             
             if($a == 11)
             {
                 break;
             }
               
       }
       
       $all_data = [$pp1,$pp2];
       
       return json_encode($all_data, $a);
       
       
       
   }
   
   public function getPopularPlaces()
   {
        $a=0;$text='';
        
        header('Access-Control-Allow-Origin: *');
        
        $obj = new HomeController2();
        
        $popular_places = $obj->getMostPopularPlaces();
        
        $popular_categories = $obj->getMostPopularCategories();
                    
        foreach($popular_places[0] as $val){
                    
                         $a++; 
                    
                        if($a <= 10){
                        
                           $text .= '<li>
                                        <a class="thumb" href="'.$this->url.'city_pic/'.$obj->getPlacePic($val->region_id).'"></a>
                                    </li>';
                        
                        }
                    
                        
                    
        }
                    
        if($a <= 10)
        {
                    
            for($i=0;$i<sizeof($popular_categories[1]);$i++)
            {
                    
                        if($a <= 10)
                        {
                        
                            $text.='<li>
                                <a class="thumb" href="'.$this->url.'city_pic/'.$obj->getPlacePic($popular_categories[1][$i]->id).'"></a>
                            </li>';
                        
                        }
                        
                       $a++; 
                    
            } 
                    
        }
        
        return json_encode($text);
   }
   
   public function ApiHeader()
   {
       header('Access-Control-Allow-Origin: *');
     //  return view('header.header');
   }
   
   public function getBillboard()
   {
        $a=0; $text='';$text2='';
                header('Access-Control-Allow-Origin: *');
                
                $billboard = billboard::all();
        
                foreach($billboard as $val){
                
                
                
                  $text .='<a href="'.$val->link.'" >
                      <li><img src="'.$this->url.'billboard/'.$val->picture.'" alt="" title="" id="wows1_'.$a.'"/></li>               
                </a>';
		
                   $text2 .='<a href="'.$val->link.'" title="">
                        <span>
                        <img src="'.$this->url.'billboard/'.$val->picture.'" alt=""/>
                        '.$a.'
                     </span>
                 </a>';

                 $a++;
                
                }
                
                $a=0; 
               
                
                return json_encode([$text,$text2]);
		
   }
   
   public function SearchResult()
   {
     $obj = new HomeController2();
     
     
     
     return redirect('searchresults?search_value='.Request::get('search_value').'&api=1');
     
     $x='';
     

    
   }
   
   public function login_check()
   {
       header('Access-Control-Allow-Origin: *');
       
       $obj = users::find(Request::get('id'));
       
       if($obj)
       {
           return 1;
       }
       else{
           return 2;
       }
   }
    
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

