<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


//use Illuminate\Http\Request;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;
use Request;
use DB;
use Session;

class PaymentController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        
        
    
        $data = (array) DB::select('select * from plans where plans="'.Request::get('category').'"')[0];
      
        
        $type= Request::get('type');
        
        Session::put('plan',Request::get('category'));
        
        Session::put('type',Request::get('type'));
        
        Session::put('amount',Request::get('amount')/100);
       
        $amt = Request::get('amount');
        
   
        $a =  array_keys($data,$amt/100);
        
       // dd($a);
        
        foreach($a as $val)
        {
            if($val == $type)
            {
                return Paystack::getAuthorizationUrl()->redirectNow();
                break;
            }
        }
       
        
          
       // }
        
      /*
        foreach($data[0] as $data_v)
        {
            echo 0;
            //dd(key($data[0]));
            
           if (key($data_v) == $type) {
               
             // dd($data_v);
               
                if($data_v == $amt/100)
                {
                  return Paystack::getAuthorizationUrl()->redirectNow();
                }
             // echo key($array).'<br />';
            }
           
            
             echo key($data_v).'<br>';
               
               // echo $type;
            //next($data[0]);
        }*/
    
        
        //if(key($data[0]))
        
       
        
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }
    
    public function store()
    {
        $trans_id = Request::get('trxref');
        
        DB::table('paidads')->insert([
              [
                  
               'plan' => Session::get('plan'),   
               'ad_type' => Session::get('type'),
               'amount' => Session::get('amount'),
               'ad_id' => Session::get('ad_id'),
               'transaction_id' => $trans_id,
                  
               'created_at'=> date("Y-m-d H:i:s",time()),
                  
               'updated_at'=> date("Y-m-d H:i:s",time())
               
              ],
              //['email' => 'dayle@example.com', 'votes' => 0]
       ]);
        
        Session::put('ad_id',null);
        
        Session::put('alert','1');
        
        return redirect('dashboard');
      
        
    }
    
  
}