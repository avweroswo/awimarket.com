<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\models\codes;
use App\models\photo;
use App\models\users;
use App\models\tags;
use App\models\contact;
use Request;
use Jusibe;
use Hash;
use Validator;
use Session;
use Auth;
use Illuminate\Validation\Rule;


class ContactController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
   /* Add Contact us Details */
    
    public function addDetails(){
        
        $value = Request::all();
        
        $rules = [
            
            'fullname' => 'required',
            'subject' => 'required',
            'message' => 'required'
         
         
        ];
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            Session::put('error','1');
            return redirect('contactus')->withErrors($validator);
        }
        else{
        
        $obj = new contact();
        
        $obj->fullname = $value['fullname'];
        
        $obj->email = $value['email'];
        
        $obj->subject = $value['subject'];
        
        $obj->message = $value['message'];
        
        $obj->save();
        
        Session::put('success','1');
        
        return redirect('contactus');
        
        
                
        }
    }
    
    public function register()
    {
        $value = Request::all();
        
        if(!isset($value['terms'])){
        
          $value['terms']='';
        }
        
        $rules = [
            'email' => 'required|unique:users,user_email|max:100|email',
            'password' => 'required',
            'phone-1' => 'required|numeric',
            'firstname' => 'required',
            'lastname' => 'required',
            'terms' => 'required|min:1',
            'code' => 'required'
        ];
        
      //  echo $value['password'];
        
        Session::put('email',$value['email']);
        
        Session::put('password',$value['password']);
        
        Session::put('phone-1',$value['phone-1']);
        
        Session::put('firstname', $value['firstname']);
        
        Session::put('lastname',$value['lastname']);
        
        Session::put('terms',$value['terms']);
        
        Session::put('code',$value['code']);
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            return redirect('register2')->withErrors($validator);
        }
        else{
            
            $val = codes::where('phone',$value['phone-1'])->orderby('id','asc')->take(1)->get();
        
            if(sizeof($val) > 0){
             
                $code_obj = codes::find($val[0]['id']);
                
                $code_value = $val[0]['code'];
            
                $code_obj->delete();
            }
            else
            {
              Session::put('error','1');
              return redirect('register2');
            }
            
            if(!($code_value == $value['code']) ){
                Session::put('error2','1');
                return redirect('register2');
            }
        
            $obj = new users();
        
            $obj->user_email = $value['email'];
        
            $obj->password =  Hash::make($value['password']);
        
            $obj->phone = $value['phone-1'];
        
            $obj->firstname = $value['firstname'];
        
            $obj->lastname =  $value['lastname'];
            
            $obj->remember_token =  'Nil';
        
            $obj->save();
            
            Session::put('email',null);
        
            Session::put('password',null);
        
            Session::put('phone-1',null);
        
            Session::put('firstname', null);
        
            Session::put('lastname',null);
        
            Session::put('terms',null);
        
            Session::put('code',null);
            
        }
        
       
        
    }
    
    public function addPhoto(){
        
        $data = Request::file('photo');
        
        echo 3;
        
        $validator = $this->validate($data, [
        'input_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        if($validator->fails()){
            
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                echo $message + "</br>";
            }
           
        }
        else{

          if ($data->hasFile('photo')) {
          $image = $data->file('photo');
          $name = time().'.'.$image->getClientOriginalExtension();
          $destinationPath = public_path('/userprofile_img');
          $image->move($destinationPath, $name);
          $this->save();

       // return back()->with('success','Image Upload successfully');
            }
        }
        
        $obj = new photo();
        $obj->name = time();
        
        
        
        
        
        
    }
    
    public function genCode()
    {
        $value = Request::all();
        
        if(!isset($value['phone']))
        {
            echo "Please enter a value for phone no";
        }
        else
        {
            $code =  rand(123457824, 987694780);
            
            $code = substr($code,0,4);
            
            $payload = [
               'to' => $value['phone'],
               'from' => 'Awi market',
               'message' => $code
               ];
            
             
                $cod_id = codes::where('phone',$value['phone'])->orderby('id','asc')->take(1)->get();
                
                //var_dump($cod_id);
                
                if(sizeof($cod_id) > 0)
                {
                    $cd = codes::find($cod_id[0]['id']);
                    
                    $cd->delete();
                    
                    //return redirect('kk');
                }
                
            
            try{
                Jusibe::sendSMS($payload)->getResponse();
               
                $codes = new codes();
            
                $codes->code =  $code;
                    
                $codes->phone = $value['phone'];
            
                $codes->save();
                
                echo "code Sent";
            }
            catch (\Exception $ex) {
                echo "Error please try again";
            }
            
        }
        
    }
    
   
}
