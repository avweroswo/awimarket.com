<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\models\SubCatValues;
use App\models\codes;
use App\models\SavedList;
use App\models\FollowList;
use App\models\users;
use App\models\categories;
use App\models\ads;
use App\models\rating;
use App\models\adspic;
use Request;
use Jusibe;
use Hash;
use Validator;
use Session;
use Auth;
use ImageOptimizer;
use DB;
use Image;
use App\Http\Controllers\HomeController2;


class AdController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /* Add temporary photo */
    
    public function addTemPhoto(){
         $data = Request::file('photo');
       
        
      //  echo 2;
          
          if (Request::hasFile('photo')) {
              
               $image = Request::file('photo');
               $name = time().'.'.$image->getClientOriginalExtension();
               $destinationPath = public_path('/temp');
               $image->move($destinationPath, $name);        
               ImageOptimizer::optimize($destinationPath.'/'.$name);
          
               echo $name;
         
          
          }
          
         // var_dump($data);
    
    }
    
    /* filter location search results for seller */
    
    public function filter_location_searchresults_seller()
    {
        
         $obj = new HomeController2();
       
        if(Request::get('priority') == ''  ){
            
           $ads = ads::where('user_id',Session::get('user_id'))->orderby('id','desc')->get();
           
           Session::put('search_result',null);
           
        }
        else if(Request::get('priority') == 'PL'  ){
            
             $ads = ads::where('user_id',Session::get('user_id'))->orderBy('price','asc')->get();
           
             Session::put('search_result','Price : Low to High');
            
        }
        else if(Request::get('priority') == 'PH'  ){
            
             $ads = ads::where('user_id',Session::get('user_id'))->orderby('price','desc')->get();
           
             Session::put('search_result','Price : High to Low');
            
        }
        else if(Request::get('priority') == 'PO'  ){
            
             $ads = $obj->getAwoofAds_searchresults_seller();
           
             Session::put('search_result','Very Popular Ads');
            
        }
        else if(Request::get('priority') == 'HR'  ){
            
             $ads = '';
             
             $a=0;
             
             $ads = ads::where('user_id',Session::get('user_id'))->get();
             
             foreach($ads as $kl)
             {
                 $ads1[$a] = $kl->id; 
                         
                 $a++;
                 
             }
                 
            
             $rating_ids = DB::table('rating')
                          ->select(DB::raw('avg(rating_value) as avg_rating,ad_id '))
                          ->whereIn('ad_id',$ads1)
                          ->groupBy('ad_id')
                          ->get()
                          ->sortByDesc('avg_rating');
             
             //dd($rating_ids);
             
             Session::put('rating_ids',$rating_ids);
            
             Session::put('search_result','Highly Rated Ads');
            
        }
        else if(Request::get('priority') == 'MR'  ){
            
             $ads = $obj->getEveriEveriAds_searchresults_seller();
           
             Session::put('search_result','Most Recent Ads');
            
        }
        
        
        
        if(Request::get('subcat') !== null)
        {
            $ads = ads::where('subcategory','=',Request::get('subcat'))->where('title','like','%'.Session::get('search_value').'%')->get();
           
            Session::put('search_result','Sub Category = '.$obj->getSubCategoryName(Request::get('subcat')));
            
        }
        
       // dd(Request::get('location'));
        
        if(Request::get('location') !== null  ){
            
           $ads = ads::where('region',Request::get('location'))->where('user_id',Session::get('user_id'))->get();
           
           Session::put('search_result','Location = '.$obj->getCityName(Request::get('location')));
           
        }
        
        
        Session::put('search','1');
       
        
        Session::put('all_ads_search_seller1',$ads);
        
        return redirect('sellerprofile?id='.Session::get('user_id'));
        
        
    }
    
    /* Save an Ad */
    
    public function saveAd()
    {
        $obj = new SavedList();
        
        $obj->saved_id = Request::get('ad_id');
        
        $obj->user_id  = Auth::id();
        
        $obj->save();
        
        
    }
    
    /* remove ad from  follow list */
    
    public function UnfollowAd()
    {
        $id = FollowList::where('followed_id',Request::get('ad_id'))->get()[0]->id;
        
        $obj = FollowList::find($id);
        
        $obj->delete();
        
    }
    
    
    /* Add Ad to follow list */
    
    public function followAd()
    {
        $obj = new FollowList();
        
        $obj->followed_id = Request::get('ad_id');
        
        $obj->user_id  = Auth::id();
        
        $obj->save();
        
    }
    
    /* UnSave an Ad */
    
    public function UnsaveAd()
    {
        $id = SavedList::where('user_id',Auth::id())->get()[0]->id;
        
        $obj = SavedList::find($id);
        
        $obj->delete();
    
    }
    
    /* Add Rating to Ad */
    
    public function addRating(){
        
        $obj = new rating();
        
        $obj->ad_id = Request::get('ad_id');
        
        $obj->rating_value = Request::get('value');
        
        $obj->save();
        
    }
    
    /* Remove rating from Ad */
    
    public function removeRating(){
        
        $id = rating::where('ad_id','=',Request::get('ad_id'))->get()[0]->id;
        
        $obj = rating::find($id);
        
        $obj->delete();
        
    }
    
    /* Delete an Ad */
    
    public function DeleteAds()
    {
        $obj = ads::find(Request::get('id'));
        
        $obj->delete();
        
        Session::put('del1',1);
        
        if(Session::get('admin') == 1){
        
            return redirect('admindashboard');
           
        }
    else{
        
        return redirect('dashboard');
           
    }
        
        
    }
    
    /* Delete Saved Ads  */
    
    public function DeleteSavedAds()
    {
        
        $obj = SavedList::find(Request::get('id'));
        
        $obj->delete();
        
        Session::put('del1',1);
        
        return redirect('dashboard');
        
    }
    
    /* Delete  Ad from  follow list */
    
    public function DeleteFollowedAds()
    {
        $obj = FollowList::find(Request::get('id'));
        
        $obj->delete();
        
        Session::put('del1',1);
        
        return redirect('dashboard');
        
    }
    
    /* Add Water Mark to Ad */
    
    public function addWaterMark($path)
    {
        $img = Image::make($path);
        $watermark = Image::make(public_path('/images/user.jpg'));                
        $img->insert($watermark, 'bottom-right', 10, 10);
        $img->save($path);
    }
    
    
    /* Add ads Pic */
    
    public function add_pic($pic_name,$ad_id,$index_value)
    {
        
        
        $extension = explode('.',$pic_name);
        
        $name = time();
       
        ImageOptimizer::optimize(public_path('/temp/').$pic_name);
        
        $this->addWaterMark(public_path('/temp/').$pic_name);
        
        if(copy(public_path('/temp/').$pic_name, public_path('ad_photo/').$name.'.'.$extension[1]))
        {
            
        copy(public_path('/temp/').$pic_name, public_path('ad_photo/').$name.'.'.$extension[1]);
        
        $obj = new adspic();
        
        $obj->pic_name = $name.'.'.$extension[1];
        
        $obj->ad_id = $ad_id;
        
        $obj->index_value = $index_value;
        
        $obj->save();
        
        }
        else
        {
            if(adspic::find(adspic::where('ad_id',$ad_id)->where('index_value',$index_value)->get()[0]->id)!=null)
            {
                $obj = adspic::find(adspic::where('ad_id',$ad_id)->where('index_value',$index_value)->get()[0]->id);
            
              // $obj = new adspic();
        
                $obj->pic_name = $name.'.'.$extension[1];
        
                $obj->ad_id = $ad_id;
        
                $obj->index_value = $index_value;
        
                $obj->save();
            }
            
            
        }
        
       
    }
    
    /* Filter add list by location */
    
    public function filter_location()
    {
        
        $obj = new HomeController2();
         
        if(Request::get('location') !== '' ){
          
            $ads = ads::where('region',Request::get('location'))->get();
           
            Session::put('search_result','Location = '.$obj->getCityName(Request::get('location')));
        }
        
        if(Request::get('index') == 'awoof'  ){
            
           $ads = $obj->getAwoofAds();
           
           Session::put('search_result',null);
           
        }
        
        if(Request::get('index') == 'everieveri'  ){
            
           $ads = $obj->getEveriEveriAds();
           
           Session::put('search_result',null);
           
        }
        
        if(Request::get('priority') == ''  ){
            
           $ads = ads::orderby('id','desc')->get();
           
           Session::put('search_result',null);
           
        }
        else if(Request::get('priority') == 'PL'  ){
            
             $ads = ads::orderby('price','asc')->get();
           
             Session::put('search_result','Price : Low to High');
            
        }
        else if(Request::get('priority') == 'PH'  ){
            
             $ads = ads::orderby('price','desc')->get();
           
             Session::put('search_result','Price : High to Low');
            
        }
        else if(Request::get('priority') == 'PO'  ){
            
             $ads = $this->getAwoofAds();
           
             Session::put('search_result','Very Popular Ads');
            
        }
        else if(Request::get('priority') == 'HR'  ){
            
            $ads = '';
             
             $a=0;
             
             $ads = ads::where('title','like','%'.Session::get('search_value').'%')->get();
             
             foreach($ads as $kl)
             {
                 $ads1[$a] = $kl->id; 
                         
                 $a++;
                 
             }
                 
            
             $rating_ids = DB::table('rating')
                          ->select(DB::raw('avg(rating_value) as avg_rating,ad_id '))
                          ->whereIn('ad_id',$ads1)
                          ->groupBy('ad_id')
                          ->get()
                          ->sortByDesc('avg_rating');
             
             //dd($rating_ids);
             
             Session::put('rating_ids',$rating_ids);
            
             Session::put('search_result','Highly Rated Ads');
            
        }
        else if(Request::get('priority') == 'MR'  ){
            
             $ads = $obj->getEveriEveriAds();
           
             Session::put('search_result','Most Recent Ads');
            
        }
        
        if(Request::get('index') == 'category')
        {
            $ads = ads::where('category','=',Request::get('cat_id'))->get();
            
            Session::put('categories',categories::where('category_id',Request::get('cat_id'))->get());
           
            Session::put('search_result','Category = '.$obj->getCategoryName(Request::get('cat_id')));
            
        }
        
        if(Request::get('subcat') !== null)
        {
            $ads = ads::where('subcategory','=',Request::get('subcat'))->get();
           
            Session::put('search_result','Sub Category = '.$obj->getSubCategoryName(Request::get('subcat')));
            
        }
        
        
        
        
        Session::put('search','1');
       
        
        Session::put('all_ads1',$ads);
        
        return redirect('category');
      
    }
    
    /* filter searched ads location */
    
     public function filter_location_searchresults()
    {
        
        $obj = new HomeController2();
        
       
       
        if(Request::get('priority') == ''  ){
            
           $ads = ads::where('title','like','%'.Session::get('search_value').'%')->orderby('id','desc')->get();
           
           Session::put('search_result',null);
           
        }
        else if(Request::get('priority') == 'PL'  ){
            
             $ads = ads::where('title','like','%'.Session::get('search_value').'%')->orderBy('price','asc')->get();
           
             Session::put('search_result','Price : Low to High');
            
        }
        else if(Request::get('priority') == 'PH'  ){
            
             $ads = ads::where('title','like','%'.Session::get('search_value').'%')->orderby('price','desc')->get();
           
             Session::put('search_result','Price : High to Low');
            
        }
        else if(Request::get('priority') == 'PO'  ){
            
             $ads = $obj->getAwoofAds_searchresults();
           
             Session::put('search_result','Very Popular Ads');
            
        }
        else if(Request::get('priority') == 'HR'  ){
            
             $ads = '';
             
             $a=0;
             
             $ads = ads::where('title','like','%'.Session::get('search_value').'%')->get();
             
             foreach($ads as $kl)
             {
                 $ads1[$a] = $kl->id; 
                         
                 $a++;
                 
             }
                 
            
             $rating_ids = DB::table('rating')
                          ->select(DB::raw('avg(rating_value) as avg_rating,ad_id '))
                          ->whereIn('ad_id',$ads1)
                          ->groupBy('ad_id')
                          ->get()
                          ->sortByDesc('avg_rating');
             
             //dd($rating_ids);
             
             Session::put('rating_ids',$rating_ids);
            
             Session::put('search_result','Highly Rated Ads');
            
        }
        else if(Request::get('priority') == 'MR'  ){
            
             $ads = $obj->getEveriEveriAds_searchresults();
           
             Session::put('search_result','Most Recent Ads');
            
        }
        
        
        
        if(Request::get('subcat') !== null)
        {
            $ads = ads::where('subcategory','=',Request::get('subcat'))->where('title','like','%'.Session::get('search_value').'%')->get();
           
            Session::put('search_result','Sub Category = '.$obj->getSubCategoryName(Request::get('subcat')));
            
        }
        
       // dd(Request::get('location'));
        
        if(Request::get('location') !== null  ){
            
           $ads = ads::where('region',Request::get('location'))->where('title','like','%'.Session::get('search_value').'%')->get();
           
           Session::put('search_result','Location = '.$obj->getCityName(Request::get('location')));
           
        }
        
         if(Request::get('index') == 'category')
        {
            $ads = ads::where('category','=',$obj->getCategoryID(Session::get('cat_id')))->get();
           
            Session::put('search_result','Category = '.Session::get('cat_id'));
            
        }
        
        Session::put('search','1');
       
        
        Session::put('all_ads_search1',$ads);
        
        return redirect('searchresults');
      
    }
    
    public function filter_location_searchresults_serv()
    {
        
        $obj = new HomeController2();
       
        if(Request::get('priority') == ''  ){
            
           $ads = ads::whereIn('user_id',Session::get('serv_tag'))->orderby('id','desc')->get();
           
           Session::put('search_result',null);
           
        }
        else if(Request::get('priority') == 'PL'  ){
            
             $ads = ads::whereIn('user_id',Session::get('serv_tag'))->orderBy('price','asc')->get();
           
             Session::put('search_result','Price : Low to High');
            
        }
        else if(Request::get('priority') == 'PH'  ){
            
             $ads = ads::whereIn('user_id',Session::get('serv_tag'))->orderby('price','desc')->get();
           
             Session::put('search_result','Price : High to Low');
            
        }
        else if(Request::get('priority') == 'PO'  ){
            
             $ads = $obj->getAwoofAds_searchresults();
           
             Session::put('search_result','Very Popular Ads');
            
        }
        else if(Request::get('priority') == 'HR'  ){
            
            $rating_ids ='';
            
             $ads = '';
             
             $ads1 = [];
             
             $a=0;
             
             $ads = ads::whereIn('user_id',Session::get('serv_tag'))->get();
             
             foreach($ads as $kl)
             {
                 $ads1[$a] = $kl->id; 
                         
                 $a++;
                 
             }
             
             
             
             
             if(sizeof($ads1) > 0)
             {
                 
                 $rating_ids = DB::table('rating')
                          ->select(DB::raw('avg(rating_value) as avg_rating,ad_id '))
                          ->whereIn('ad_id',$ads1)
                          ->groupBy('ad_id')
                          ->get()
                          ->sortByDesc('avg_rating');
             }
                 
            
             
             
             //dd($rating_ids);
             
             Session::put('rating_ids',$rating_ids);
            
             Session::put('search_result','Highly Rated Ads');
            
        }
        else if(Request::get('priority') == 'MR'  ){
            
             $ads = $obj->getEveriEveriAds_searchresults_serv();
           
             Session::put('search_result','Most Recent Ads');
            
        }
        
        
        
        if(Request::get('subcat') !== null)
        {
            $ads = ads::where('subcategory','=',Request::get('subcat'))->whereIn('user_id',Session::get('serv_tag'))->get();
           
            Session::put('search_result','Sub Category = '.$obj->getSubCategoryName(Request::get('subcat')));
            
        }
        
       // dd(Request::get('location'));
        
        if(Request::get('location') !== null  ){
            
           $ads = ads::where('region',Request::get('location'))->whereIn('user_id',Session::get('serv_tag'))->get();
           
           Session::put('search_result','Location = '.$obj->getCityName(Request::get('location')));
           
        }
        
        
        Session::put('search','1');
       
        
        Session::put('all_ads_search1',$ads);
        
        return redirect('searchresults');
      
    }
    
    
    public function createAd()
    {
        $value = Request::all();
        
        Session::put('edit_a',null);
        
        //dd($value);
        
       // $value['title'] ='ggg';
        
        if(!isset($value['negotiable'])){
        
          $value['negotiable'] = 0;
        }
        
         if(!isset($value['subcategory'])){
        
          $value['subcategory'] = '';
        }
        
        if(!isset($value['region'])){
        
          $value['region'] = '';
        }
        
        $value['picture'] = $value['pic1'];
        
        $rules = [
            'title' => 'required|max:100',
            'category' => 'required',
            'picture' => 'required',
            //'subcategory' => 'required|numeric',
            'price' => 'required|numeric',
            
        ];
        
        /*
        
        Session::put('email',$value['email']);
        
        Session::put('password',$value['password']);
        
        Session::put('phone-1',$value['phone-1']);
        
        Session::put('firstname', $value['firstname']);
        
        Session::put('lastname',$value['lastname']);
        
        Session::put('terms',$value['terms']);
        
        Session::put('code',$value['code']);
         * 
         * 
         */
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            
            Session::put('er1','1');
            
            return redirect('createAd')->withErrors($validator);
        }
        else{
            
             Session::put('er1','2');
            
            $obj = new ads();
        
            $obj->title = $value['title'];
        
            $obj->category =  $value['category'];
            
            if($value['subcategory'] != ''){
        
            $obj->subcategory = $value['subcategory'];
            
            }
            else{
                $obj->subcategory = 0;
            }
        
            $obj->price = $value['price'];
        
            $obj->negotiable =  $value['negotiable'];
            
            $obj->details =  $value['details'];
            
            $obj->region =  $value['region'];
            
            $obj->phone =  $value['phone'];
             
            $obj->user_id =  Auth::id();
            
            $obj->save();
            
            $ad_id = ads::orderby('id','desc')->get()[0]->id;
            
            for($i=1;$i<=5;$i++){
                
               if(isset($value['pic'.$i])){
               
                   $this->add_pic($value['pic'.$i],$ad_id,$i);
               
               }
               
            }
            
            for($i=1;$i<=Request::get('sub_max');$i++)
            {
                if(isset($value['sub_'.$i])){
                    
                    if(Request::get('sub_'.$i)!='')
                    {            
                       $this->addSubCatvalues($i,Request::get('sub_'.$i),$ad_id);
                    }
                
                }
                
            }
            
            echo 1;
            
            $url = 'https://www.awimarket.com/adDetails?id='.$id; 
            
            
            
            if(isset($value['share'])){
                if($value['share'] == 'fb'){
                     $id = ads::orderby('id','desc')->get()[0]->id;
                     return redirect('https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fawimarket.com/adDetails?id='.$id.'%2F&amp;src=sdkpreparse');
                }
                elseif($value['share'] == 'tw')
                {
                     $id = ads::orderby('id','desc')->get()[0]->id;
                     return redirect('https://twitter.com/intent/tweet?text='.$url);
                }
                elseif($value['share'] == 'go')
                {
                    $id = ads::orderby('id','desc')->get()[0]->id;
                    return redirect('https://plus.google.com/share?url='.$url);
                }
            }
            
            
     
        }
        
       
        
    }
    
    public function editAd()
    {
        $value = Request::all();
        
        Session::put('edit_a',1);
        
        if(!isset($value['negotiable'])){
        
          $value['negotiable'] = 0;
        }
        
         if(!isset($value['subcategory'])){
        
          $value['subcategory'] = '';
        }
        
        if(!isset($value['region'])){
        
          $value['region'] = '';
        }
        
        $rules = [
            'title' => 'required|max:100',
            'category' => 'required',
            'region' => 'required',
            'price' => 'required',
            
        ];
        
        /*
        
        Session::put('email',$value['email']);
        
        Session::put('password',$value['password']);
        
        Session::put('phone-1',$value['phone-1']);
        
        Session::put('firstname', $value['firstname']);
        
        Session::put('lastname',$value['lastname']);
        
        Session::put('terms',$value['terms']);
        
        Session::put('code',$value['code']);
         * 
         * 
         */
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            
            $error = $validator->errors()->first();
            
            echo $error;
            //return redirect('register')->withErrors($validator);
        }
        else{
            
            $obj = ads::find(Request::get('ad_id'));
        
            $obj->title = $value['title'];
        
            $obj->category =  $value['category'];
        
            $obj->subcategory = $value['subcategory'];
        
            $obj->price = $value['price'];
        
            $obj->negotiable =  $value['negotiable'];
            
            $obj->details =  $value['details'];
            
            $obj->region =  $value['region'];
            
            $obj->phone =  $value['phone'];
             
            $obj->user_id =  Auth::id();
            
            $obj->save();
            
            $ad_id = Request::get('ad_id');
            
            for($i=1;$i<=5;$i++){
                
               if(isset($value['pic'.$i])){
               
                   $this->add_pic($value['pic'.$i],$ad_id,$i);
               
               }
               
            }
            
             DB::table('subcat_values')->where('ad_id', '=', $ad_id)->delete();
            
            
            if(Request::get('sub_max')!=''){
                
                for($i=1;$i<=Request::get('sub_max');$i++)
                {
                    //echo $value['sub_'.$i];
                   // break;
                    if($value['sub_'.$i]!=''){
            
                     $this->addSubCatvalues($i,Request::get('sub_'.$i),$ad_id);
                
                    }
                }
              
            }
            
            echo 1;
            
            /*
            
            Session::put('email',null);
        
            Session::put('password',null);
        
            Session::put('phone-1',null);
        
            Session::put('firstname', null);
        
            Session::put('lastname',null);
        
            Session::put('terms',null);
        
            Session::put('code',null);
            
             * */
             
        }
        
       
        
    }
    
    public function addSubCatvalues($id,$value,$ad_id){
       $obj = new  SubCatValues();
       $obj->sub_cat_id = $id;
       $obj->value = $value;
       $obj->ad_id = $ad_id;
       $obj->save();
       
    }
    
    public function genCode()
    {
        $value = Request::all();
        
        if(!isset($value['phone']))
        {
            echo "Please enter a value for phone no";
        }
        else
        {
            $code =  rand(123457824, 987694780);
            
            $code = substr($code,0,4);
            
            $payload = [
               'to' => $value['phone'],
               'from' => 'Awi market',
               'message' => $code
               ];
            
             
                $cod_id = codes::where('phone',$value['phone'])->orderby('id','asc')->take(1)->get();
                
                //var_dump($cod_id);
                
                if(sizeof($cod_id) > 0)
                {
                    $cd = codes::find($cod_id[0]['id']);
                    
                    $cd->delete();
                    
                    //return redirect('kk');
                }
                
            
            try{
                Jusibe::sendSMS($payload)->getResponse();
               
                $codes = new codes();
            
                $codes->code =  $code;
                    
                $codes->phone = $value['phone'];
            
                $codes->save();
                
                echo "code Sent";
            }
            catch (\Exception $ex) {
                echo "Error please try again";
            }
            
        }
        
    }
    
   
}
