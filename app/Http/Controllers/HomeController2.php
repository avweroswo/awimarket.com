<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Jusibe;
use Auth;
use Session;
use Socialite;
use App\models\billboard;
use App\models\rating;
use App\models\category;
use App\models\FollowList;
use App\models\SavedList;
use App\models\categories;
use App\models\subcategory2;
use App\models\cities;
use App\models\ads;
use App\models\adspic;
use App\models\users;
use App\models\reportedSeller;
use App\models\navigation;
use DB;
use Image;
use Request;
use Carbon\Carbon;
use App\models\SubCatValues;
use App\models\MaxViews;
use App\models\popularcategories;
use App\models\failedsearch;
use App\models\allsearch;
use App\models\makeawish;
use Validator;

class HomeController2 extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    private $url='http://localhost:8000/';
    
    public function getCategoryID($cat_name)
    {
        $id = category::where('cat_name',$cat_name)->get()[0]->id;
        
        return $id;
    }
    
    public function addRandCode($value)
    {
        Session::put('rand_code',$value);
        
        return 1;
    }
    
    public function createCaptcha()
    {
        $num1=rand(1,9); //Generate First number between 1 and 9  
        $num2=rand(1,9); //Generate Second number between 1 and 9  
        $captcha_total=$num1+$num2;  

        $math = "$num1"." + "."$num2"." =";  
        
        if($this->addRandCode($captcha_total) == 1)
        {
              $font = public_path().'/font/arial_narrow_7.ttf';
              
              $image = imagecreatetruecolor(120, 30); //Change the numbers to adjust the size of the image
              $black = imagecolorallocate($image, 0, 0, 0);
              $color = imagecolorallocate($image, 0, 100, 90);
              $white = imagecolorallocate($image, 255, 255, 255);

              imagefilledrectangle($image,0,0,399,99,$black);
              imagettftext ($image, 20, 0, 20, 25, $white, $font, $math );//Change the numbers to adjust the font-size

              header("Content-type: image/png");
              //imagepng($image);
              imagepng($image, public_path()."/font/".'captcha.png');
             
       
        }


    }
    
    public function getAdtype($id)
    {
        if($this->checkAd($id) == 1){
       
           $ads = DB::table('paidads')->where('ad_id',$id)->where('plan','featured')->orderby('id','desc')->get()[0]->plan;
           
           return $ads;
           
        }
        else{
            return 'Free';
        }
        
        
    }
    
    public function mobileview()
    {
        return view('mobileview');
    }
    
    public function payment()
    {
        return view('payment');
    }
    
    public function makeawish_cron()
    {
        $data = makeawish::where('status','0')->get();
        
        foreach($data as $val)
        {
            
           $value = $this->getData_makeawish($val->searchword,$val->category,$val->subcategory,$val->region);
           
           //dd($value);
           
           if($value == 1 )
           {
              // alert('hi');
               
               $this->sendMakeAWishMessage($this->phone);
               
               $this->sendMakeAWishEmail($this->email);
               
               $this->updateMakeAWishStatus($val->id); 
           }
            
        }
        
    }
    
    public function sendMakeAWishMessage($phone)
    {
         try{
             
             $msg ='Hi';
             
             $payload = [
               'to' => $phone,
               'from' => 'Awi market',
               'message' => $msg
               ];
             
                Jusibe::sendSMS($payload)->getResponse();
               
                echo "Message Sent";
            }
            catch (\Exception $ex) {
                echo "Error please try again";
            }
        
    }
    
     public function sendMakeAWishEmail($email)
     {
        
        
     }
    
    public function updateMakeAWishStatus($id)
    {
        $obj = makeawish::find($id);
        
        $obj->status = 1;
        
        $obj->save();
        
    }
    
  
    
    public function getData_makeawish($searchword,$category,$subcategory,$region)
    {
        $data = ads::where('title','Like','%'.$searchword.'%')
                    ->where('category',$category)
                    ->where('subcategory',$subcategory)
                    ->where('region',$region)
                    ->get();
        
        if(sizeof($data) > 0)
        {
            return 1;
        }
        
        
    }
    
    public function makeawish()
    {
        
       // dd(Session::get('category_error'));
        
        $validator = Validator::make(Request::all(), [
            
         'category' => 'required',
         'subcategory' => 'required',
         'searchword' => 'required',
         'email' => 'required',
         'phone' => 'required',
         'region' => 'required',

        ]);
        
        if($validator->fails()){
            
           Session::put('category_error1',1); 
           
           //dd(Session::get('category_error'));
            
           return redirect('category')->withErrors($validator);
           
        }
        else{
            
               Session::put('category_error1',2); 
            
               $obj = new makeawish();
        
               $obj->category = Request::get('category');
               
               $obj->subcategory  = Request::get('subcategory');
               
               $obj->searchword  = Request::get('searchword');
               
               $obj->email  = Request::get('email');
               
               $obj->phone =   Request::get('phone');
               
               $obj->region = Request::get('region');
               
               $obj->save();
               
               return redirect('category');
            
        }
     
                
        }

        
    public function getMostPopularCategories()
    {
        $obj = MaxViews::find(1);
        
        $sum_views = ads::all()->sum('views');
        
       
        
        if($sum_views >= $obj->value)
        {
            
            $category =  DB::table('ads')
                          ->select(DB::raw('sum(views) as Total_views,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_views');
            
            popularcategories::truncate();
            
            foreach($category as $val)
            {
            
                   $this->addPopularCategories($val->category_id);
                   
            }
            
            if(sizeof($category)<12)
            {
                $all_cat = DB::select('SELECT * FROM category  WHERE id NOT IN (SELECT category FROM ads)');
                
                
                
                for($i=0;$i<sizeof($all_cat);$i++)
                {
            
                   $this->addPopularCategories($all_cat[$i]->id);
                   
                }
            
              
                return [$category,$all_cat];
            }
            
            return $category;
             
        }
        else
        {
            
            $category =  DB::table('ads')
                          ->select(DB::raw('count(category) as Total_category,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_category');
            
            popularcategories::truncate();
            
            foreach($category as $val)
            {
            
                   $this->addPopularCategories($val->category_id);
                   
            }
            
            if(sizeof($category)<12)
            {
            
                
                $all_cat = DB::select('SELECT * FROM category  WHERE id NOT IN (SELECT category FROM ads)');
                
                //popularcategories::truncate();
                
                for($i=0;$i<sizeof($all_cat);$i++){
                {
            
                   $this->addPopularCategories($all_cat[$i]->id);
                   
                }
                
                
              
                return [$category,$all_cat];
            }
            
          
           return $category;
           
        }
       
    }
    }
    
    
    
    public function getPrice($id)
    {
        $obj = ads::find($id);
        
        return $obj->price;
    }
    
    public function getTitle($id)
    {
        $obj = ads::find($id);
        
        return $obj->title;
    }
    
    public function getRegion($id)
    {
        $obj = ads::find($id);
        
        if(cities::find($obj->region)){
            
            return cities::find($obj->region)->city;
        }
        
     
    }
    
    public function getDetails($id)
    {
        $obj = ads::find($id);
        
        return $obj->details;
        
    }
    
    public function getEveriEveriAds_searchresults_seller()
    {
        
            
            $ads =  DB::table('ads')
                        ->where('user_id',Session::get('user_id'))
                        ->orderby('created_at','desc')
                        ->get();
               
          
            return $ads;
         
    }
    
    public function watermark_image()
    {
      
                
                //Upload Images One After the Order into folder
                $img = Image::make(public_path('/images/awoof2.jpg'));
                $watermark = Image::make(public_path('/images/user.jpg'));                
                $img->insert($watermark, 'bottom-right', 10, 10);
                $img->save(public_path().'/images/awoof2.jpg');
    }
    
    
    public function getEveriEveriAds_searchresults()
    {
        
            
            $ads =  DB::table('ads')
                        ->where('title','like','%'.Session::get('search_value').'%')
                        ->orderby('created_at','desc')
                        ->get();
               
          
            return $ads;
         
    }
    
     public function getEveriEveriAds()
    {
        
            
            $ads =  DB::table('ads')
                       // ->where('title','like','%'.Session::get('search_value').'%')
                        ->orderby('created_at','desc')
                        ->get();
               
          
            return $ads;
         
    }
    
     public function getEveriEveriAds_searchresults_serv()
    {
        
            
            $ads =  DB::table('ads')
                        ->whereIn('user_id',Session::get('serv_tag'))
                        ->orderby('created_at','desc')
                        ->get();
               
          
            return $ads;
         
    }
    
     public function getAwoofAds()
    {
        $obj = MaxViews::find(1);
        
        $sum_views = ads::all()->sum('views');
        
       
        
        if($sum_views >= $obj->value)
        {
            
            $ads =  DB::table('ads')
                          ->select(DB::raw('sum(views) as Total_views,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_views');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                       // ->where('title','like','%'.Session::get('search_value').'%')
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
             
        }
        else
        {
            
            $ads =  DB::table('ads')                         
                          ->select(DB::raw(' count(category) as Total_category,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_category');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                        //->where('title','like','%'.Session::get('search_value').'%')
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
           
        }
       
    }
    public function getAwoofAds_searchresults()
    {
        $obj = MaxViews::find(1);
        
        $sum_views = ads::all()->sum('views');
        
       
        
        if($sum_views >= $obj->value)
        {
            
            $ads =  DB::table('ads')
                          ->select(DB::raw('sum(views) as Total_views,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_views');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                        ->where('title','like','%'.Session::get('search_value').'%')
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
             
        }
        else
        {
            
            $ads =  DB::table('ads')                         
                          ->select(DB::raw(' count(category) as Total_category,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_category');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                        ->where('title','like','%'.Session::get('search_value').'%')
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
           
        }
       
    }
    
     public function getAwoofAds_searchresults_seller()
    {
        $obj = MaxViews::find(1);
        
        $sum_views = ads::all()->sum('views');
        
       
        
        if($sum_views >= $obj->value)
        {
            
            $ads =  DB::table('ads')
                          ->select(DB::raw('sum(views) as Total_views,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_views');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                        ->where('user_id',Session::get('user_id'))
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
             
        }
        else
        {
            
            $ads =  DB::table('ads')                         
                          ->select(DB::raw(' count(category) as Total_category,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_category');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                        ->where('user_id',Session::get('user_id'))
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
           
        }
       
    }
    
      public function getAwoofAds_searchresults_serv()
    {
        $obj = MaxViews::find(1);
        
        $sum_views = ads::all()->sum('views');
        
       
        
        if($sum_views >= $obj->value)
        {
            
            $ads =  DB::table('ads')
                          ->select(DB::raw('sum(views) as Total_views,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_views');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                        ->whereIn('user_id',Session::get('serv_tag'))
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
             
        }
        else
        {
            
            $ads =  DB::table('ads')                         
                          ->select(DB::raw(' count(category) as Total_category,category as category_id '))
                          ->where('category', '<>', 0)
                          ->groupBy('category')
                          ->get()
                          ->sortByDesc('Total_category');
            
            popularcategories::truncate();
            
            foreach($ads as $values)
            {
                $this->addPopularCategories($values->category_id);
            }
            
            $ads =  DB::table('ads')
                        ->where('ads.category', '=', 'popularcategories.category_id')
                        ->whereIn('user_id',Session::get('serv_tag'))
                        ->orderby('price','asc')
                        ->get();
               
          
            return $ads;
           
        }
       
    }
    
    public function addPopularCategories($cat_id)
    {
        $obj = new popularcategories();
        
        $obj->category_id = $cat_id;
        
        $obj->save();
    }
    
    public function getPercentageRating($ad_id)
    {
        $rating_percent = (rating::where('ad_id','=',$ad_id)->avg('rating_value')/5) * 100;
        
        return $rating_percent;
        
    }
    
     public function getMostPopularPlaces()
    {
        $obj = MaxViews::find(1);
        
        $sum_views = ads::all()->sum('views');
        
       
        
        if($sum_views >= $obj->value)
        {
            
            $region =  DB::table('ads')
                          ->select(DB::raw('sum(views) as Total_views,region as region_id '))
                          ->where('region', '<>', 0)
                          ->groupBy('region')
                          ->get()
                          ->sortByDesc('Total_views');
            
            if(sizeof($region)<12)
            {
                $all_region = DB::select('SELECT * FROM cities  WHERE id NOT IN (SELECT region FROM ads) ');
              
                return [$region,$all_region];
            }
            
            return $region;
             
        }
        else
        {
            
            $region =  DB::table('ads')
                          ->select(DB::raw('count(category) as Total_category,region as region_id '))
                          ->where('region', '<>', 0)
                          ->groupBy('region')
                          ->get()
                          ->sortByDesc('Total_category');
            
            if(sizeof($region)<12)
            {
            
                
                $all_region = DB::select('SELECT * FROM cities  WHERE id NOT IN (SELECT region FROM ads)');
              
                return [$region,$all_region];
            }
            
          
           return $region;
           
        }
       
    }
    
    public function get_icon_data($id)
    {
        $obj = category::find($id);
        
        if($obj){
        
        return $obj->icon_data;
        
        }
        
    }
    
    public function getPlacePic($id)
    {
        $obj = cities::find($id);
        
        if($obj)
        {
        
           return $obj->pic_name ;
           
        }
        
    }
    
    public function index()
    {
        Session::put('al',0);
        
        $category_all = category::all();
        
        $popular_categories = $this->getMostPopularCategories();
        
        $popular_places = $this->getMostPopularPlaces();
        
        $billboard = billboard::all();
        
        $navigation_all = navigation::orderby('position','asc')->get();
        
        $ads_tags = ads::all();
        
        $billboard_ads =  DB::table('paidads')->where('plan','billboard')->get();
        
        $ad_id = '';
        
        foreach($billboard_ads as $v)
        {
            $ad_id .= $v->ad_id.',';
        }
        
        $ad_id = substr($ad_id,0,strlen($ad_id)-1);
        
        $ad_id = explode(',',$ad_id);
        
        $billboard_ads = ads::whereIn('id',$ad_id)->get();
             
        $featured_ads = $this->getFeatured_ads();
        
        //dd($featured_ads);
        
        //dd($featured_ads);
        
       
    return view('index')->with(['category_all' => $category_all,'featured_ads'=>$featured_ads,'billboard_ads'=>$billboard_ads,'ads_tags'=>$ads_tags,'navigation_all'=>$navigation_all,'billboard'=>$billboard, 'popular_places' => $popular_places,'popular_categories' => $popular_categories,'obj'=>$this,'obj1'=>$this,'p1'=>$popular_categories[0]]);
    }
    
    public function getFeatured_ads()
    {
        
           $featured_ads = DB::table('paidads')->where('plan','featured')->get();
        
           $ad_id = '';
        
           foreach($featured_ads as $v)
           {
             $ad_id .= $v->ad_id.',';
           }
        
           $ad_id = substr($ad_id,0,strlen($ad_id)-1);
        
           $ad_id = explode(',', $ad_id);
           
           $featured = ads::whereIn('id',$ad_id)->get();
           
           $obj1 = $this;
           
           $value='';
           
           
        if(Session::get('Api') !== 1)
        {
           return $featured;
           
        }
        else{
          
          foreach($featured as $val)
          {
            
            if($obj1->checkAd($val->id) == 1){

                 $value.= '<div class="item">
                   <div class="product-item">
                    <div class="carousel-thumb">
                      <a href="#" onclick="adDetailsPage('.$val->id.')" >
                        <img src="'.$this->url.'ad_photo/'.$obj1->getPhoto($val->id).'" height="150" width="200" alt=""></a>

                          <div class="overlay">
                             <a href="adDetails?id='.$val->id.'"><i class="fa fa-link"></i></a>
                          </div>
                    </div>
                    <a href="adDetails?id='.$val->id.'" class="item-name">'.substr($val->title,0,20).'</a>
                    <span class="price">&#8358;'.number_format($val->price).'</span>
                    </div>
                    </div>';
            }               
             
          }
          
          Session::put('Api',null);
          
          return $value;
    }
    }
    
    public function searchresults()
    {
        $searchs = explode('(',Request::get('search_value'));
        
        $search = Request::get('search_value');
        
       // dd($search);
        
        if(isset($searchs[1]))
        {
            $search = explode(' ',$searchs[0])[0];
            
           // $search = $searchs[0];
            
            Session::put('search_value',$searchs[0]);
            
            $search2 = substr($searchs[1],0,strlen($searchs[1])-1);
            
            //dd($f);
        }
        
        
        
        
        
        $category_all = category::all();
        $ads = ads::where('title','like','%'.$search.'%')->get();
        $services = users::where('tags','like','%'.$search.'%')->get();
        $serv = [];
        $ads2 = ads::all();
        
        $a=0;
        
        foreach($services as $ods)
        {
            $serv[$a] = $ods->id;
            $a++;
            
        }
        
        Session::put('serv_tag',$services);
        
        $services = ads::whereIn('user_id',$serv)->get();
        
        $sellers = users::where('company_name','like','%'.$search.'%')->where('user_status','0')->paginate(20);
        
        //dd($sellers);
        
        if(Request::get('search_value')!=null){
       
            Session::put('search_value',$search);
        }
        
        Session::put('all_ads_search',$ads);
       // $sellers = users::paginate(20);
        
         if(Session::get('search') == '1')
         {
        
            Session::put('all_ads_search',Session::get('all_ads_search1'));
            
           
         }
         
         if(sizeof(Session::get('all_ads_search')) > 0 and $search != '')
            {
                $db = new failedsearch();
                
                $db->search_value = $search;
                
                $db->save();
                
                $db = new allsearch();
                
                $db->search_value = $search;
                
                $db->save();
                
            }  
            
            if(isset($searchs[1]))
            {
               //$search = substr($search[0],0,strlen($search[0])-1);
                
               //dd(Session::get('search_value'));
            
               Session::put('cat_id',$search2);
               
              // dd(substr($searchs[1],0,strlen($searchs[1])-1));
            
               return redirect('filter_location_searchresults?index=category');
            
            }
            
        
          
        
        
        
        Session::put('services',$services);
        
        if(Request::get('api') ==1)
        {
            $obj = $this;       $x='';
            
                  foreach(Session::get('all_ads_search') as $val){
                      
               
        
    $x .='<a href="adDetails?id='.$val->id.'">
    
           <div class="item-list">
<div class="col-sm-2 no-padding photobox">
<div class="add-image">
<img src="ad_photo/'.$obj->getPhoto($val->id).'" width="300" height="262" alt="" class="img-box1" >
<span class="photo-count text-color-white color-purple" style="font-size:15px;padding:5px;">&#8358;'.$val->price.'</span>
<span class="photo-count text-color-white color-purple " style="top:40px;font-size:10px;padding:5px;text-transform:uppercase;">U Fit price am</span>
<div class="cat-text2 " style="word-wrap:break-word;max-width:80%;">'.$val->title.'
</div>
</div>
</div>
<div class="col-sm-12 add-desc-box" style="width:83%;">
<div class="add-details" >
<h5 class="add-title"><a href="#">'.$val->title.'</a></h5>
<div class="cat-text" style="display:none;word-wrap:break-word;min-height:93px;">'.$val->details.'
</div>

<div style="width:120%;">
<a class="btn btn-sm click-add " style="display:inline;"><i class="fa fa-certificate"></i>
<span>Ad type</span></a>
<a class="btn btn-common btn-sm click-add" style="display:inline;"> <i class="fa fa-eye"></i> <span>'.$obj->getCityName($val->region).'</span> </a>
</div>
</div>
</div>
   

</div>
        
    </a>';
      
      
    
    
   }
    
     
    return json_encode($x);
        }
        
       
        return view('searchresults')->with(['category_all' => $category_all,'ads'=>$ads2,'cities_all'=>cities::all(), 'obj'=>$this ,'obj1'=>$this, 'navigation_all'=>navigation::all(),'sellers' => $sellers,'ads_tags'=>ads::all()]);
    }
    
    public function pick(){
       
       Socialite::driver('facebook')->user();
       echo "hi";    
    }
    
    public function pick2(){
        Socialite::driver('facebook')->redirect();
       echo "hi";    
    }
    
    public function sellerprofile()
    {
        $id = Request::get('id');
        
        $users = users::find($id);
        
        $obj = $this;
        
        $all_ads = ads::orderby('id','desc')->where('user_id',$id)->get();
        
        $dt = Carbon::now();
        
        $category_all = category::all();
        
        $cities_all = cities::all();
        
        $ads = ads::where('user_id',$users->id)->get();
        
        //dd($ads);
        
        Session::put('user_id',$users->id);
        
        Session::put('all_ads_search_seller',$ads);
        
        
       
        if(Session::get('search') == '1')
        {        
            Session::put('all_ads_search_seller',Session::get('all_ads_search_seller1'));
            
        }
        
         // $navigation_all = navigation::all();
            //$ads_tags = ads::all();
       
        //>$ads_tags,
        return view('viewseller')->with(['users' => $users  ,'cities_all'=>$cities_all ,'ads_tags'=>ads::all(), 'all_ads' => $all_ads, 'obj' => $obj,'obj1'=>$this, 'dt' => $dt,'category_all' => $category_all,'navigation_all' => navigation::all()]);
    }
    
    public function register()
    {
        $category_all = category::all();
        $ads_tags = ads::all();
        return view('register')->with(['category_all' => $category_all,'navigation_all'=>navigation::all(),'ads_tags'=>$ads_tags,'obj'=>$this,'obj1'=>$this,]);
    }
    
    public function reportSeller()
    {
        $validator = Validator::make(Request::all(), [
         'reportreason' => 'required',
         'reportDescription' => 'required',
        
            
        ]);
        
        if($validator->fails()){
            
            Session::put('report','1');
         
            return redirect('adDetails?id='.Request::get('id'))->withErrors($validator);
            
        }
        else{
            
            $obj = new reportedSeller();
            $obj->seller = Request::get('seller');
            $obj->report_reason = Request::get('reportreason');
            $obj->reportDesc = Request::get('reportDescription');
            $obj->save();       
            Session::put('report','2');
            return redirect('adDetails?id='.Request::get('id'));
            
        }
        
        
    }
    
    public function dashboard()
    {
        if(Auth::check()){
            //echo Auth::user()->id;
            $all_ads = ads::orderby('id','desc')->where('user_id',Auth::id())->get();
            Session::put('al',1);
            $follow_list = FollowList::where('user_id',Auth::id())->get();
            $saved_list =  SavedList::where('user_id',Auth::id())->get();
            $category_all = category::all();
            $obj = $this;
            $navigation_all = navigation::all();
            $ads_tags = ads::all();
            if(Request::get('id')=='promoted'){
                $paid_ads = DB::table('paidads')->get();
                $ad_id = '';
        
                foreach($paid_ads as $v)
                {
                   $ad_id .= $v->ad_id.',';
                }
        
                $ad_id = substr($ad_id,0,strlen($ad_id)-1);
        
                $ad_id = explode(',', $ad_id);
                
                $all_ads = ads::orderby('id','desc')->where('user_id',Auth::id())->whereIn('id',$ad_id)->get();
                Session::put('promote-ads','1');
            }
            
            
            //dd($all_ads);
            return view('dashboard')->with(['all_ads' => $all_ads,'navigation_all'=>$navigation_all,'ads_tags'=>$ads_tags,'category_all'=>$category_all, 'obj' => $obj , 'follow_list' => $follow_list , 'saved_list' => $saved_list,'obj1'=>$this,]);
        }
        else{
            return redirect('signup?login=1');
        }
    }
    
    public function drag()
    {
        return view('drag');
    }
    
    public function createAd()
    {
         if(Auth::check()){
             
             $categories = category::all();
             
             $ads = '';
             
             $obj = $this;
             
             $all_cities = cities::all();
             
             if(Request::get('id')!=null){
                 $ads = ads::find(Request::get('id'));
                 
                 
             }
            
             $category_all = category::all();
             
             $navigation_all = navigation::all();
             
             //$navigation_all = navigation::all();
             $ads_tags = ads::all();
             
             
            
            return view('create-ad')->with(['categories' => $categories,'ads_tags'=>$ads_tags, 'navigation_all' => $navigation_all ,'category_all'=>$category_all, 'ads' => $ads , 'obj' => $obj, 'all_cities' => $all_cities,'obj1'=>$this ]);
        }
        else{
             return redirect('/');
            
        }
    }
    
    public function getSubValue($id){
        
        if(isset( SubCatValues::where('sub_cat_id',$id)->get()[0]->value))
        {
        
            return SubCatValues::where('sub_cat_id',$id)->get()[0]->value;
           
        }
        
    }
    
       public function maxSubID($id)
    { 
      
           if(isset(categories::find($id)->subCategory2)){
               
           
            $max_value =  categories::find($id)->subCategory2->max('id');
            
            return $max_value;
            
         
          }
      
    }
    
    public function getSubsAll($id)
    { 
      
           if(isset(categories::find($id)->subCategory2)){
               
           
            $sub =  categories::find($id)->subCategory2;
            
            return $sub;
            
         
          }
      
    }
    
    public function userprofile()
    {
        
        if(Auth::check()){
            $category_all = category::all(); 
            $navigation_all = navigation::all();
            return view('userprofile')->with(['category_all' => $category_all,'obj1'=>$this,'navigation_all' => $navigation_all,'ads_tags'=>ads::all() ]);
        }
        else{
           return redirect('signup?login=1');
        }
        
    }
    
    public function promoteAd()
    {
        
       if(Request::get('id')!=null)
       {
           Session::put('ad_id',Request::get('id'));
       }
        
        if(Auth::check() and Session::get('ad_id')!=null){
           $category_all = category::all();
           $navigation_all = navigation::all();
           $ads_tags = ads::all();
           
           $featured = DB::table('plans')->where('plans','featured')->get()[0];
           
           $useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){

return view('mobileview')->with(['category_all' => $category_all,'featured' => $featured,'navigation_all'=>$navigation_all,'ads_tags'=>$ads_tags,'obj'=>$this,'obj1'=>$this ]);

}
else{

           
           return view('promote-ads')->with(['category_all' => $category_all,'featured' => $featured,'navigation_all'=>$navigation_all,'ads_tags'=>$ads_tags,'obj'=>$this,'obj1'=>$this ]);
    
}       
        }
        else{
            return redirect('dashboard');
        }
           
        }
    
    public function sidebarAd()
    {
     
        
        if(Auth::check() and Session::get('ad_id')!=null){
        $category_all = category::all();
        $featured = DB::table('plans')->where('plans','sidebar')->get()[0];
        
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){

return view('mobileview_sidebar')->with(['category_all' => $category_all,'featured'=>$featured,'navigation_all' => navigation::all() ,'ads_tags'=>ads::all(),'obj'=>$this,'obj1'=>$this ]);

}
else{

           
          return view('sidebar-ads')->with(['category_all' => $category_all,'featured'=>$featured,'navigation_all' => navigation::all() ,'ads_tags'=>ads::all(),'obj'=>$this,'obj1'=>$this ]);
    
}       
        
           }
        else{
            return redirect('dashboard');
        }
    }
    
    public function checkAd($id)
    {
        $ads = DB::table('paidads')->where('ad_id',$id)->orderby('id','desc')->get();
        
        if(sizeof($ads) > 0)
        {
           $ad_type = $ads[0]->ad_type;
           
           if(substr($ad_type,0,1) == 'o')
           {
               if(strtotime(date('Y-m-d H:i:s',strtotime($ads[0]->created_at))) <= time("+30 days"))
               {
                   return 0;
               }
               else{
                  return 1;
               }
           }
           elseif(substr($ad_type,0,1) == 't')
           {
               if(strtotime(date('Y-m-d H:i:s',strtotime($ads[0]->created_at))) <= time("+90 days"))
               {
                   return 0;
               }
               else{
                  return 1;
               }
               
           }
           elseif(substr($ad_type,0,1) == 's')
           {
               if(strtotime(date('Y-m-d H:i:s',strtotime($ads[0]->created_at))) <= time("+180 days"))
               {
                   return 0;
               }
               else{
                  return 1;
               }
           }
          
        }
        
        return 2;
        //return 1;
    }
    
    public function billboardAd()
    {
 
        
        if(Auth::check() and Session::get('ad_id')!=null){
        $category_all = category::all();
        $featured = DB::table('plans')->where('plans','sidebar')->get()[0];
        
         $useragent=$_SERVER['HTTP_USER_AGENT'];
        
                if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){

return view('mobileview_billboard')->with(['category_all' => $category_all,'featured'=>$featured,'navigation_all'=>navigation::all(),'obj'=>$this,'ads_tags'=>ads::all(),'obj1'=>$this ]);

}
else{

           
         return view('billboard-ads')->with(['category_all' => $category_all,'featured'=>$featured,'navigation_all'=>navigation::all(),'obj'=>$this,'ads_tags'=>ads::all(),'obj1'=>$this ]);
    
}    
        
        
           }
        else{
            return redirect('dashboard');
        }
    }
    
    public function adDetails()
    {
        $ads = ads::find(Request::get('id'));
        $ads->views++;
        $ads->dummy_views++;
        $ads->save();
        $ads = ads::find(Request::get('id'));
        $obj = $this;        
        $category_all = category::all();
        $navigation_all = navigation::all();
        $ads_tags = ads::all();
        
        $featured_ads = DB::table('paidads')->where('plan','featured')->get();
        
        $ad_id = '';
        
        foreach($featured_ads as $v)
        {
            $ad_id .= $v->ad_id.',';
        }
        
        $ad_id = substr($ad_id,0,strlen($ad_id)-1);
        
        $ad_id = explode(',', $ad_id);
        
        $featured_ads = ads::whereIn('id',$ad_id)->where('category',$ads->category)->get();
        
        $sidebar_ads = DB::table('paidads')->where('plan','sidebar')->get();
        
        $ad_id = '';
        
        foreach($sidebar_ads as $v)
        {
            $ad_id .= $v->ad_id.',';
        }
        
        $ad_id = substr($ad_id,0,strlen($ad_id)-1);
        
        $ad_id = explode(',', $ad_id);
        
        $sidebar_ads = ads::whereIn('id',$ad_id)->where('category',$ads->category)->inRandomOrder()
                ->first();
        
           if(Session::get('Api') != 1)
           {
              return view('ad-details')->with(['ads'=>$ads,'sidebar_ads'=>$sidebar_ads,'featured_ads'=>$featured_ads,'ads_tags'=>$ads_tags,'navigation_all'=>$navigation_all,'obj'=>$obj,'category_all'=>$category_all,'rating_percentage' => $this->getPercentageRating(Request::get('id')),'obj1'=>$this]);
           }
           else{
               
               Session::put('Api',null);
               //return $this->adDetailsHTML($ads,$this,$this);
               
           }
        }
        
    public function adDetailsHTML($ads,$obj,$obj1)
    {
               $Photo_iteration = '';
               
               $value = '<div id="content" style="margin-top:150px;">
<div class="container">
<div class="row">

<div class="product-info">
<div class="col-sm-8">
<div class="inner-box ads-details-wrapper">
<h2>"'.$ads->title.'"</h2>
<p class="item-intro"><span class="poster"><a href="sellerprofile?id="'.$ads->user_id.'">'.$obj->getSellerName($ads->user_id).'</a> <span class="ui-bubble is-member">post am on </span> <span class="date">'.date('d-m-Y H:i:s',strtotime($ads->created_at)).'</span> E Location <span class="location">'.$obj->getCityName($ads->region).'</span></p>
<div id="owl-demo" class="owl-carousel owl-theme">';
    
foreach($obj->getAllPhoto($ads->id) as $values){

$Photo_iteration.='<div class="item">
<img src="'.$this->url.'ad_photo/'.$values->pic_name.'" style="height:500px!important" alt="">
</div>';

}

$value.= $Photo_iteration;

$value.='</div>
</div>
<div class="box">
<h2 class="title-2"><strong>Wetin The Seller Talk</strong></h2>
<div class="row">
<div class="ads-details-info col-md-8">
<p class="mb15" style="word-wrap:break-word;">'.$ads->details.'</p>
</div>
<div class="col-md-4">
<aside class="panel panel-body panel-details">
<ul>
<li>
<p class=" no-margin "><strong>Price:</strong> &#8358;'.$ads->price.'</p>
</li>
<li>
<p class="no-margin"><strong>Type:</strong> <a href="#">'.$obj->getCategoryName($ads->category).'</a></p>
</li>
<li>
<p class="no-margin"><strong>The Area:</strong> <a href="#">'.$obj->getCityName($ads->region).'</a></p>
</li>
<!--
<li>
<p class=" no-margin "><strong>Condition:</strong> Tear Rubber</p>
</li>
<li>
<p class="no-margin"><strong>Brand:</strong> <a href="#">Apple</a></p>
</li>
-->
</ul>
</aside>
<div class="ads-action">
<ul class="list-border">

<li>
    <i class=" fa fa-phone"></i> <span class="pointer-style hidden_nos" >08XXXXXXXXX</span><span class="hide h-nos1">'.$ads->phone.'</span>
</li>

    
<li>
    
  <i class="fa fa-star-o pointer-style rating" data-value="1" aria-hidden="true" style="font-size:30px;"></i> 
  &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value="2" aria-hidden="true" style="font-size:30px;"></i>
    &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value="3" aria-hidden="true" style="font-size:30px;"></i>
    &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value="4" aria-hidden="true" style="font-size:30px;"></i>
    &nbsp; 
  <i class="fa fa-star-o pointer-style rating" data-value="5" aria-hidden="true" style="font-size:30px;"></i>
  
</li>

<li>
  Ad Rating: &nbsp; '.intval($rating_percentage).'%
</li>


  <li>
        <span id="report_button" class="pointer-style" data-toggle="modal" data-target="#reportSeller" >Report</span>
  </li';


    if(Auth::check()){
    
     $value.='<li id="save_am">
        <span  class="save_am pointer-style" >Save am </span>
     </li>

     <li id="follow_am">
       <span class="follow_am pointer-style" >Follow am </span>
     </li>';
             
    }
     




$value.-'<li>
  <a href="sellerprofile?id='.$ads->user_id.'">
    <i class=" fa fa-user"></i> Seller Name na '.$obj->getSellerName($ads->user_id).'
  </a>
</li>
<!--
<li>
<a href="#"> <i class=" fa fa-heart"></i> Save Am</a></li>
<li>
-->
<a href="#"> <i class="fa fa-share-alt"></i> Show Other Pipo On </a>
<div class="social-link">
<input type="text" id="ad_id" value="'.$ads->id.'" style="display:none;" />';
    
   $url = 'https://www.awimarket.com/adDetails?id='.$ads->id; 

$value.='<a class="twitter" target="_blank" data-original-title="twitter" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fawimarket.com/adDetails?id='.$ads->id.'%2F&amp;src=sdkpreparse" data-toggle="tooltip" data-placement="top"><i class="fa fa-twitter"></i></a>
<a class="facebook" target="_blank" data-original-title="facebook" href="https://twitter.com/intent/tweet?text='.$url.'" data-toggle="tooltip" data-placement="top"><i class="fa fa-facebook"></i></a>
<a class="google" target="_blank" data-original-title="google-plus" href="https://plus.google.com/share?url='.$url.'" data-toggle="tooltip" data-placement="top"><i class="fa fa-google"></i></a>

</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-4">
<div class="inner-box">
<div class="widget-title">
<h4>Advertisement</h4>
</div>';

if($sidebar_ads != null){
 if($obj->checkAd($val->id) == 1){ 
  $value.= '<img src="ad_photo/'.$obj1->getPhoto($sidebar_ads->id).'" alt="">';
    } 
 }

$value.='</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.3s">
<div class="features-icon">
<i class="lnr lnr-star">
</i>
</div>
<div class="features-content">
<h4>
Fraud Protection
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.6s">
<div class="features-icon">
<i class="lnr lnr-chart-bars"></i>
</div>
<div class="features-content">
<h4>
No Extra Fees
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.9s">
<div class="features-icon">
<i class="lnr lnr-spell-check"></i>
</div>
<div class="features-content">
<h4>
Verified Data
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
<div class="col-xs-12">
<div class="features-box wow fadeInDownQuick" data-wow-delay="0.9s">
<div class="features-icon">
<i class="lnr lnr-smile"></i>
</div>
<div class="features-content">
<h4>
Friendly Return Policy
</h4>
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>

  

        
        <div class="wrapper" style="margin-bottom:100px;">

<section id="categories-homepage">
<div class="container">
<div class="row">
<div class="col-md-12">

<section class="featured-lis">
<div class="container">
<div class="row">
<div class="col-md-12 wow fadeIn" data-wow-delay="0.5s">
<h3 class="section-title" style="text-transform:uppercase;">Other things wey you fit like</h3>
<div id="new-products" class="owl-carousel">';

    foreach($featured_ads as $val){

    if($obj1->checkAd($val->id) == 1){

$value .='<div class="item">
<div class="product-item">
<div class="carousel-thumb">
<a href="adDetails?id='.$val->id.'" >
<img src="ad_photo/'.$obj1->getPhoto($val->id).'" height="150" width="200" alt=""></a>

<div class="overlay">
<a href="adDetails?id='.$val->id.'"><i class="fa fa-link"></i></a>
</div>
</div>
<a href="adDetails?id='.$val->id.'" class="item-name">'.substr($val->title,0,20).'</a>
<span class="price">&#8358;'.number_format($val->price).'</span>
</div>
</div>';

}

}

$value.='</div>
</div>
</div>
</div>
</section>


</div>
    
<div class="modal fade" id="reportSeller" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Report Seller</h5>
      </div>
     <form action="reportSeller" method="get" >
      <div class="modal-body">
          	<!-- Search Form -->
				
					
                                            <div class="row">
                                              
                                                <div class="col-sm-12">';
                                                    
                                                   
                                                           
                                                             if(Session::get('report') == '1'){
                                                         
                                                           
                                                            
                                                                $value.=' <div style="background-color: red;color:white;">';
                                                                     foreach($errors->all() as $error){
                                                                 $value.=$error.'<BR>';
                                                                     }
                                                                $value.='</div>';
                                                            }
                                                     
                                                    
                                                    $value.='  <input type="text" class="form-control" name="seller" value="'.$ads->user_id.'" style="display:none;" />
                                                        <br>
                                                        <select name="reportreason" >
                                                            <option value="">Select</option>
                                                            <option>7</option>
                                                            <option>8</option>
                                                        </select>
                                                        <br>
                                                        <textarea  class="form-control" name="reportDescription" placeholder="Report Description" ></textarea>
                                                        <br>
                                                        <input type="text" class="form-control"  name="id" value="{{$ads->id}}" style="display:none;" />
                                                        <br>
                                                        
                                               
                                                    
                                                </div>
                                               
                                            </div>
					
				</div> <!-- /.rbm_search -->

      
      <div class="modal-footer">
       
        <button type="Submit" class="btn btn-primary" style="z-index:99999999">Submit</button>
        
        <button type="button" class="btn btn-primary" data-dismiss="modal" style="z-index:99999999">Close</button>
       
      </div>
      </form> <!-- /form -->
  </div>
  </div>
</div>';

    if(Session::get('report') == '1'){
    
        $value.='<script>
        
                $("#reportSeller").modal();
        
          </script>';
    
    }
    
    if(Session::get('report') == '2'){
    
       $value.='<script>
        
                alert("Report Sent Successfully");
        
          </script>';
    
    }
     
 Session::put('report',null);  
    


$value.='</div>

<a href="#" class="back-to-top">
<i class="fa fa-angle-up"></i>
</a>
    

      
    </div>
    
        </div>';
    }
    
    public function aboutus()
    {
        $category_all = category::all();
        $navigation_all = navigation::all();
        $ads_tags = ads::all();
        return view('aboutus')->with(['category_all'=>$category_all,'navigation_all'=> $navigation_all,'ads_tags'=>$ads_tags,'obj'=>$this,'obj1'=>$this]);
    }
    
    public function faq()
    {
        $category_all = category::all();
        return view('faq')->with(['category_all'=>$category_all,'navigation_all'=>navigation::all(),'ads_tags'=>ads::all(),'obj'=>$this,'obj1'=>$this]);
    }
    
    public function contactus()
    {
        $category_all = category::all();
        $navigation_all = navigation::all();
        $ads_tags = ads::all();
        return view('contactus')->with(['category_all'=>$category_all,'navigation_all'=>$navigation_all,'ads_tags'=>$ads_tags,'obj'=>$this,'obj1'=>$this]);
    }
    
    public function getAdDetails($id){
        if(isset(ads::where('id',$id)->get()[0]->details))
        {
             return ads::where('id',$id)->get()[0]->details;
        }
    }
    
    public function getAdPrice($id){
        if(isset(ads::where('id',$id)->get()[0]->price))
        {
             return ads::where('id',$id)->get()[0]->price;
        }
    }
    
    public function getPhoto($id){
        
        if(isset(adspic::where('ad_id',$id)->where('index_value','1')->get()[0]->pic_name))
        {
             return adspic::where('ad_id',$id)->where('index_value','1')->get()[0]->pic_name;
        }
       
        //return $id;
    }
    
    public function getCategoryName($cat_id)
    {
        $obj = category::find($cat_id);
        
        if($obj){
        
           return $obj->cat_name;
           
        }
        
    }
    
   
    
    public function getSubCategoryName($cat_id)
    {
        $obj = categories::find($cat_id);
        
        if($obj){
        
           return $obj->cat_name;
           
        }
        
    }
    
    public function getPhoneNo($id)
    {
        $obj = users::find($id);
        
       // return $obj->
    }
    
    public function getAllPhoto($id){
      
        
        return adspic::where('ad_id',$id)->orderby('index_value','asc')->get();
      
    }
    
    public function getCityName($id){
        
        $obj = cities::find($id);
        
        if($obj)
        {
        
           return $obj->city;
           
        }
        
    }
    
    public function category()
    {
        $all_ads = ads::orderby('id','desc')->get();
        
        Session::put('all_ads',$all_ads);
        
        if(Session::get('search') == '1')
        {
            Session::put('all_ads',Session::get('all_ads1'));
        }    
        
        $obj = $this;
        $category_all = category::all();
        $cities_all = cities::all();
        //$navigation_all = navigation::all();
        $ads_tags = ads::all();
        
        return view('category')->with(['all_ads'=>$all_ads ,'ads_tags'=>$ads_tags,'all_cities'=>cities::all(), 'navigation_all' => navigation::all() ,'obj'=>$obj,'category_all'=>$category_all,'cities_all'=>$cities_all,'obj1'=>$this]);
    }
    
    
    
    public function getSellerName($id)
    {
        
      //  dd($id);
        
        $obj = users::find($id);
        
        return $obj->firstname.' '.$obj->lastname;
       
        
    }
}
