<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\models\codes;
use App\models\users;
use Request;
use Jusibe;
use Hash;
use Validator;
use Session;
use Auth;

class LoginController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function Authenticate()
    {
       $data = Request::all();
       
        $rules = [
            'captcha_entered' => 'required|numeric',
           
        ];
        
        $validator = Validator::make($data,$rules);
        
        if($validator->fails()){
            Session::put('error_login','2'); 
            return redirect('signup?login=1');
        }
        elseif((int) Request::get('captcha_entered') !== (int)Session::get('rand_code')){
            Session::put('error_login','2'); 
            return redirect('signup?login=1');
        }
        else{
            
             if(Auth::attempt(['user_email' => $data['email_login'], 'password' => $data['password_login'], 'user_status' => '0'])) {
        
                      Session::put('email_login',null);
                      Session::put('password_login',null );    
                      return redirect('dashboard');
             }
             else{
                    Session::put('email_login',$data['email_login'] );
                    Session::put('password_login',$data['password_login'] );
                    Session::put('error_login','1'); 
                    return redirect('signup?login=1');
             }
            
        }
     
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
   
     public function Adminlogout()
    {
        Auth::guard('admins')->logout();
        return redirect('adminLogin');
    }
   
}
