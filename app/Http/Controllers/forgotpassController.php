<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\models\codes;
use App\models\photo;
use App\models\users;
use App\models\tags;
use Request;
use Jusibe;
use Hash;
use Validator;
use Session;
use Auth;
use Image;
use Illuminate\Validation\Rule;
use ImageOptimizer;


class forgotpassController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function forgotpass(){
        
        return view('forgotpass');
    }
    
    public function confirm_code(){
        
        $data = Request::all();
        
        $rules = [
           
            'code' => 'required|exists:codes',
            
        ];
        
        $validator = Validator::make($data,$rules);
        
        if($validator->fails()){
            
            return redirect('forgotpass')->withErrors($validator);
           
        }
        else{
        
       
            
            
            Session::put('suc1',2);
            
            return redirect('forgotpass');
            
        }
    }
    
    public function changepassword()
    {
        
        $value = Request::all();
        
        $rules = [
            
            'password1' => 'required',
            'password2' => 'required|same:password1' 
         
        ];
        
        $validator = Validator::make($value,$rules);
        
        if($validator->fails()){
            
            //Session::put('error2','1');
            return redirect('forgotpass')->withErrors($validator);
            
            
        }
        else{
            $obj = users::find(users::where('phone',Session::get('ph'))->get()[0]->id);
            
            $obj->password = Hash::make($value['password1']);
         
            
            $obj->save();
            
            Session::put('suc1','3');
            
            return redirect('forgotpass');
        }
        
    }
    
    public function send_forgot_email()
    {
        $data = Request::all();
        
        $rules = [
           
            'phone' => 'required|numeric|exists:users',
            
        ];
        
        $validator = Validator::make($data,$rules);
        
        if($validator->fails()){
            
            return redirect('forgotpass')->withErrors($validator);
           
        }
        else{
        
         $reg_obj = new forgotpassController();
         
         
        
         $reg_obj->sendcode($data['phone']);
         
         Session::put('suc1',1);
         
         Session::put('ph',$data['phone']);
         
         return redirect('forgotpass');
         
        
        }
        
        
        
    }
    
    public function sendcode($phone){
    
    $value = Request::all();
      
            $code =  rand(123457824, 987694780);
            
            $code = substr($code,0,4);
            
            $payload = [
               'to' => $value['phone'],
               'from' => 'Awi market',
               'message' => $code
               ];
            
             
                $cod_id = codes::where('phone',$phone)->orderby('id','asc')->take(1)->get();
                
                //var_dump($cod_id);
                
                if(sizeof($cod_id) > 0)
                {
                    $cd = codes::find($cod_id[0]['id']);
                    
                    $cd->delete();
                    
                    //return redirect('kk');
                }
                
            
            try{
                Jusibe::sendSMS($payload)->getResponse();
               
                $codes = new codes();
            
                $codes->code =  $code;
                    
                $codes->phone = $phone;
            
                $codes->save();
                
                echo "code Sent";
            }
            catch (\Exception $ex) {
                echo "Error please try again";
            }
            
        }
        
    
        
        
    
   
   
}
