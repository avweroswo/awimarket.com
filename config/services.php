<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
        'twitter' => [
    'client_id' => env('TWITTER_KEY','X6ZupB0IMRcd04FnC8vcNIPNl' ),       // facebook client id
    'client_secret' => env('TWITTER_SECRET','qpjUeEugmZXPhDx6L9MUNXY94uIV6kiFWomle4603uLcGiYHKP'), // facebook secret
    'redirect' => 'https://www.awimarket.com/login/twitter/callback',
],

'facebook' => [
    'client_id' => env("app_id",'188255891794242' ),       // facebook client id
    'client_secret' => env("app_secret",'c4b0d32b0c83105b383556c1c3e17961'), // facebook secret
    'redirect' => 'https://www.awimarket.com/login/facebook/callback',
],

'google' => [
    'client_id' => '504862492169-22au6tpg32rg405glfqugb6dtn772crk.apps.googleusercontent.com',       // facebook client id
    'client_secret' => 'OViyugwMjQ8C5WrUK6QEfMED', // facebook secret
    'redirect' => 'https://www.awimarket.com/login/google/callback',
    ],

];
