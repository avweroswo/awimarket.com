<?php

/*
 * This file is part of the Laravel Jusibe package.
 *
 * (c) Prosper Otemuyiwa <prosperotemuyiwa@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /**
     * Public Key From Jusibe Dashboard
     *
     */
    'publicKey' => getenv('3a7ac24586e961ceb0e6358e5ba9972a'),

    /**
     * Access Token From Jusibe  Dashboard
     *
     */
    'accessToken' => getenv('c165acb382ee53336525ef47a8a83919'),
];